'use strict';

function MachinesCtrl($scope, $state, $q, Alert, Authenticate,MachineApi) {
    var vm = this;
    
    vm.api = MachineApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Название',
            attr: 'MACHINE_NAME',
            type: 'text'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.MACHINE_NAME) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.MACHINE_NAME)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.MACHINE_ID, gridRow.MACHINE_NAME)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.MACHINE_ID)
    }

}

angular.module('rControllers').controller('MachinesCtrl', MachinesCtrl);