'use strict';
            
function WorknormsCtrl($scope, $state, $q, Alert, WorknormApi, Authenticate, OperationApi, MachineApi, CabletypeApi, CablestructureApi, ConductorApi) {
    var vm = this;

    vm.api = WorknormApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Жила',
            attr: 'CONDUCTOR_ID',
            type: 'list',
            load: ConductorApi.all,
            value_attr: 'CONDUCTOR_NAME',
            nullable: true
        },
        {
            name: 'Сечение',
            attr: 'CABLESTRUCTURE_ID',
            type: 'list',
            load: CablestructureApi.all,
            value_attr: 'CABLESTRUCTURE_NAME',
            nullable: true
        },
        {
            name: 'Тип кабеля',
            attr: 'CABLETYPE_ID',
            type: 'list',
            load: CabletypeApi.all,
            value_attr: 'CABLETYPE_NAME',
            nullable: true
        },
        {
            name: 'Станок',
            attr: 'MACHINE_ID',
            type: 'list',
            load: MachineApi.all,
            value_attr: 'MACHINE_NAME'
        },
        {
            name: 'Операция',
            attr: 'OPERATION_ID',
            type: 'list',
            load: OperationApi.all,
            value_attr: 'OPERATION_NAME'
        },
        {
            name: 'Норма, ч/км',
            attr: 'WORKNORM_TIME',
            type: 'float'
        },
        {
            name: 'Норма, км/8ч',
            attr: 'WORKNORM_VOL',
            type: 'float',
            disableEdit: true //TODO не давать редактировать (ибо поле вычисляемое исходя из 'Норма, ч/км')
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.MACHINE_ID || !newObject.OPERATION_ID || !newObject.WORKNORM_TIME) {
            return $q.reject("Не все поля заполнены.");
        }
        var CABLESTRUCTURE_ID;
        var CONDUCTOR_ID;
        var CABLETYPE_ID;
        if (newObject.CABLESTRUCTURE_ID) CABLESTRUCTURE_ID = newObject.CABLESTRUCTURE_ID.CABLESTRUCTURE_ID
        if (newObject.CONDUCTOR_ID) CONDUCTOR_ID = newObject.CONDUCTOR_ID.CONDUCTOR_ID
        if (newObject.CABLETYPE_ID) CABLETYPE_ID = newObject.CABLETYPE_ID.CABLETYPE_ID
        return vm.api.add(CABLESTRUCTURE_ID, 
                          CONDUCTOR_ID, 
                          newObject.MACHINE_ID.MACHINE_ID, 
                          newObject.OPERATION_ID.OPERATION_ID, 
                          CABLETYPE_ID,
                          newObject.WORKNORM_TIME)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.WORKNORM_ID, 
                           gridRow.CABLESTRUCTURE_ID, 
                           gridRow.CONDUCTOR_ID, 
                           gridRow.MACHINE_ID, 
                           gridRow.OPERATION_ID, 
                           gridRow.CABLETYPE_ID, 
                           gridRow.WORKNORM_TIME)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.WORKNORM_ID)
    }

}

angular.module('rControllers').controller('WorknormsCtrl', WorknormsCtrl);