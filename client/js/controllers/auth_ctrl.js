'use strict';

function AuthCtrl($scope, $state, Alert, Authenticate) {
    var vm = this;
    vm.login = login;

    function login (user) {
        Authenticate.login(user)
            .then(function (res) {
                console.log("$scope.login ", res.data)
                $state.go("main.hello");
            }).catch(function (err) {
                console.log("$scope.login error ", err.data)
                if (err.data && err.data.message) {
                    Alert.showError(err.data.message);
                } else if (err.data) {
                    Alert.showError(err.data);
                }
            });
    }

}

angular.module('rControllers').controller('AuthCtrl', AuthCtrl);