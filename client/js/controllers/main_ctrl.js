function MainCtrl($scope, $state,  Authenticate) {
    var vm = this;
    vm.isSignedIn = function () {
        return Authenticate.isAuthenticated();
    };

    vm.checkRole = function (roleArray){
        return Authenticate.checkRole(roleArray)
    }

    vm.logout  = function () {
        Authenticate.logout()
            .then(function () {
                $state.go("main.login");
            }).catch(function () {
            });
    }

    vm.userLogin = function () {
        return Authenticate.userLogin();
    }

    vm.userId = function () {
        return Authenticate.userId();
    }
}
angular.module('rControllers').controller('MainCtrl', MainCtrl);