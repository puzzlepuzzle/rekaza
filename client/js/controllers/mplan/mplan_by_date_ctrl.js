'use strict';

function MplanByDateCtrl($scope, $state, Alert, $timeout, uiGridConstants, Authenticate,
                         MplanApi, MachineApi, OperationApi, ModalApi, DetailApi) {
    var vm = this;
    vm.loading = false;
    vm.onLoading = function (isLoading) {
        vm.loading = isLoading;
    };
    vm.load = load;
    vm.newElement = newElement;
    vm.data = null;
    vm.selectedData = null;
    vm.dateFrom = null;
    vm.dateTo = null
    vm.hoursCells = null
    vm.detailsCells = null
    vm.localLang = {
        selectAll: "Выбрать Все",
        selectNone: "Очистить",
        reset: "Сбросить",
        search: "Введите имя станка...",
        nothingSelected: "Выберете станки"         //default-label is deprecated and replaced with this.
    }
    vm.startLoadDone = false;
    vm.machines = [];
    vm.operations = [];
    vm.details = [];

    startLoad();

    function startLoad() {
        vm.loading = true;
        return MachineApi.all()
            .catch(function (err) {
                console.log('MachineApi.all return ERROR!', err.data);
                vm.machines = [];
            })
            .then(function (res) {
                console.log('MachineApi.all ', res.data);
                vm.machines = res.data;
                return OperationApi.all()
            })
            .catch(function (err) {
                console.log('OperationApi.all return ERROR!', err.data);
                vm.operations = [];
            })
            .then(function (res) {
                console.log('OperationApi.all ', res.data);
                vm.operations = res.data;

                var d = new Date();
                d.setHours(0, 0, 0, 0);
                vm.dateFrom = new Date(d);
                d.setDate(d.getDate() + 1)
                vm.dateTo = new Date(d);

                return load();
            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function loadDetails() {
        return DetailApi.all(vm.dateFrom.toISOString(), vm.dateTo.toISOString())
            .catch(function (err) {
                console.log('DetailApi.all return ERROR!', err.data);
                vm.details = [];
            })
            .then(function (res) {
                console.log('DetailApi.all ', res.data);
                vm.details = res.data.sort(function (a, b) {
                    return a['DETAIL_FROM'] - b['DETAIL_FROM']
                })
            })
    }

    function getHoursCells() {
        if (!vm.dateFrom || !vm.dateTo) {
            console.log('not all dates for cells')
        }
        var dateFrom = new Date(vm.dateFrom)
        var dateTo = new Date(vm.dateTo)

        var hours = []
        for (var hourDate = new Date(dateFrom); hourDate < dateTo; addHours(hourDate, 1)) {
            hours.push({date: new Date(hourDate)})
        }
        return hours;
    }

    function getDetailsCells() {
        if (!vm.dateFrom || !vm.dateTo) {
            console.log('not all dates for getDetailsCells')
        }
        if (!vm.details || !vm.details.length) {
            console.log('No details!')
            return null
        }
        var to = new Date(vm.dateTo)
        var from = new Date(vm.dateFrom)
        var cells = []
        for (var key in vm.details) {
            var d = vm.details[key]
            cells.push({
                colspan: diffHours(Math.min(new Date(d['DETAIL_TO']), to),
                    Math.max(new Date(d['DETAIL_FROM']), from)),
                data: d
            })
        }
        return cells;
    }

    function addHours(date, hours) {
        return date.setHours(date.getHours() + hours)
    }

    function diffHours(date1, date2) {
        return Math.abs(date1 - date2) / 36e5
    }

    function load() {
        var dateFrom = vm.dateFrom;
        var dateTo = vm.dateTo;
        if (!dateFrom || !dateTo) {
            console.log('not all dates');
            return;
        }
        if (dateTo <= dateFrom) {
            console.log('no correct data');
            return
        }
        console.log('load', dateFrom.toISOString(), dateTo.toISOString());
        vm.loading = true;
        return loadDetails()
            .then(function () {
                return MplanApi.get_date_mplan(dateFrom.toISOString(), dateTo.toISOString())
            })
            .then(function (res) {
                console.log('mplan_date are received ', res.data);
                var sorderdData = sortByMachine(res.data);
                var data = createTd(sorderdData, dateFrom, dateTo)
                vm.data = data.map(function (el) {
                    el.ticked = true;
                    return el
                })
                vm.selectedData = null
                vm.hoursCells = getHoursCells()
                vm.detailsCells = getDetailsCells()
                console.log(' vm.detailsCells', vm.detailsCells)
                console.log('mplan_date RECREATE ', vm.data);
            })
            .catch(function (err) {
                console.log('get mplan_date return ERROR!', err.data);
                vm.data = null;
                Alert.showError( "Ошибка при получении Плана: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })

    }

    function sortByMachine(data) {
        var result = []
        for (var key in data) {
            var row = data[key];

            var stankiWithCurrentName = result.filter(function (v) {
                return v.title === row['MACHINE_NAME'];
            })

            var stanok;
            if (stankiWithCurrentName.length > 0) {
                stanok = stankiWithCurrentName[0];
            } else {
                stanok = {
                    title: row['MACHINE_NAME'],
                    data: []
                }
                result.push(stanok)
            }
            stanok.data.push(row)
        }
        result = result.map(function (el) {
            el['data'].sort(function (a, b) {
                return a['MPLAN_FROM'] - b['MPLAN_FROM']
            })
            return el
        })

        return result;
    }

    function crossPeriods(period1, period2) {
        if (period1['FROM'] < period2['TO'] && period2['FROM'] < period1['TO']) {
            return true;
        }
        return false;
    }

    function addPeriodToRow(row, period) {
        for (var i = 0; i < row.length; i++) {
            if (row[i]['FROM'] > period['FROM']) {
                row.splice(i, 0, period);
                return;
            }
        }
        row.splice(row.length, 0, period);
    }

    function addRowToRows(row, rows) {
        rows.push(row)
    }

    function addPeriodToRows(period, rows) {
        rowLabel : for (var keyRow in rows) {
            var row = rows[keyRow]
            periodLabel : for (var keyPeriod in row) {
                var p = row[keyPeriod]
                if (crossPeriods(period, p)) {
                    continue rowLabel;
                }
            }
            addPeriodToRow(row, period)
            return
        }
        addRowToRows([period], rows)
    }

    function addPeriodsToRows(periods, rows) {
        for (var i in periods) {
            addPeriodToRows(periods[i], rows)
        }
    }

    function addEmptiesToRow(row, delimeterDates) {
        for1 : for (var i = 0; i < delimeterDates.length - 1; i++) {
            var currentPeriod = {FROM: delimeterDates[i], TO: delimeterDates[i + 1]};
            for (var j = 0; j < row.length; j++) {
                var period = row[j];
                if (crossPeriods(period, currentPeriod)) {
                    period['colspan'] +=1
                    continue for1;
                }
            }
            addPeriodToRow(row, currentPeriod)
        }
    }

    function addEmptiesAndColspanToRows(rows, delimeterDates) {
        for (var j = 0; j < rows.length; j++) {
            var row = rows[j];
            addEmptiesToRow(row, delimeterDates)
        }
    }

    function getFromToColspan(periods) {
        for (var i = 0; i < periods.length; i++) {
            var period = periods[i]
            period['FROM'] = new Date(period['MPLAN_FROM'])
            period['FROM'].setSeconds(0);

            period['TO'] = new Date(period['MPLAN_TO'])
            period['TO'].setSeconds(0);

            period['colspan'] = 0;
        }
    }

    function addDelimiterToDates(dates, date, startDate, endDate) {
        if(date <= startDate) return
        if(date >= endDate) return
        for (var i = 0; i < dates.length; i++) {
            if (dates[i].getTime() == date.getTime()) {
                //console.log('addDelimiterToDates = eq', dates, date)
                return;
            }
            if (dates[i].getTime() > date.getTime()) {
                dates.splice(i, 0, date);
                //console.log('addDelimiterToDates > add', dates, date)
                return;
            }
        }
        dates.splice(dates.length, 0, date);
    }
    function addDelimitersToDates(dates,periods, startDate, endDate) {
        for (var key in periods) {
            var period = periods[key]
            addDelimiterToDates(dates, period['FROM'], startDate, endDate)
            addDelimiterToDates(dates, period['TO'], startDate, endDate)
        }
    }

    function createTd(data, startDate, endDate) {
        var delimeterDates = [new Date(startDate), new Date(endDate)];
        for (var key in data) {
            var machine = data[key];
            var periods = machine.data;
            getFromToColspan(periods);
            //console.log('periods', periods)
            addDelimitersToDates(delimeterDates, periods, startDate, endDate);
        }
        //console.log('delimeterDates', delimeterDates)
        for (var key in data) {
            var machine = data[key];
            var periods = machine.data;
            var rows = []
            addPeriodsToRows(periods, rows);
            addEmptiesAndColspanToRows(rows, delimeterDates);
            machine.rows = rows;
        }

        return data
    }

    function newElement() {
        //if (!params.machine.MACHINE_ID
        //|| !params.newPeriod.MPLAN_FROM || !)
        var add = function () {
            vm.loading = true;
            return MplanApi.add(null, params.machine.MACHINE_ID,
                null, params.newPeriod.MPLAN_FROM, params.newPeriod.MPLAN_DURATION,
                params.operation.OPERATION_ID, params.newPeriod.MPLAN_IMPORTANT)
                .then(function (res) {
                    console.log('period added ', res.data);
                    return vm.load();
                })
                .catch(function (err) {
                    console.log('add period return ERROR!', err.data);
                    Alert.showError( "Ошибка при создании: " + err.data);
                })
                .finally(function () {
                    vm.loading = false;
                })


        }

        var params = {
            newPeriod: {MPLAN_IMPORTANT: 0},
            operations: vm.operations,
            machines: vm.machines,
            operation: {},
            machine: {},
            updateDateTo: function () {
                if (params.newPeriod.MPLAN_DURATION >= 1) {
                    var newTo = new Date(params.newPeriod.MPLAN_FROM)
                    newTo.setTime(newTo.getTime() + params.newPeriod.MPLAN_DURATION * 60 * 60 * 1000)
                    params.newPeriod.MPLAN_TO = newTo;
                }
            },
            updateDuration: function () {
                var to = new Date(params.newPeriod.MPLAN_TO)
                var from = new Date(params.newPeriod.MPLAN_FROM)
                params.newPeriod.MPLAN_DURATION = Math.abs(to - from) / 36e5
            },
            changeImportant: function () {
                params.newPeriod.MPLAN_IMPORTANT = params.newPeriod.MPLAN_IMPORTANT ? 0 : 1;
            },
            add: add

        }

        console.log('CLICK', params)
        return ModalApi.show('partials/directives/plan_cell_modal.html', params, false)
            .then(function (confirm) {
                console.log('confirm', confirm)
            })
            .catch(function (err) {
                console.log('err', err)
            })
    }
}

angular.module('rControllers').controller('MplanByDateCtrl', MplanByDateCtrl);