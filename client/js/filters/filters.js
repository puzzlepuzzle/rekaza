'use strict';

/* Filters */

var rFilters = angular.module('rFilters', []);

rFilters.filter('yes_no', function () {
    return function (input) {
        return input ? 'Да' : 'Нет';
    };
});
rFilters.filter('operation_type', function () {
    var hash = {
        1: 'Покабельно',
        2: 'Пожильно',
        3: 'Отжиг'
    };
    return function(input) {
        if (!input){
            return '';
        } else {
            return hash[input];
        }
    };
})

rFilters.filter('foreign_type', function () {

    return function(input, hash) {
        //console.log('filter', hash, input)
        if (!input){
            return '';
        } else {
            return hash[input];
        }
    };
})
