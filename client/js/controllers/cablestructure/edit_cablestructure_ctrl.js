'use strict';

function CablestructureCtrl($scope, $stateParams, $state, $q, $timeout, Alert, Authenticate,CablestructureApi, RealcspartApi, CspartApi, ColorApi) {
    var vm = this;

    vm.loading = false;
    vm.dataIsLoaded = false;
    vm.cablestructure = null;
    vm.cablestructureCopy = null;
    vm.realcsparts = [];
    vm.realcspartsCopy = [];
    vm.csparts = [];
    vm.colors = [];
    vm.checkRole = Authenticate.checkRole;
    vm.load = load;
    vm.removeRealcspart = removeRealcspart
    vm.changeCablestructure = changeCablestructure;
    vm.cablestructureIsChanged = cablestructureIsChanged;
    vm.realcspartIsChanged = realcspartIsChanged;
    vm.saveRealcspart = saveRealcspart;
    vm.cndIsChanged = cndIsChanged;
    vm.saveCnd = saveCnd;
    vm.addRealcspart = addRealcspart;
    vm.cancelAddRealcspart = cancelAddRealcspart;
    vm.createRealcspart = createRealcspart;
    vm.isNewRealcspart = false
    vm.newRealcspart = null
    startLoad();

    function startLoad() {
        return load()
            .then(function () {
                return loadCsparts()
            })
            .then(function () {
                return loadRealcsparts()
            })
            .then(function () {
                return loadColors()
            })
            .then(function () {
                vm.dataIsLoaded = true;
            })
    }

    function load() {
        if (!$stateParams.cablestructure_id) {
            Alert.showError("Не найден id сечения.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return CablestructureApi.get($stateParams.cablestructure_id)
            .then(function (res) {
                console.log('cablestructure is reseived ', res.data);
                vm.cablestructure = res.data;
                vm.cablestructureCopy = JSON.parse(JSON.stringify(res.data));
            })
            .catch(function (err) {
                console.log('get cablestructure return ERROR!', err.data);
                vm.cablestructure = null;
                vm.cablestructureCopy = null;
                Alert.showError("Ошибка при получении сечения: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function loadRealcsparts() {
        vm.loading = true;
        return RealcspartApi.all($stateParams.cablestructure_id)
            .then(function (res) {
                vm.realcsparts = res.data;
                return $q.all( vm.realcsparts.map(function(element){
                    return RealcspartApi.all_cnds(element.REALCSPART_ID)
                        .then(function(res1){
                            element.cnds = res1.data
                        })
                        .catch(function (err1) {
                            console.log('get all_cnds return ERROR!', element, err1.data);
                        })
                }))
            })
            .then(function (res) {
                console.log('Realcsparts are reseived ',  vm.realcsparts );
                vm.realcspartsCopy = JSON.parse(JSON.stringify(vm.realcsparts))
            })
            .catch(function (err) {
                console.log('get Realcsparts return ERROR!', err.data);
                vm.realcsparts = [];
                vm.realcspartsCopy = [];
                Alert.showError("Ошибка при получении групп жил для сечения: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function loadCsparts() {
        vm.loading = true;
        return CspartApi.all()
            .then(function (res) {
                console.log('Csparts are reseived ', res.data);
                vm.csparts = res.data;
            })
            .catch(function (err) {
                console.log('get Csparts return ERROR!', err.data);
                vm.csparts = [];
                Alert.showError("Ошибка при получении списка групп жил: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function loadColors() {
        vm.loading = true;
        return ColorApi.all()
            .then(function (res) {
                console.log('Colors are reseived ', res.data);
                vm.colors = res.data;
            })
            .catch(function (err) {
                console.log('get Csparts return ERROR!', err.data);
                vm.colors = [];
                Alert.showError("Ошибка при получении списка цветов: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function changeCablestructure (){
        if (!vm.cablestructure){
            return $q.when(undefined); ;
        }
        vm.loading = true;
        return CablestructureApi.edit(vm.cablestructure.CABLESTRUCTURE_ID, vm.cablestructure.CABLESTRUCTURE_NAME)
            .then(function (res) {
                console.log('cablestructure is changed ', res.data);
                Alert.showSuccess("Сечение успешно обновлено.");
                vm.cablestructureCopy = JSON.parse(JSON.stringify(vm.cablestructure));

            })
            .catch(function (err) {
                console.log('change cablestructure return ERROR!', err.data);
                Alert.showError( "Ошибка при сохранении сечении: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function cablestructureIsChanged (){
        if (!vm.cablestructure || !vm.cablestructureCopy){
            return false;
        }
        //console.log(vm.cablestructure,vm.cablestructureCopy)

        if (vm.cablestructure.CABLESTRUCTURE_NAME != vm.cablestructureCopy.CABLESTRUCTURE_NAME){
            return true;
        }
        return false
    }
    function removeRealcspart (realcspart){
        if ( !realcspart){
            return $q.when(undefined); ;
        }
        vm.loading = true;
        return RealcspartApi.remove(realcspart.REALCSPART_ID)
            .then(function (res) {
                console.log('realcspart is deleted ', res.data);
                Alert.showSuccess("Группа удалена.");

                var deletedRealcspart = vm.realcsparts.filter(function ( obj ) {
                    return obj.REALCSPART_ID === realcspart.REALCSPART_ID;
                })[0];
                if (deletedRealcspart){
                    var index = vm.realcsparts.indexOf(deletedRealcspart);
                    if (index >= 0) {
                        vm.realcsparts.splice(index, 1);
                        vm.realcspartsCopy = JSON.parse(JSON.stringify(vm.realcsparts))
                    }
                } else {
                    Alert.showSuccess("Не найдена удаленная группа.");
                }


            })
            .catch(function (err) {
                console.log('remove realcspart return ERROR!', err.data);
                Alert.showError("Ошибка при удалении группы: ");
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function realcspartIsChanged(rc){
        if (!rc){
            return false
        }
        //console.log(cable)
        var changedRc = vm.realcsparts.filter(function ( obj ) {
            return obj.REALCSPART_ID === rc.REALCSPART_ID;
        })[0];
        if (changedRc){
            if (changedRc.CSPART_COLOR_ID != rc.CSPART_COLOR_ID){
                return true
            } else {
                return false;
            }
        } else {
            return false
        }
    }

    function cndIsChanged(realcnspart, cnd){
        if (!realcnspart || !cnd){
            return false
        }
        //console.log(cable)
        var changedRc = vm.realcsparts.filter(function ( obj ) {
            return obj.REALCSPART_ID === realcnspart.REALCSPART_ID;
        })[0];
        if (changedRc){
            var changedCnd = changedRc.cnds.filter(function ( obj ) {
                return obj.CNDCSPART_ID === cnd.CNDCSPART_ID;
            })[0];
            if (changedCnd && changedCnd.COLOR_ID != cnd.COLOR_ID){
                return true
            } else {
                return false;
            }
        } else {
            return false
        }
    }

    function saveRealcspart (rc){
        if (!rc || !rc.CSPART_COLOR_ID){
            console.log(rc)
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined) ;
        }
        vm.loading = true;
        //id, cabletype_id, techseq_id, cablestructure_id, done, vol
        return RealcspartApi.edit(rc.REALCSPART_ID, rc.CSPART_COLOR_ID)
            .then(function (res) {
                console.log('rc is saved ', res.data);
                var changedRc = vm.realcsparts.filter(function ( obj ) {
                    return obj.REALCSPART_ID === rc.REALCSPART_ID;
                })[0];
                if (changedRc){
                    changedRc.CSPART_COLOR_ID = rc.CSPART_COLOR_ID
                    Alert.showSuccess("Группа сохранена.")
                    return;
                }
                Alert.showError( "Ошибка при сохранении группы.");
            })
            .catch(function (err) {
                console.log('edit rc return ERROR!', err.data);
                Alert.showError( "Ошибка при сохранении группы: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function saveCnd (rc, cn){
        if (!rc || !cn || !cn.COLOR_ID){
            console.log(cn)
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined) ;
        }
        vm.loading = true;
        //id, cabletype_id, techseq_id, cablestructure_id, done, vol
        return RealcspartApi.edit_cnd(cn.REALCNDCSPART_ID, cn.COLOR_ID)
            .then(function (res) {
                console.log('cn is saved ', res.data);
                var changedRc = vm.realcsparts.filter(function ( obj ) {
                    return obj.REALCSPART_ID === rc.REALCSPART_ID;
                })[0];
                if (changedRc){
                    var changedCnd = changedRc.cnds.filter(function ( obj ) {
                        return obj.CNDCSPART_ID === cn.CNDCSPART_ID;
                    })[0];
                    if (changedCnd){
                        changedCnd.COLOR_ID = cn.COLOR_ID
                        Alert.showSuccess("Группа сохранена.");
                        return
                    }
                }
                Alert.showError( "Ошибка при сохранении группы.");
            })
            .catch(function (err) {
                console.log('edit rc return ERROR!', err.data);
                Alert.showError( "Ошибка при сохранении группы: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function addRealcspart (){
        vm.isNewRealcspart = true;
    }

    function  cancelAddRealcspart (){
        vm.isNewRealcspart = false;
        vm.newRealcspart = {};
    }

    function createRealcspart (){
        if (!vm.newRealcspart  || !vm.newRealcspart.CSPART_ID || !vm.newRealcspart.count || !vm.newRealcspart.COLOR_ID ){
            console.log(vm.newRealcspart)
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined) ;
        }
        vm.loading = true;
        return RealcspartApi.add(vm.newRealcspart.CSPART_ID, $stateParams.cablestructure_id,
            vm.newRealcspart.count, vm.newRealcspart.COLOR_ID)
            .then(function (res) {
                console.log('rc is created ', res.data);
                vm.realcsparts.push(res.data)
                vm.newRealcspart = {};
                vm.isNewRealcspart = false;
                return RealcspartApi.all_cnds(res.data.REALCSPART_ID)
                    .then(function(res1){
                        res.data.cnds = res1.data
                    })
                    .catch(function (err1) {
                        console.log('get all_cnds return ERROR!', res.data, err1.data);
                    })
            })
            .then(function (res) {
                Alert.showSuccess("Группа добавлена.");
                vm.realcspartsCopy = JSON.parse(JSON.stringify(vm.realcsparts))
            })
            .catch(function (err) {
                console.log('create cable return ERROR!', err.data);
                Alert.showError( "Ошибка при создании кабеля: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }
}

angular.module('rControllers').controller('CablestructureCtrl', CablestructureCtrl);