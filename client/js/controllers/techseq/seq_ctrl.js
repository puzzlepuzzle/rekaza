'use strict';

function SeqCtrl($scope, $state, $q, $stateParams, Alert, TechseqApi, TechUnitApi, OperationApi, Authenticate) {
    var vm = this;
    vm.loading = false;
    vm.seq = null;
    vm.newSeqName = null;
    vm.units = [];
    vm.operations = [];
    vm.copuUnits = [];
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.updateSeqName = updateSeqName;
    vm.seqNameChanged = seqNameChanged;
    vm.removeUnit = removeUnit;
    //vm.changeSeq = changeSeq;
    //vm.seqIsChanged = seqIsChanged;
    vm.removeSeq = removeSeq;
    vm.newUnit = null;
    vm.addUnit = addUnit;
    vm.saveSeq = saveSeq;
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.load();

    function load() {
        if (!$stateParams.techseq_id) {
            Alert.showError("Не найден id тех.цепочки.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return TechseqApi.get($stateParams.techseq_id)
            .then(function (res) {
                console.log('seq is reseived ', res.data);
                vm.seq = res.data;
                vm.newSeqName = res.data.TECHSEQ_NAME;
                return TechUnitApi.all($stateParams.techseq_id)
            })
            .then(function (res) {
                console.log('units is reseived ', res.data);
                vm.units = res.data;
                return OperationApi.all()
            })
            .then(function (res) {
                console.log('operaions is reseived ', res.data);
                vm.operations = res.data;
            })
            .catch(function (err) {
                console.log('get seq or units or operations return ERROR!', err.data);
                vm.seq = null;
                vm.units = [];
                vm.operations = [];
                Alert.showError("Ошибка при получении тех.цепочки: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
                vm.dataIsLoaded = true
            })

    };

    function updateSeqName() {
        if (!vm.newSeqName) {
            Alert.showError("Имя цепочки не может быть пустым.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return TechseqApi.edit(vm.seq.TECHSEQ_ID, vm.newSeqName)
            .then(function (res) {
                console.log('seq is updated ', res.data);
                vm.seq = res.data;
                vm.newSeqName = res.data.TECHSEQ_NAME;
            })
            .catch(function (err) {
                console.log('update seq return ERROR!', err.data);
                Alert.showError("Ошибка при обновлении названия тех.цепочки: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })

    }

    function seqNameChanged() {
        if (!vm.seq || !vm.newSeqName) {
            return false
        }
        if (vm.seq.TECHSEQ_NAME != vm.newSeqName) {
            return true
        } else {
            return false
        }
    }

    function removeUnit(unit) {

        vm.loading = true;
        return TechUnitApi.remove(unit.TECHUNIT_ID)
            .then(function (res) {
                console.log('seq is removed ', res.data);
                var index = vm.units.indexOf(unit);
                if (index >= 0) {
                    vm.units.splice(index, 1);
                }
            })
            .catch(function (err) {
                console.log('remove unit return ERROR!', err.data);
                Alert.showError("Ошибка при удалении элемента: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })

    }

    function removeSeq() {
        if (!vm.seq) {
            return $q.when(undefined);
        }
        vm.loading = true;
        return TechseqApi.remove(vm.seq.TECHSEQ_ID)
            .then(function (res) {
                console.log('seq is removed ', res.data);
                $state.go('main.techseqs')
            })
            .catch(function (err) {
                console.log('update seq return ERROR!', err.data);
                Alert.showError("Ошибка при удалении тех.цепочки: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })

    }

    function addUnit (){
        if (!vm.newUnit || !vm.seq ) {
            return $q.when(undefined);
        }
        vm.loading = true;
        var maxN = vm.units.length;

        return TechUnitApi.add(vm.seq.TECHSEQ_ID, vm.newUnit.OPERATION_ID, maxN)
            .then(function (res) {
                console.log('unit is saved ', res.data);
                vm.units.push(res.data)
            })
            .catch(function (err) {
                console.log('add seq return ERROR!', err.data);
                Alert.showError("Ошибка при добавлении элемента тех.цепочки: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })

    }


    function saveSeq () {

        console.log(vm.units)
        vm.loading = true;

        var ids = vm.units.map(function(el){
            return el.TECHUNIT_ID
        }).join(',')

        return TechUnitApi.edit(vm.seq.TECHSEQ_ID, ids)
            .then(function (res) {
                console.log('seq is saved ', res.data);
            })
            .catch(function (err) {
                console.log('save seq return ERROR!', err.data);
                Alert.showError("Ошибка при сохранении тех.цепочки: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })
    }
}

angular.module('rControllers').controller('SeqCtrl', SeqCtrl);