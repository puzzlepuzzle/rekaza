var Excel = require("xlsx");
var fs = require('fs');
var Q = require('q');

model = {
    handle_excel: function handle_excel(excel_path) {
        return Q.fcall(function () {
            try {
                var workbook = Excel.readFile(excel_path);
                var first_sheet_name = workbook.SheetNames[0];
                return workbook.Sheets[first_sheet_name];
            }
            catch (err) {
                throw 'Не удалось обработать файл. Поддерживаемые форматы: .xlsx, .xls'
            }
            finally {
                fs.unlinkSync(excel_path);
            }
        });
    },
    getPurchase: function getPurchase(_worksheet) {
        try {
            var result = {};
            result['no'] = _worksheet["A2"].v;
            result['company'] = _worksheet["B2"].v;
            result['contact'] = _worksheet["C2"].v;
            var important_name = _worksheet["D2"].v;
            result['important'] = important_name.toUpperCase() == 'СРОЧНЫЙ' ? 1 : 0;
            return result;
        }
        catch (err) {
            throw 'Содержимое файла некорректно! Проверьте правильность заполнения данных Заказа.'
        }
    },
    getCables: function getPurchase(_worksheet, _purchase_id) {
        try {
            var cables = [];
            var rowNumber = 4
            while(rowNumber < 10000){//чтобы 100% не висел бесконечный цикл
                var _cabletype_name_cell = _worksheet["A" + rowNumber];
                var _cablestructure_name_cell = _worksheet["B" + rowNumber];
                var _vol_cell = _worksheet["C" + rowNumber];
                var _desc_cell = _worksheet["D" + rowNumber];
                if (!_cabletype_name_cell || !_cablestructure_name_cell || !_vol_cell) break;
                var _cabletype_name = _cabletype_name_cell.v;
                var _cablestructure_name = _cablestructure_name_cell.v;
                var _vol = _vol_cell.v;
                var _desc = _desc_cell ? _desc_cell.v : undefined;
                if (!_cabletype_name || !_cablestructure_name || !_vol) break;
                cables.push({
                    now : new Date().toISOString(),
                    purchase_id : _purchase_id,
                    cabletype_name : _cabletype_name,
                    cablestructure_name : _cablestructure_name,
                    vol : _vol,
                    desc : _desc
                })
                rowNumber += 1;
            }
            return cables;
        }
        catch (err) {
            throw 'Содержимое файла некорректно! Проверьте правильность заполнения данных Кабелей.'
        }
    }
}

module.exports = model;