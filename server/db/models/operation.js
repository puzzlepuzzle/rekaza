var DB = require('./../db');

var Operation = DB.Model.extend({
    tableName: 'operation',
    idAttribute: 'OPERATION_ID',

});

module.exports = Operation;