var DB = require('./../db');

var Worknorm = DB.Model.extend({
    tableName: 'worknorm',
    idAttribute: 'WORKNORM_ID',

});

module.exports = Worknorm;