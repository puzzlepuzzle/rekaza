var DB = require('./../db');

var Purchase = DB.Model.extend({
    tableName: 'purchase',
    idAttribute: 'PURCHASE_ID',

});

module.exports = Purchase;