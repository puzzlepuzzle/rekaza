'use strict';
            
function MnormsCtrl($scope, $state, $q, Alert, Authenticate,MnormApi, OperationApi, MaterialApi, CablestructureApi, ConductorApi) {
    var vm = this;

    vm.api = MnormApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Жила',
            attr: 'CONDUCTOR_ID',
            type: 'list',
            load: ConductorApi.all,
            value_attr: 'CONDUCTOR_NAME',
            nullable: true
        },
        {
            name: 'Сечение',
            attr: 'CABLESTRUCTURE_ID',
            type: 'list',
            load: CablestructureApi.all,
            value_attr: 'CABLESTRUCTURE_NAME',
            nullable: true
        },
        {
            name: 'Операция',
            attr: 'OPERATION_ID',
            type: 'list',
            load: OperationApi.all,
            value_attr: 'OPERATION_NAME'
        },
        {
            name: 'Материал',
            attr: 'MATERIAL_ID',
            type: 'list',
            load: MaterialApi.all,
            value_attr: 'MATERIAL_NAME',
            nullable: true
        },
        {
            name: 'Норма',
            attr: 'MNORM_VOL',
            type: 'float'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.MATERIAL_ID || !newObject.OPERATION_ID || !newObject.MNORM_VOL) {
            return $q.reject("Не все поля заполнены.");
        }
        var CABLESTRUCTURE_ID;
        var CONDUCTOR_ID;
        if (newObject.CABLESTRUCTURE_ID) CABLESTRUCTURE_ID = newObject.CABLESTRUCTURE_ID.CABLESTRUCTURE_ID
        if (newObject.CONDUCTOR_ID) CONDUCTOR_ID = newObject.CONDUCTOR_ID.CONDUCTOR_ID
        return vm.api.add(CABLESTRUCTURE_ID, 
                          CONDUCTOR_ID, 
                          newObject.OPERATION_ID.OPERATION_ID, 
                          newObject.MATERIAL_ID.MATERIAL_ID, 
                          newObject.MNORM_VOL)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.MNORM_ID, 
                           gridRow.CABLESTRUCTURE_ID, 
                           gridRow.CONDUCTOR_ID, 
                           gridRow.OPERATION_ID, 
                           gridRow.MATERIAL_ID, 
                           gridRow.MNORM_VOL)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.MNORM_ID)
    }

}

angular.module('rControllers').controller('MnormsCtrl', MnormsCtrl);