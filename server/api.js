var User = require("./db/models/user");
var DB = require("./db/db");
var Q = require("q");
var dateFormat = require('dateformat');
var excelHandler = require('./api/excel_handle');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var callback = function (data, response) {
    response.json(data);
};

var errback = function (err, response) {
    console.log(err, err.stack);
    response.statusCode = 440;
    response.send(err);
};

module.exports = function Api(app, passport) {

//зарегистрирован ли пользователь? если нет - errback
    var is_auth = function (req, res) {
        if (req.user && req.user.USER_ID) {
            return 1;
        }
        return 0
    };
//форматирование строки
    if (!String.prototype.db_format) {
        String.prototype.db_format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                var result = args[number];
                if (typeof result == 'string'){
                    result = result.replace(/\\/g,"\\\\");
                    result = result.replace(/'/g,"\\'");
                }
                if (typeof result != 'undefined' && result != null){
                    result = "'" + result + "'";
                } else {
                    result = 'NULL';
                }
                return result;
            });
        };
    }
    if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return args[number];
            });
        };
    }
    var get_by_raw = function (req, res, isCall, onlyFirst, raw_func) {
        var raw = raw_func(req);
        console.log(raw);
        return DB.knex.raw(raw)
            .then(function (result) {
                var _result;
                var _error;
                // get result
                if (isCall && onlyFirst)
                    _result = result[0][0][0];
                else if (!isCall && !onlyFirst)
                    _result = result[0];
                else
                    _result = result[0][0];
                // get err
                if (onlyFirst && _result && _result.ERR) {
                    _error = _result.ERR
                }
                else if (!onlyFirst && _result.length && _result[0].ERR) {
                    _error = _result[0].ERR
                }
                //console.log('raw:',raw);
                //console.log('result:',_result);
                // back
                if (_error) {
                    console.log(raw, _error);
                    throw _error;
                }
                return _result
            });
    }
// упрощение регистрации url-ов
    var simple_app_req = function (reqtype, url, isCall, onlyFirst, raw_func) {
        func = function (req, res, next) {
            console.log(url);

            if (!is_auth(req, res)) return errback("Вы не зарегистрировались.", res);
            return get_by_raw(req, res, isCall, onlyFirst, raw_func)
                .then(function (result) {
                    callback(result, res)
                })
                .catch(function (error) {
                    errback(error, res)
                })
        }
        if (reqtype.toUpperCase() == 'POST')
            app.post(url, func)
        else
            app.get(url, func)
    }

// routes ======================================================================

// api --------------------------------------------------------------------
    function api_purchases_plan_print (req, res, next) {
        console.log('/api/purchases/plan/print');

        if (!is_auth(req, res)) return  errback("Вы не зарегистрировались.", res);

        return get_by_raw(req, res, 1, 0, function (req) {return 'CALL PLAN_TO_MONTH({0})'.db_format(req.query.period)})
            .then(get_purchases_plan_print)
            .then(function (result) {
                //callback(result, res)
                console.log('plantomonth',JSON.stringify(result, null, 2))
                res.render('plantomonth', {data:result.result, materials: result.materials,
                    period: req.query.period});
            })
            .catch(function (error) {
                console.log(error, error.stack);
                errback(error, res);
            })
    }
    function api_mplan_detail_print (req, res, next) {
        console.log('/api/mplan/date/print');

        if (!is_auth(req, res)) return  errback("Вы не зарегистрировались.", res);

        return get_by_raw(req, res, 1, 0, api_mplan_date)
            .then(get_mplan_detail_print)
            .then(function (result) {
                //callback(result, res)
                console.log(JSON.stringify(result, null, 2))
                res.render('taskondetails', {data:result,
                    from: dateFormat(new Date(req.query.from), "dd.mm.yyyy"),
                    to: dateFormat(new Date(req.query.to), "dd.mm.yyyy")});
            })
            .catch(function (error) {
                console.log(error, error.stack);
                errback(error, res);
            })
    }
    function get_purchases_plan_print (mass){
        var materials = {};
        var result = [];
        //--------------------------------
        for1 : for (var i = 0; i < mass.length; i++) {
            var row = mass[i];
            var res_row_id = -1;
            var res_obj_id = -1;
            var res_m_id = -1;
            var res_cable_id = -1;
            // проверяем есть ли запись с таким же станком в результатах
            for2 : for (var j = 0; j < result.length; j++) {
                var res_row = result[j]
                if (row['PURCHASE_ID'] == res_row.id) {
                    res_row_id = j;
                    break for2;
                }
            }
            // если нет в результатах станков
            if (res_row_id == -1) {
                result.push({
                    id:             row['PURCHASE_ID'],
                    no:             row['PURCHASE_NO'],
                    regdate:        row['PURCHASE_REGDATE'],
                    company:        row['PURCHASE_COMPANY'],
                    cables: []
                })
                res_row_id = result.length - 1;
            }
            //есть кабель?
            for2 : for (var j = 0; j < result[res_row_id].cables.length; j++) {
                cable = result[res_row_id].cables[j]
                if (row['CABLE_ID'] == cable.id) {
                    res_cable_id = j;
                    break for2;
                }
            }
            //если еще нет, то добавляем
            if (res_cable_id == -1) {
                result[res_row_id].cables.push({
                    id:            row['CABLE_ID'],
                    no:            row['CABLE_ID'],
                    vol:           parseFloat(row['CABLE_VOL']),
                    desc:          row['CABLE_DESC'],
                    from:          row['CABLE_FROM'],
                    to:            row['CABLE_TO'],
                    structure:     row['CABLESTRUCTURE_NAME'],
                    type:          row['CABLETYPE_NAME'],
                    objects:       []
                });
                res_cable_id = result[res_row_id].cables.length - 1;
            }
            function tmp(val){ res = isNaN(val) || val == null? '' : val; return res;}
            for2 : for (var j = 0; j < result[res_row_id].cables[res_cable_id].objects.length; j++) {
                obj = result[res_row_id].cables[res_cable_id].objects[j]
                if (tmp(row['CONDUCTOR_ID']) == obj.id) {
                    res_obj_id = j;
                    break for2;
                }
            }
            //если еще нет, то добавляем
            if (res_obj_id == -1) {
                result[res_row_id].cables[res_cable_id].objects.push({
                    id:        tmp(row['CONDUCTOR_ID']),
                    name:      tmp(row['CONDUCTOR_MATERIAL']),
                    vol:       tmp(parseFloat(row['CONDUCTOR_VOL'])),
                    squere:    tmp(parseFloat(row['CONDUCTOR_SQUERE'])),
                    count:     tmp(parseInt(row['CABLESPART_COUNT'])),
                    materials: []
                });
                res_obj_id = result[res_row_id].cables[res_cable_id].objects.length - 1;
            }
            materials[row['MATERIAL_ID']] = row['MATERIAL_NAME'];
            for2 : for (var j = 0; j < result[res_row_id].cables[res_cable_id].objects[res_obj_id].materials.length; j++) {
                obj = result[res_row_id].cables[res_cable_id].objects[res_obj_id].materials[j]
                if (row['MATERIAL_ID'] == obj.id) {
                    res_m_id = j;
                    break for2;
                }
            }
            //если еще нет, то добавляем
            if (res_m_id == -1) {
                result[res_row_id].cables[res_cable_id].objects[res_obj_id].materials.push({
                    id:        row['MATERIAL_ID'],
                    name:      row['MATERIAL_NAME'],
                    vol:       0,
                    norm:      tmp(parseFloat(row['MNORM_VOL']))
                });
                res_m_id = result[res_row_id].cables[res_cable_id].objects[res_obj_id].materials.length - 1;
            }
            result[res_row_id].cables[res_cable_id].objects[res_obj_id].materials[res_m_id].vol += row['MPMATERIAL_VOL'];
        }
        //--------------------------------
        return {result: result, materials: materials};
    }
    function get_mplan_detail_print (mass){
        //console.log('get mass ro DEATIL: ', mass);
        var result = [];
        //--------------------------------
        for1 : for (var i = 0; i < mass.length; i++) {
            var row = mass[i];
            var res_row_id = -1;
            // проверяем есть ли запись с таким же станком в результатах
            for2 : for (var j = 0; j < result.length; j++) {
                var res_row = result[j]
                if (row['MACHINE_ID'] == res_row.machine_id) {
                    res_row_id = j;
                    break for2;
                }
            }
            // если нет в результатах станков
            if (res_row_id == -1) {
                result.push({
                    machine_id: row['MACHINE_ID'],
                    machine_name: row['MACHINE_NAME'],
                    mplans: []
                })
                res_row_id = result.length - 1;
            }
            result[res_row_id].mplans.push({
                order_no:     row['PURCHASE_NO'],
                vol:          parseFloat(row['OBJ_VOL']),
                cable_desc:   row['CABLE_DESC'],
                obj_name:     row['OBJ_NAMES'],
                cabltype:     row['CABLETYPE_NAME'],
                structure:    row['CABLESTRUCTURE_NAME'],
                operation:    row['OPERATION_NAME'],
                duration:     parseFloat(row['MPLAN_DURATION']),
                norm_vol:     parseFloat(row['WORKNORM_VOL'])
            });
        }
        //--------------------------------
        return result;
    }
    function api_detail_print(req, res, next) {
        console.log('/api/detail');

        if (!is_auth(req, res)) return errback("Вы не зарегистрировались.", res);

        return get_by_raw(req, res, 1, 0, function (req) {
            return 'CALL DETAIL_ITM({0})'.db_format(req.query.id)
        })
            .then(api_get_print_detail)
            .then(function (result) {
                //callback(result, res)
                console.log(JSON.stringify(result, null, 2))
                res.render('naryad', {data:result, detail_id:req.query.id});
            })
            .catch(function (error) {
                console.log(error, error.stack);
                errback(error, res);
            })
    }

    function api_add_by_excel(req, res, next) {
        console.log('/api/purchase/add/excel');
        var msg, purchase_no, cables_count;
        if (!is_auth(req, res)) return errback("Вы не зарегистрировались.", res);
        console.log(req.files)
        return excelHandler.handle_excel(req.files.file.path)
            .then(function (worksheet) {
                //console.log('result',worksheet)
                if (!worksheet){
                    throw 'Неопознанный формат данных'
                }
                _worksheet = worksheet;
                var purchase = excelHandler.getPurchase(_worksheet);
                console.log('purchase',purchase)
                return purchase;
            }).then(function (purchase) {
                console.log('purchase TO add by excel', purchase);
                return get_by_raw(purchase, res, 1, 1, function (purchase) {
                    return 'CALL PURCHASE_ADD({0},{1},{2},{3})'.db_format(purchase.company, purchase.contact, purchase.important, purchase.no)
                })
            }).then(function (purchase) {
                console.log('purchase add by excel', purchase);
                purchase_no = purchase.PURCHASE_NO;
                return excelHandler.getCables(_worksheet, purchase.PURCHASE_ID);
            }).then(function (cables) {
                var promise_chain = Q.fcall(function () {
                });
                cables_count = cables.length;
                for (var i = 0; i < cables.length; i++) {
                    (function (cable) {
                        console.log('cable TO add by excel', cable);
                        var promise_link = function () {
                            return get_by_raw(cable, res, 1, 1, function (cable) {
                                return 'CALL CABLE_ADD2({0},{1},{2},{3},{4},{5})'.db_format(
                                    cable.cabletype_name, cable.techseq_id, cable.purchase_id,
                                    cable.cablestructure_name, cable.vol, cable.desc)
                            })
                                .then(function (result) {
                                    console.log('cable add by excel', result);
                                })
                        }
                        // add the link onto the chain
                        promise_chain = promise_chain.then(promise_link);
                    })(cables[i])
                }
                return promise_chain
            })
            .then(function(){
                msg = "Создан заказ №{0}. Количество добавленных кабелей: {1}".format(purchase_no, cables_count)
                callback(msg,res);
            })
            .catch(function(err){
                return get_by_raw(null, res, 0, 0, function (cable) {
                    return 'ROLLBACK';
                })
                .finally(function () {
                    errback(err, res);
                })
            })

    }
    function api_get_print_detail(mass) {
        //console.log('get mass ro DEATIL: ', mass);
        var result = [];
        //--------------------------------
        for1 : for (var i = 0; i < mass.length; i++) {
            var row = mass[i];
            var res_row_id = -1;
            var res_mplan_id = -1;
            var res_order_id = -1;
            var res_material_id = -1;
            // проверяем есть ли запись с такой же операцией и станком в результатах
            for2 : for (var j = 0; j < result.length; j++) {
                var res_row = result[j]
                if (row['OPERATION_ID'] == res_row.operation_id && row['MACHINE_ID'] == res_row.machine_id) {
                    res_row_id = j;
                    for3 : for (var n = 0; n < res_row.mplans.length; n++) {
                        res_mplan = res_row.mplans[n];
                        if (row['MPLAN_ID'] == res_mplan.mplan_id) {
                            res_mplan_id = n;
                            break for2;
                        }
                    }
                    break for2;
                }
            }
            // если нет в результатах станков и операций
            if (res_row_id == -1) {
                result.push({
                    operation_id: row['OPERATION_ID'],
                    operation_name: row['OPERATION_NAME'],
                    machine_id: row['MACHINE_ID'],
                    machine_name: row['MACHINE_NAME'],
                    mplans: []
                })
                res_row_id = result.length - 1;
            }
            // если нет в результатах mplan-ов
            if (res_mplan_id == -1) {
                result[res_row_id].mplans.push({
                    mplan_id: row['MPLAN_ID'],
                    from: row['MPLAN_FROM'],
                    to: row['MPLAN_TO'],
                    orders: [],
                    materials: []
                })
                res_mplan_id = result[res_row_id].mplans.length - 1;
            }
            //есть заказ?
            for2 : for (var j = 0; j < result[res_row_id].mplans[res_mplan_id].orders.length; j++) {
                order = result[res_row_id].mplans[res_mplan_id].orders[j]
                if (row['ORDER_NAME'] == order.order) {
                    res_order_id = j;
                    break for2;
                }
            }
            //если еще нет, то добавляем
            if (res_order_id == -1) {
                result[res_row_id].mplans[res_mplan_id].orders.push({
                    order: row['ORDER_NAME'],
                    cable: row['CABLE_NAME'],
                    cable_vol: parseFloat(row['CABLE_VOL']),
                });
                res_order_id = result[res_row_id].mplans[res_mplan_id].orders.length - 1;
            }
            //есть материал?
            for2 : for (var j = 0; j < result[res_row_id].mplans[res_mplan_id].materials.length; j++) {
                material = result[res_row_id].mplans[res_mplan_id].materials[j]
                if (row['MATERIAL_NAME'] == material.material) {
                    res_material_id = j;
                    break for2;
                }
            }
            //если еще нет, то добавляем
            if (res_material_id == -1) {
                result[res_row_id].mplans[res_mplan_id].materials.push({
                    material: row['MATERIAL_NAME'],
                    material_vol: 0
                });
                res_material_id = result[res_row_id].mplans[res_mplan_id].materials.length - 1;
            }
            result[res_row_id].mplans[res_mplan_id].materials[res_material_id].material_vol += parseFloat(row['MATERIAL_VOL']);
        }
        //--------------------------------
        return result;
    }

    function api_mplan_date(req) {
        return 'CALL DATE_MPLAN_LST({0},{1})'.db_format(req.query.from,req.query.to)
    }
    // app.get/post
    app.get('/api/mplan/date/print',     api_mplan_detail_print);
    app.get('/api/purchases/plan/print', api_purchases_plan_print);
    //app.get('/api/detail/print',               api_detail_print);
    app.post('/api/purchase/add/excel',  multipartMiddleware, api_add_by_excel);
    //simple_app_req(GET/POST,    URL,          ISCALL, ONLYFIRST,    function(request){return raw})
    simple_app_req('GET' ,'/api/purchases',                1, 0, function(req) { return 'CALL PURCHASE_LST()'});
    simple_app_req('GET' ,'/api/purchase',                 1, 1, function(req) { return 'CALL PURCHASE_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/purchase/delete',          1, 1, function(req) { return 'CALL PURCHASE_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/purchase/add',             1, 1, function(req) { return 'CALL PURCHASE_ADD({0},{1},{2},{3})'.db_format(req.body.company,req.body.contact,req.body.important, req.body.no)});
    simple_app_req('POST','/api/purchase/edit',            1, 1, function(req) { return 'CALL PURCHASE_EDIT({0},{1},{2},{3},{4},{5})'.db_format(req.body.id, req.body.company,req.body.contact, req.body.important, req.body.confirm, req.body.no)});

    //simple_app_req('GET' ,'/api/cable/mplan',              1, 0, function(req) { return 'CALL CABLE_MPLAN_LST({0})'.db_format(req.query.id)});

    simple_app_req('GET' ,'/api/details',                  1, 0, function(req) { return 'CALL DETAIL_LST({0},{1})'.db_format(req.query.from,req.query.to)});
    simple_app_req('GET' ,'/api/detail',                   1, 0, function(req) { return 'CALL DETAIL_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/detail/add',               1, 0, function(req) { return 'CALL DETAIL_ADD({0},{1},{2})'.db_format(req.body.date, req.body.detailtype_id, req.body.machine_id)});
    simple_app_req('POST','/api/detail/edit',              1, 0, function(req) { return 'CALL DETAIL_EDIT({0},{1},{2},{3},{4})'.db_format(req.body.id, req.body.date, req.body.detailtype_id, req.body.machine_id, req.body.done)});
    simple_app_req('POST','/api/detail/mplans/set',        1, 0, function(req) { return 'CALL DETAIL_SET_MPLANSTACK({0},{1})'.db_format(req.body.detail_id, req.body.ids_string)});
    
    simple_app_req('GET' ,'/api/mplan/date',               1, 0, api_mplan_date);
    simple_app_req('GET' ,'/api/mplans',                   1, 0, function(req) { return 'CALL MPLAN_LST()'});
    simple_app_req('GET' ,'/api/mplan',                    1, 1, function(req) { return 'CALL MPLAN_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/mplan/delete',             1, 1, function(req) { return 'CALL MPLAN_DEL({0})'.db_format(req.body.id)});
    //simple_app_req('POST','/api/mplan/add',                1, 1, function(req) { return 'CALL MPLAN_ADD({0},{1},{2},{3},{4},{5},{6})'.db_format(req.body.detail_id,req.body.machine_id,req.body.pmplan_id,req.body.from,req.body.duration,req.body.operation_id,req.body.important)});
    simple_app_req('POST','/api/mplan/edit',               1, 1, function(req) { return 'CALL MPLAN_EDIT({0},{1},{2},{3})'.db_format(req.body.id, req.body.done, req.body.duration, req.body.vol)});
    simple_app_req('POST','/api/mplan/done',               1, 1, function(req) { return 'CALL MPLAN_DONE({0},{1})'.db_format(req.body.id, req.body.done)});


    simple_app_req('GET' ,'/api/machines',                 1, 0, function(req) { return 'CALL MACHINE_LST()'});
    simple_app_req('GET' ,'/api/machine',                  1, 1, function(req) { return 'CALL MACHINE_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/machine/delete',           1, 1, function(req) { return 'CALL MACHINE_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/machine/add',              1, 1, function(req) { return 'CALL MACHINE_ADD({0})'.db_format(req.body.name)});
    simple_app_req('POST','/api/machine/edit',             1, 1, function(req) { return 'CALL MACHINE_EDIT({0},{1})'.db_format(req.body.id, req.body.name)});

    simple_app_req('GET' ,'/api/machine/mplanstack',       1, 0, function(req) { return 'CALL MACHINE_MPLANSTACK_LST({0})'.db_format(req.query.id)});

    simple_app_req('GET' ,'/api/cables',                   1, 0, function(req) { return 'CALL CABLE_LST()'});

    simple_app_req('GET' ,'/api/purchase/cables',          1, 0, function(req) { return 'CALL PURCHASE_CABLE_LST ({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/purchase/cable/edit',      1, 1, function(req) { return 'CALL CABLE_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.done, req.body.desc)});
    simple_app_req('POST','/api/purchase/cable/apply',     1, 1, function(req) { return 'CALL CABLE_APPLY({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/purchase/cable/add',       1, 1, function(req) { return 'CALL CABLE_ADD({0},{1},{2},{3},{4},{5})'.db_format(req.body.cabletype_id, req.body.techseq_id, req.body.purchase_id, req.body.cablestructure_id, req.body.vol, req.body.desc)});
    simple_app_req('POST','/api/purchase/cable/delete',    1, 1, function(req) { return 'CALL CABLE_DEL({0})'.db_format(req.body.id)});

    simple_app_req('GET' ,'/api/operations',               1, 0, function(req) { return 'CALL OPERATION_LST()'});
    simple_app_req('GET' ,'/api/operation_types',          1, 0, function(req) { return 'CALL OPERATIONTYPE_LST()'});
    simple_app_req('GET' ,'/api/operation',                1, 1, function(req) { return 'CALL OPERATION_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/operation/delete',         1, 1, function(req) { return 'CALL OPERATION_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/operation/add',            1, 1, function(req) { return 'CALL OPERATION_ADD({0},{1})'.db_format(req.body.name,req.body.type)});
    simple_app_req('POST','/api/operation/edit',           1, 1, function(req) { return 'CALL OPERATION_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.name,req.body.type)});

    simple_app_req('GET' ,'/api/materials',                1, 0, function(req) { return 'CALL MATERIAL_LST()'});
    simple_app_req('GET' ,'/api/material',                 1, 1, function(req) { return 'CALL MATERIAL_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/material/delete',          1, 1, function(req) { return 'CALL MATERIAL_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/material/add',             1, 1, function(req) { return 'CALL MATERIAL_ADD({0},{1})'.db_format(req.body.name,req.body.vol)});
    simple_app_req('POST','/api/material/edit',            1, 1, function(req) { return 'CALL MATERIAL_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.name,req.body.vol)});

    simple_app_req('GET' ,'/api/cablestructures',          1, 0, function(req) { return 'CALL CABLESTRUCTURE_LST()'});
    simple_app_req('GET' ,'/api/cablestructure',           1, 1, function(req) { return 'CALL CABLESTRUCTURE_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/cablestructure/delete',    1, 1, function(req) { return 'CALL CABLESTRUCTURE_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/cablestructure/add',       1, 1, function(req) { return 'CALL CABLESTRUCTURE_ADD({0})'.db_format(req.body.name)});
    simple_app_req('POST','/api/cablestructure/edit',      1, 1, function(req) { return 'CALL CABLESTRUCTURE_EDIT({0},{1})'.db_format(req.body.id, req.body.name)});

    simple_app_req('GET' ,'/api/realcsparts',              1, 0, function(req) { return 'CALL REALCSPART_LST({0})'.db_format(req.query.cablestructure_id)});
    simple_app_req('GET' ,'/api/realcspart',               1, 1, function(req) { return 'CALL REALCSPART_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/realcspart/delete',        1, 1, function(req) { return 'CALL REALCSPART_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/realcspart/add',           1, 1, function(req) { return 'CALL REALCSPART_ADD({0},{1},{2},{3})'.db_format(req.body.cspart_id,req.body.cablestructure_id,req.body.count,req.body.color_id)});
    simple_app_req('POST','/api/realcspart/edit',          1, 1, function(req) { return 'CALL REALCSPART_EDIT({0},{1})'.db_format(req.body.id, req.body.color_id)});

    simple_app_req('GET' ,'/api/realcspart/cnds',          1, 0, function(req) { return 'CALL REALCSPART_REALCNDCSPART_LST({0})'.db_format(req.query.realcspart_id)});
    simple_app_req('POST','/api/realcspart/cnd/edit',      1, 1, function(req) { return 'CALL REALCNDCSPART_EDIT({0},{1})'.db_format(req.body.id,req.body.color_id)});

    simple_app_req('GET' ,'/api/cabletypes',               1, 0, function(req) { return 'CALL CABLETYPE_LST()'});
    simple_app_req('GET' ,'/api/cabletype',                1, 1, function(req) { return 'CALL CABLETYPE_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/cabletype/delete',         1, 1, function(req) { return 'CALL CABLETYPE_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/cabletype/add',            1, 1, function(req) { return 'CALL CABLETYPE_ADD({0},{1})'.db_format(req.body.name,req.body.techseq_id)});
    simple_app_req('POST','/api/cabletype/edit',           1, 1, function(req) { return 'CALL CABLETYPE_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.name,req.body.techseq_id)});

    simple_app_req('GET' ,'/api/conductors',               1, 0, function(req) { return 'CALL CONDUCTOR_LST()'});
    simple_app_req('GET' ,'/api/conductor',                1, 1, function(req) { return 'CALL CONDUCTOR_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/conductor/delete',         1, 1, function(req) { return 'CALL CONDUCTOR_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/conductor/add',            1, 1, function(req) { return 'CALL CONDUCTOR_ADD({0},{1})'.db_format(req.body.squere,req.body.material_id)});
    simple_app_req('POST','/api/conductor/edit',           1, 1, function(req) { return 'CALL CONDUCTOR_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.squere,req.body.material_id)});

    simple_app_req('GET' ,'/api/mnorms',                   1, 0, function(req) { return 'CALL MNORM_LST()'});
    simple_app_req('GET' ,'/api/mnorm',                    1, 1, function(req) { return 'CALL MNORM_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/mnorm/delete',             1, 1, function(req) { return 'CALL MNORM_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/mnorm/add',                1, 1, function(req) { return 'CALL MNORM_ADD({0},{1},{2},{3},{4})'.db_format(req.body.cablestructure_id,req.body.conductor_id,req.body.operation_id,req.body.material_id,req.body.vol)});
    simple_app_req('POST','/api/mnorm/edit',               1, 1, function(req) { return 'CALL MNORM_EDIT({0},{1},{2},{3},{4},{5})'.db_format(req.body.id, req.body.cablestructure_id,req.body.conductor_id,req.body.operation_id,req.body.material_id,req.body.vol)});

    simple_app_req('GET' ,'/api/worknorms',                1, 0, function(req) { return 'CALL WORKNORM_LST()'});
    simple_app_req('GET' ,'/api/worknorm',                 1, 1, function(req) { return 'CALL WORKNORM_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/worknorm/delete',          1, 1, function(req) { return 'CALL WORKNORM_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/worknorm/add',             1, 1, function(req) { return 'CALL WORKNORM_ADD({0},{1},{2},{3},{4},{5})'.db_format(req.body.cablestructure_id,req.body.conductor_id,req.body.machine_id,req.body.operation_id,req.body.cabletype_id,req.body.time)});
    simple_app_req('POST','/api/worknorm/edit',            1, 1, function(req) { return 'CALL WORKNORM_EDIT({0},{1},{2},{3},{4},{5},{6})'.db_format(req.body.id, req.body.cablestructure_id,req.body.conductor_id,req.body.machine_id,req.body.operation_id,req.body.cabletype_id,req.body.time)});

    simple_app_req('GET' ,'/api/techseqs',                 1, 0, function(req) { return 'CALL TECHSEQ_LST()'});
    simple_app_req('GET' ,'/api/techseq',                  1, 1, function(req) { return 'CALL TECHSEQ_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/techseq/delete',           1, 1, function(req) { return 'CALL TECHSEQ_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/techseq/add',              1, 1, function(req) { return 'CALL TECHSEQ_ADD({0})'.db_format(req.body.name)});
    simple_app_req('POST','/api/techseq/edit',             1, 1, function(req) { return 'CALL TECHSEQ_EDIT({0},{1})'.db_format(req.body.id, req.body.name)});

    simple_app_req('GET' ,'/api/csparts',                  1, 0, function(req) { return 'CALL CSPART_LST()'});
    simple_app_req('POST','/api/cspart/delete',            1, 1, function(req) { return 'CALL CSPART_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/cspart/add',               1, 1, function(req) { return 'CALL CSPART_ADD({0})'.db_format(req.body.conductors_ids_string)});

    simple_app_req('GET' ,'/api/techseq/units',            1, 0, function(req) { return 'CALL TECHSEQ_TECHUNIT_LST({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/techseq/units/edit',       1, 0, function(req) { return 'CALL TECHSEQ_TECHUNIT_EDIT({0},{1})'.db_format(req.body.techseq_id, req.body.ids_string)});
    simple_app_req('POST','/api/techseq/unit/delete',      1, 1, function(req) { return 'CALL TECHUNIT_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/techseq/unit/add',         1, 1, function(req) { return 'CALL TECHUNIT_ADD({0},{1},{2})'.db_format(req.body.techseq_id,req.body.operation_id,req.body.n)});
    simple_app_req('POST','/api/techseq/unit/edit',        1, 1, function(req) { return 'CALL TECHUNIT_EDIT({0},{1})'.db_format(req.body.id, req.body.n)});

    simple_app_req('GET' ,'/api/colors',                   1, 0, function(req) { return 'CALL COLOR_LST()'});
    simple_app_req('GET' ,'/api/color',                    1, 1, function(req) { return 'CALL COLOR_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/color/delete',             1, 1, function(req) { return 'CALL COLOR_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/color/add',                1, 1, function(req) { return 'CALL COLOR_ADD({0},{1})'.db_format(req.body.name,req.body.abbr)});
    simple_app_req('POST','/api/color/edit',               1, 1, function(req) { return 'CALL COLOR_EDIT({0},{1},{2})'.db_format(req.body.id, req.body.name,req.body.abbr)});

    simple_app_req('GET' ,'/api/user' ,                    1, 1, function(req) { return 'CALL USER_ITM({0})'.db_format(req.query.id)});
    simple_app_req('POST','/api/user/delete',              1, 1, function(req) { return 'CALL USER_DEL({0})'.db_format(req.body.id)});
    simple_app_req('POST','/api/user/edit',                1, 1, function(req) { return 'CALL USER_EDIT({0},{1},{2},{3},{4})'.db_format(req.body.id,req.body.login,new User().generateHash(req.body.password),req.body.disabled,req.body.role_id)});
    simple_app_req('GET' ,'/api/users',                    1, 0, function(req) { return 'CALL USER_LST()'});
    simple_app_req('POST','/api/register',                 1, 1, function(req) { return 'CALL USER_ADD({0},{1},{2},{3})'.db_format(req.body.login,new User().generateHash(req.body.password),req.body.disabled,req.body.role_id)});
    //справочники
    simple_app_req('GET' ,'/api/roles',                    0, 0, function(req) { return 'SELECT * FROM roles'});
    simple_app_req('GET' ,'/api/detailtypes',              0, 0, function(req) { return 'SELECT * FROM detailtype'});


    app.post('/api/login', function (req, res, next) {
            passport.authenticate('login', function (err, user, info) {
                if (err) {
                    return next(err)
                }
                if (!user) {
                    return errback({message: info.message}, res);
                }
                req.logIn(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    return callback({message: info.message}, res);
                });
            })(req, res, next);
        }
    );

    app.get('/api/logout', function (req, res) {
        console.log('/api/logout');
        req.logout();

        return callback("logout ok", res);
    });


    app.get('/api/check_auth', function (req, res, next) {
        // if user is authenticated in the session, carry on
        var r = {
            isAuth: req.isAuthenticated(),
            role: req.isAuthenticated() ? req.user.ROLE_ID : "",
            userLogin: req.isAuthenticated() ? req.user.USER_LOGIN : "",
            userId: req.isAuthenticated() ? req.user.USER_ID : ""
        }

        if (req.isAuthenticated() && req.user.DISABLED) {
            console.log('user ' + req.user.USER_LOGIN + ' is disabled!')
            req.logout();
            r.isAuth = false;
        }

        console.log('/api/check_auth', r)
        callback(r, res);
    });

}
