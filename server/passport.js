
var LocalStrategy = require('passport-local').Strategy;

// load up the user model
var User = require('./db/models/user');

// expose this function to our app using module.exports
module.exports = function (passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user_id, done) {
        //console.log('serializeUser', user_id)
        done(null, user_id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (user_id, done) {
        //console.log('deserializeUser', user_id)
        new User({user_id: user_id}).fetch().then(function(user) {
            if (user){
                done(null, user.toJSON());
            } else {
                done(null, null);
            }

        });
    });

    passport.use('login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'login',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, username, password, done) {
            console.log("passport login ", username, password)
            // check in db if a user with username exists or not

            new User({USER_LOGIN: username}).fetch().then(function(user) {
                if(user === null) {
                    return done(null, false, {message: 'Пользователя с таким логином нет'});
                } else {
                    var valid = user.validPassword(password, user.get('USER_PASSWORD'))
                    if (!valid) {
                        return done(null, false, {message: 'Неправильный пароль.'});
                    }
                    if (user.get('DISABLED')) {
                        return done(null, false, {message: 'Вы отключены от системы!'});
                    }
                    return done(null, user.get('USER_ID'), {message: 'Успешный вход.'});
                }
            });
        }));
}
;
