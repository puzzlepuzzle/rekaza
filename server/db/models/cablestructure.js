var DB = require('./../db');

var CableStructure = DB.Model.extend({
    tableName: 'cablestructure',
    idAttribute: 'CABLESTRUCTURE_ID',

});

module.exports = CableStructure;