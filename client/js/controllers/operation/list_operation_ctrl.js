'use strict';

function OperationsCtrl($scope, $state, $q, Alert, Authenticate,OperationApi) {
    var vm = this;

    vm.api = OperationApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Название',
            attr: 'OPERATION_NAME',
            type: 'text'
        },
        {
            name: 'Тип',
            attr: 'OPERATIONTYPE_ID',
            type: 'list',
            load: OperationApi.types,
            value_attr: 'OPERATIONTYPE_NAME'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.OPERATION_NAME || !newObject.OPERATION_TYPE) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.OPERATION_NAME, newObject.OPERATION_TYPE.OPERATION_TYPE)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.OPERATION_ID, gridRow.OPERATION_NAME, gridRow.OPERATION_TYPE)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.OPERATION_ID)
    }

}

angular.module('rControllers').controller('OperationsCtrl', OperationsCtrl);