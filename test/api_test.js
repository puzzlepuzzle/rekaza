//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000
var superagent = require('superagent');
var app = require('../server')
var request = require('supertest')(app);
var agent = superagent.agent();

var testAccount = {
    "login": "admin",
    "password": "1"
};

var login = function (request, done) {
    request
        .post('/api/login')
        .send(testAccount)
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            agent.saveCookies(res);
            done(agent);
        });
};

describe('API TEST', function () {

    var loginAgent;
    before(function (done) {
        login(request, function (authAgent) {
            loginAgent = authAgent;
            done();
        });
    });

    var test_api = function (mode, api, params) {
        it(api, function (done) {
            var req;
            if (mode == 'post') {
                console.log('post', params)
                req = request.post(api);
                agent.attachCookies(req);
                req.send(params).expect(200)
                    .end(function(err, res){
                        console.log(res.body)
                        console.log(err)
                        done(err)
                    });
            }
            else {
                console.log('get', api)
                req = request.get(api);
                agent.attachCookies(req);
                req.expect(200)
                    .end(function(err, res){
                        done(err)
                    });
            }
        });
    }
    before(function (done) {
        login(request, function (authAgent) {
            loginAgent = authAgent;
            done();
        });
    });
    /*
     test_api('post','/api/detail/edit', {id: 5, date: '2015-09-03', detailtype_id: 2, machine_id: 2, done: 0});
     test_api('get' ,'/api/details?from=2014-01-01&to=2016-01-01');
     */
    //test_api('get' ,'/api/detail/print?id=1');
    test_api('get' ,'/api/mplan/date/print?from=2015-09-01&to=2015-10-01');
});


