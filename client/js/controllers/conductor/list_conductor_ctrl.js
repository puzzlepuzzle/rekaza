'use strict';

function ConductorsCtrl($scope, $state, $q, Alert, Authenticate,ConductorApi, MaterialApi) {
    var vm = this;

    vm.api = ConductorApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Площадь',
            attr: 'CONDUCTOR_SQUERE',
            type: 'float'
        },
        {
            name: 'Материал',
            attr: 'MATERIAL_ID',
            type: 'list',
            load: MaterialApi.all,
            value_attr: 'MATERIAL_NAME'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.CONDUCTOR_SQUERE || !newObject.MATERIAL_ID) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.CONDUCTOR_SQUERE, newObject.MATERIAL_ID.MATERIAL_ID)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.CONDUCTOR_ID, gridRow.CONDUCTOR_SQUERE, gridRow.MATERIAL_ID)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.CONDUCTOR_ID)
    }

}

angular.module('rControllers').controller('ConductorsCtrl', ConductorsCtrl);