var DB = require('./../db');
var bcrypt = require('bcrypt-nodejs');

var User = DB.Model.extend({
    tableName: 'users',
    idAttribute: 'USER_ID',

    // generating a hash
    generateHash: function (password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    },

    // checking if password is valid
    validPassword: function (password, savedPassword) {
        //console.log('validPassword', password, savedPassword, bcrypt.compareSync(password, savedPassword))
        return bcrypt.compareSync(password, savedPassword);
    }
});

module.exports = User;