USE rekaza;

DELIMITER $$

DROP PROCEDURE IF EXISTS PROVIEW_MPLAN$$
CREATE PROCEDURE PROVIEW_MPLAN()
BEGIN

    DROP TEMPORARY TABLE IF EXISTS tt_proview_mplan;
    CREATE TEMPORARY TABLE tt_proview_mplan AS
        SELECT
                MP.MPLAN_ID,
                MP.MPLAN_NO,
                MP.MPLAN_DONE,
                IFNULL(MP.MPLAN_DURATION,MPS.MPLANSTACK_DURATION) AS MPLAN_DURATION,
                IFNULL(MP.MPLAN_VOL,MPS.MPLANSTACK_VOL) AS MPLAN_VOL,

                MPS.MPLANSTACK_ID,
                MPS.OBJ_NAME,
                MPS.MPLANSTACK_VOL,
                MPS.MPLANSTACK_DURATION,
                MPS.OPERATIONTYPE_ID,
                MPS.OPERATIONTYPE_NAME,
                MPS.OPERATION_ID,
                MPS.OPERATION_NAME,

                MPS.CABLE_ID,
                MPS.CABLE_DONE,
                MPS.CABLE_VOL,
                MPS.CABLE_DESC,
                MPS.CABLE_NAME,
                MPS.CABLESTRUCTURE_NAME,
                MPS.CABLESTRUCTURE_ID,
                MPS.CABLETYPE_NAME,
                MPS.CABLETYPE_NAME2,
                MPS.CABLETYPE_ID,
                MPS.TECHSEQ_NAME,
                MPS.PURCHASE_ID,
                MPS.PURCHASE_COMPANY,
                MPS.PURCHASE_CONTACT,
                MPS.PURCHASE_NO,
                MPS.PURCHASE_REGDATE,
                MPS.PURCHASE_CONFIRM,
                MPS.PURCHASE_IMPORTANT,

                MPS.MACHINE_ID,
                MPS.MACHINE_NAME,

                MPS.WORKNORM_TIME,
                MPS.WORKNORM_VOL,

                MP.DETAIL_ID
            FROM
                mplan MP
                JOIN view_mplanstack MPS
                    ON MP.MPLANSTACK_ID = MPS.MPLANSTACK_ID;
    CREATE INDEX IDX_tt_proview_mplan ON tt_proview_mplan (DETAIL_ID, MPLAN_NO);
    SET @CUR_FROM = CAST(NULL AS DATETIME);
    SET @PRE_TO = CAST(NULL AS DATETIME);
    DROP TEMPORARY TABLE IF EXISTS proview_mplan;
    CREATE TEMPORARY TABLE proview_mplan AS
        SELECT
                T.*,
                DV.DETAIL_DATE,
                DV.DETAILTYPE_ID,
                DV.DETAILTYPE_NAME,
                DV.DETAIL_FROM,
                DV.DETAIL_TO,
                CAST(IF(T.MPLAN_NO = 1, @CUR_FROM := DV.DETAIL_FROM, @CUR_FROM := @PRE_TO) AS DATETIME) AS MPLAN_FROM,
                CAST(@PRE_TO := MPLAN_TO(@CUR_FROM,T.MPLAN_DURATION) AS DATETIME) AS MPLAN_TO
            FROM
                tt_proview_mplan T
                JOIN view_detail DV
                    ON T.DETAIL_ID = DV.DETAIL_ID
        ORDER BY
            T.DETAIL_ID, T.MPLAN_NO;

    DROP TEMPORARY TABLE tt_proview_mplan;


END$$
DROP PROCEDURE IF EXISTS PROVIEW_MATERIAL$$
CREATE PROCEDURE PROVIEW_MATERIAL()
BEGIN
    
    DROP TEMPORARY TABLE IF EXISTS proview_material;
    CREATE TEMPORARY TABLE proview_material AS
        SELECT 
                T.MATERIAL_ID,
                T.MATERIAL_NAME,
                T.MATERIAL_VOL,
                T.MATERIAL_USING,
                T.MATERIAL_VOL - T.MATERIAL_USING AS MATERIAL_BALANCE
            FROM 
                (SELECT
                    T.MATERIAL_ID,
                    T.MATERIAL_NAME,
                    T.MATERIAL_VOL,
                    -- IFNULL(SUM(MPCM.MPCMATERIAL_VOL),0) AS MATERIAL_USING
                    0 AS MATERIAL_USING
                FROM
                    material T
                    -- LEFT JOIN mpcmaterial MPCM
                    --     ON T.MATERIAL_ID = MPCM.MATERIAL_ID
                GROUP BY
                    T.MATERIAL_ID) T
            ;
    CREATE INDEX IDX_proview_material ON proview_material (MATERIAL_ID);

END$$
-- вход: tt_cable_lst
DROP PROCEDURE IF EXISTS PROVIEW_CABLE_DATE$$
CREATE PROCEDURE PROVIEW_CABLE_DATE()
BEGIN
    DROP TEMPORARY TABLE IF EXISTS tt_mplanstack_lst;
    CREATE TEMPORARY TABLE tt_mplanstack_lst AS
        SELECT MPLANSTACK_ID FROM view_mplanstack MPS JOIN tt_cable_lst USING(CABLE_ID);

    CALL PROVIEW_MPLANSTACK();
    CREATE INDEX IDX_proview_mplanstack_2 ON proview_mplanstack (CABLE_ID, TECHUNIT_N);
    SET @PRE_CABLE_ID = NULL;
    SET @PRE_OPERATION_NAME = NULL;
    SET @PRE_MPLANSTACK_TO = CAST(NULL AS DATETIME);
    SET @PRE_OPERCAB_ID = NULL;
    SET @PRE_OPERCAB_DONE = 1;
    DROP TEMPORARY TABLE IF EXISTS proview_cable_date;
    CREATE TEMPORARY TABLE proview_cable_date AS
        SELECT

                CABLE_ID,
                CABLE_DONE,
                CABLE_VOL,
                CABLE_DESC,
                CABLE_NAME,
                CABLESTRUCTURE_NAME,
                CABLESTRUCTURE_ID,
                CABLETYPE_NAME,
                CABLETYPE_NAME2,
                CABLETYPE_ID,
                TECHSEQ_NAME,
                TECHSEQ_ID,
                PURCHASE_ID,
                PURCHASE_COMPANY,
                PURCHASE_CONTACT,
                PURCHASE_NO,
                PURCHASE_REGDATE,
                PURCHASE_CONFIRM,
                PURCHASE_IMPORTANT,
                SUM(MPLAN_DURATION) AS CABLE_TIMEVOL,
                DATE(NOW() + INTERVAL 1.5 * TIMESTAMPDIFF(DAY,NOW(),MAX(MPLANSTACK_TO)) DAY) AS CABLE_FINDATE,
                SUM(IS_NEW_OPERCAB) AS OPERCAB_COUNT,
                SUM(IS_NEW_OPERCAB_DONE) AS OPERCAB_COUNT_DONE,
                MAX(PRE_OPERATION_NAME) AS PRE_OPERATION_NAME,
                MAX(NEXT_OPERATION_NAME) AS NEXT_OPERATION_NAME
            FROM
                (SELECT
                    CV.*,
                    MPS.MPLAN_DURATION,
                    @PRE_MPLANSTACK_TO := IF(@PRE_CABLE_ID = MPS.CABLE_ID,
                                                IF(@PRE_MPLANSTACK_TO > MPS.MPLANSTACK_FROM,
                                                    MPLAN_TO(@PRE_MPLANSTACK_TO,MPS.MPLAN_DURATION),
                                                    MPLAN_TO(MPS.MPLANSTACK_FROM,MPS.MPLAN_DURATION)),
                                                MPLAN_TO(MPS.MPLANSTACK_FROM,MPS.MPLAN_DURATION)) AS MPLANSTACK_TO,
                    IF(@PRE_OPERCAB_ID = MPS.OPERCAB_ID,0,1) AS IS_NEW_OPERCAB,
                    IF(@PRE_OPERCAB_ID = MPS.OPERCAB_ID,NULL,IF(@PRE_OPERCAB_DONE = 1 AND MPS.OPERCAB_DONE = 0, @PRE_OPERATION_NAME, NULL)) AS PRE_OPERATION_NAME,
                    IF(@PRE_OPERCAB_ID = MPS.OPERCAB_ID,NULL,IF(@PRE_OPERCAB_DONE = 1 AND MPS.OPERCAB_DONE = 0, MPS.OPERATION_NAME, NULL)) AS NEXT_OPERATION_NAME,
                    IF(@PRE_OPERCAB_ID = MPS.OPERCAB_ID,0,MPS.OPERCAB_DONE) AS IS_NEW_OPERCAB_DONE,
                    @PRE_CABLE_ID := MPS.CABLE_ID AS _CABLE_ID,
                    @PRE_OPERCAB_DONE := MPS.OPERCAB_DONE AS OPERCAB_DONE,
                    @PRE_OPERATION_NAME := MPS.OPERATION_NAME AS OPERATION_NAME,
                    @PRE_OPERCAB_ID := MPS.OPERCAB_ID AS OPERCAB_ID
                FROM
                    view_cable CV
                    JOIN proview_mplanstack MPS
                        ON CV.CABLE_ID = MPS.CABLE_ID
                ORDER BY
                    CABLE_ID,
                    TECHUNIT_N,
                    MPLANSTACK_FROM) T
            GROUP BY 
                CABLE_ID;
                
    CREATE INDEX IDX_proview_cable_date ON proview_cable_date (CABLE_ID);

END$$



-- вход: tt_mplanstack_lst
DROP PROCEDURE IF EXISTS PROVIEW_MPLANSTACK$$
CREATE PROCEDURE PROVIEW_MPLANSTACK()
BEGIN

    CALL PROVIEW_MPLAN();
    CREATE INDEX IDX_proview_mplan ON proview_mplan (MPLANSTACK_ID);

    DROP TEMPORARY TABLE IF EXISTS proview_mplanstack;
    CREATE TEMPORARY TABLE proview_mplanstack AS
        SELECT
                MPS.MPLANSTACK_ID,
                MPS.OBJ_NAME,
                MPS.MPLANSTACK_VOL,
                MPS.MPLANSTACK_DURATION,
                MPS.OPERATIONTYPE_ID,
                MPS.OPERATIONTYPE_NAME,
                MPS.OPERATION_ID,
                MPS.OPERATION_NAME,
                
                MPS.OPERCAB_ID,
                MPS.OPERCAB_DONE,

                MPS.CABLE_ID,
                MPS.CABLE_DONE,
                MPS.CABLE_VOL,
                MPS.CABLE_DESC,
                MPS.CABLE_NAME,
                MPS.CABLESTRUCTURE_NAME,
                MPS.CABLESTRUCTURE_ID,
                MPS.CABLETYPE_NAME,
                MPS.CABLETYPE_NAME2,
                MPS.CABLETYPE_ID,
                MPS.TECHSEQ_NAME,
                MPS.PURCHASE_ID,
                MPS.PURCHASE_COMPANY,
                MPS.PURCHASE_CONTACT,
                MPS.PURCHASE_NO,
                MPS.PURCHASE_REGDATE,
                MPS.PURCHASE_CONFIRM,
                MPS.PURCHASE_IMPORTANT,
                MPS.TECHUNIT_N,

                MPS.MACHINE_ID,
                MPS.MACHINE_NAME,

                MPS.WORKNORM_TIME,
                MPS.WORKNORM_VOL,

                MIN(MP.MPLAN_DONE) AS MPLAN_DONE,
                SUM(IFNULL(MP.MPLAN_DURATION,MPS.MPLANSTACK_DURATION)) AS MPLAN_DURATION,
                SUM(IFNULL(MP.MPLAN_VOL,MPS.MPLANSTACK_VOL)) AS MPLAN_VOL,

                MAX(MP.MPLAN_FROM) AS MPLAN_FROM,
                MAX(MP.MPLAN_TO) AS MPLAN_TO,

                COALESCE(MAX(MP.MPLAN_FROM),NOW() + INTERVAL 1 DAY) AS MPLANSTACK_FROM,
                MPLAN_TO(COALESCE(MAX(MP.MPLAN_FROM),NOW() + INTERVAL 1 DAY),MPS.MPLANSTACK_DURATION) AS MPLANSTACK_TO
            FROM
                tt_mplanstack_lst LST
                JOIN view_mplanstack MPS
                    ON MPS.MPLANSTACK_ID = LST.MPLANSTACK_ID
                LEFT JOIN proview_mplan MP
                    ON MP.MPLANSTACK_ID = MPS.MPLANSTACK_ID
            GROUP BY
                MPS.MPLANSTACK_ID;
    CREATE INDEX IDX_proview_mplanstack1 ON proview_mplanstack (MACHINE_ID, PURCHASE_IMPORTANT, PURCHASE_NO);

    UPDATE
        proview_mplanstack MPS
        JOIN view_mplanstack PRE_MPS
            ON MPS.MACHINE_ID = PRE_MPS.MACHINE_ID
            AND IF(MPS.PURCHASE_IMPORTANT = 1,
                PRE_MPS.PURCHASE_IMPORTANT = 1 AND MPS.PURCHASE_NO >= PRE_MPS.PURCHASE_NO,
                PRE_MPS.PURCHASE_IMPORTANT = 1 OR MPS.PURCHASE_NO >= PRE_MPS.PURCHASE_NO)
      SET
        MPS.MPLANSTACK_FROM = MPLAN_TO(MPS.MPLANSTACK_FROM,PRE_MPS.MPLANSTACK_DURATION),
        MPS.MPLANSTACK_TO = MPLAN_TO(MPS.MPLANSTACK_TO,PRE_MPS.MPLANSTACK_DURATION);

END$$

DROP PROCEDURE IF EXISTS PROVIEW_MACHINE_MPLANSTACK_AVAILABLE$$
CREATE PROCEDURE PROVIEW_MACHINE_MPLANSTACK_AVAILABLE(
    vMACHINE_ID INT
)
BEGIN
    
    DROP TEMPORARY TABLE IF EXISTS proview_machine_mplanstack_available;
    CREATE TEMPORARY TABLE proview_machine_mplanstack_available AS
        SELECT * FROM (SELECT
                MSV.MPLANSTACK_ID,
                MSV.OBJ_NAME,
                MSV.MPLANSTACK_VOL,
                W.WORKNORM_TIME * MSV.MPLANSTACK_VOL AS MPLANSTACK_DURATION,
                
                MSV.OPERATIONTYPE_ID,
                MSV.OPERATIONTYPE_NAME,
                MSV.OPERATION_ID,
                MSV.OPERATION_NAME,
                
                MSV.CABLE_ID,
                MSV.CABLE_DONE,
                MSV.CABLE_VOL,
                MSV.CABLE_DESC,
                MSV.CABLE_NAME,
                MSV.CABLESTRUCTURE_NAME,
                MSV.CABLESTRUCTURE_ID,
                MSV.CABLETYPE_NAME,
                MSV.CABLETYPE_NAME2,
                MSV.CABLETYPE_ID,
                MSV.TECHSEQ_NAME,
                MSV.PURCHASE_COMPANY,
                MSV.PURCHASE_CONTACT,
                MSV.PURCHASE_ID,
                MSV.PURCHASE_NO,
                MSV.PURCHASE_REGDATE,
                MSV.PURCHASE_CONFIRM,
                MSV.PURCHASE_IMPORTANT,
                
                W.WORKNORM_TIME,
                W.WORKNORM_ID,
                W.WORKNORM_VOL
            FROM
                view_mplanstack MSV
                JOIN worknorm W
                    ON  (MSV.CABLETYPE_ID      =  W.CABLETYPE_ID OR W.CABLETYPE_ID IS NULL)
                    AND (MSV.CABLESTRUCTURE_ID =  W.CABLESTRUCTURE_ID OR W.CABLESTRUCTURE_ID IS NULL)
                    AND  MSV.OPERATION_ID      =  W.OPERATION_ID
                    AND CASE MSV.OPERATIONTYPE_ID 
                            WHEN 2 THEN MSV.CONDUCTOR_ID     <=> W.CONDUCTOR_ID AND W.CSPART_ID IS NULL
                            WHEN 4 THEN MSV.CSPART_ID        <=> W.CSPART_ID AND W.CONDUCTOR_ID IS NULL
                            ELSE TRUE
                        END
            WHERE
                W.MACHINE_ID = vMACHINE_ID
                AND MSV.PURCHASE_CONFIRM = 1
                AND MSV.MPLANSTACK_DONE = 0
                -- AND (MSV.TECHUNIT_N = 0 OR PRE_OC.OPERCAB_DONE = 1)
            GROUP BY
                MSV.MPLANSTACK_ID) T;
  CREATE INDEX IDX_proview_machine_mplanstack_available ON proview_machine_mplanstack_available (MPLANSTACK_ID);

  UPDATE
        proview_machine_mplanstack_available MPS
        JOIN mplan MP
            ON MPS.MPLANSTACK_ID = MP.MPLANSTACK_ID
     SET
        MPS.MPLANSTACK_VOL = MPS.MPLANSTACK_VOL - MP.MPLAN_VOL;

  DELETE FROM proview_machine_mplanstack_available WHERE MPLANSTACK_VOL <= 0;


END$$
    
DELIMITER ;