'use strict';

function MaterialsCtrl($scope, $state, $q, Alert, Authenticate,MaterialApi) {
    var vm = this;

    vm.api = MaterialApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Название',
            attr: 'MATERIAL_NAME',
            type: 'text'
        },
        {
            name: 'Всего',
            attr: 'MATERIAL_VOL',
            type: 'float'
        },
        {
            name: 'В использовании',
            attr: 'MATERIAL_USING',
            type: 'float',
            disableEdit: true
        },
        {
            name: 'Остаток',
            attr: 'MATERIAL_BALANCE',
            type: 'float',
            disableEdit: true
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.MATERIAL_NAME || !newObject.MATERIAL_VOL) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.MATERIAL_NAME, newObject.MATERIAL_VOL)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.MATERIAL_ID, gridRow.MATERIAL_NAME, gridRow.MATERIAL_VOL)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.MATERIAL_ID)
    }

}

angular.module('rControllers').controller('MaterialsCtrl', MaterialsCtrl);