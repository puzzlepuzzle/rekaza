'use strict';

function ColorsCtrl($scope, $state, $q, Alert, Authenticate, ColorApi) {
    var vm = this;
    
    vm.api = ColorApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Название',
            attr: 'COLOR_NAME',
            type: 'text'
        },
        {
            name: 'Аббревиатура',
            attr: 'COLOR_ABBR',
            type: 'text'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.COLOR_NAME || !newObject.COLOR_ABBR) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.COLOR_NAME, newObject.COLOR_ABBR)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.COLOR_ID, gridRow.COLOR_NAME, gridRow.COLOR_ABBR)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.COLOR_ID)
    }

}

angular.module('rControllers').controller('ColorsCtrl', ColorsCtrl);