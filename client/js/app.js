'use strict';

/* App Module */

var rekazaApp = angular.module('rekazaApp', [
    'ui.router',
    'rControllers',
    'rServices',
    'rDirectives',
    'rFilters',
    'mgcrea.ngStrap.popover',
    'mgcrea.ngStrap.tooltip',
    'mgcrea.ngStrap.modal',
    'mgcrea.ngStrap.alert',
    'mgcrea.ngStrap.aside',
    'mgcrea.ngStrap.dropdown',
//    'ui.bootstrap',
    'ui.bootstrap.tpls',
    'ui.bootstrap.accordion',
    'ui.bootstrap.datepicker',
    'ui.bootstrap.timepicker',
//    'ui.bootstrap.tooltip',
    'ngAnimate',
    'ngSanitize',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.rowEdit',
    'ui.grid.cellNav',
    'ui.select',
    'isteven-multi-select',
    'ngFileUpload',
    'dndLists'
]);

rekazaApp.config(["$stateProvider", "$urlRouterProvider",
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        //
        // Now set up the states
        $stateProvider
            .state('main', {
                url: "",
                template: "<div ui-view></div>",
                abstract: true,
                authenticate: true,
                resolve: {
                    Authenticate: ['Authenticate',
                        function(Authenticate) {
                            console.log('RESOLVE checkAccess')
                            return Authenticate.checkAccess();
                        }
                    ]
                }
            })
            .state('main.hello', {
                url: "/",
                templateUrl: "partials/main.html",
                authenticate: true
            })

            .state('main.login', {
                url: '/login',
                templateUrl: 'partials/login.html',
                authenticate: false
            })
            .state('main.logout', {
                url: '/logout',
                redirectTo: '/login',
                authenticate: false
            })

            .state('main.orders', {
                url: '/orders',
                templateUrl: 'partials/order/orders.html',
                authenticate: true
            })
            .state('main.order', {
                url: '/order/:order_id',
                templateUrl: 'partials/order/order.html',
                authenticate: true
            })
            .state('main.new_order', {
                url: '/orders/create',
                templateUrl: 'partials/order/new_order.html',
                authenticate: true,
                roles: [1,3]
            })

            .state('main.worknorms', {
                url: '/worknorms',
                templateUrl: 'partials/worknorms.html',
                authenticate: true
            })

            .state('main.operations', {
                url: '/operations',
                templateUrl: 'partials/operation/operations.html',
                authenticate: true
            })
            .state('main.csparts', {
                url: '/csparts',
                templateUrl: 'partials/cspart/csparts.html',
                authenticate: true
            })
            .state('main.materials', {
                url: '/materials',
                templateUrl: 'partials/material/materials.html',
                authenticate: true
            })
            .state('main.conductors', {
                url: '/conductors',
                templateUrl: 'partials/conductor/conductors.html',
                authenticate: true
            })
            .state('main.cablestructures', {
                url: '/cablestructures',
                templateUrl: 'partials/cablestructure/cablestructures.html',
                authenticate: true
            })
            .state('main.cablestructure', {
                url: '/cablestructure/:cablestructure_id',
                templateUrl: 'partials/cablestructure/cablestructure.html',
                authenticate: true
            })
            .state('main.cabletypes', {
                url: '/cabletypes',
                templateUrl: 'partials/cabletype/cabletypes.html',
                authenticate: true
            })
            .state('main.techseqs', {
                url: '/techseqs',
                templateUrl: 'partials/techseq/techseqs.html',
                authenticate: true
            })
            .state('main.techseq', {
                url: '/techseq/edit/:techseq_id',
                templateUrl: 'partials/techseq/techseq.html',
                authenticate: true,
                roles: [1,2,4]
            })
            .state('main.show_techseq', {
                url: '/techseq/:techseq_id',
                templateUrl: 'partials/techseq/techseq_show.html',
                authenticate: true
            })
            .state('main.machines', {
                url: '/machines',
                templateUrl: 'partials/machine/machines.html',
                authenticate: true
            })
            .state('main.mnorms', {
                url: '/mnorms',
                templateUrl: 'partials/mnorm/mnorms.html',
                authenticate: true
            })
            .state('main.users', {
                url: '/users',
                templateUrl: 'partials/users.html',
                authenticate: true,
                roles: [1,2]
            })
            .state('main.accessdenied', {
                url: '/accessdenied',
                templateUrl: 'partials/accessdenied.html'
            })
            .state('main.error', {
                url: '/error',
                templateUrl: 'partials/error.html'
            })
            .state('main.details', {
                url: '/details',
                templateUrl: 'partials/detail/details.html',
                authenticate: true
            })
            .state('main.detail', {
                url: '/detail/:detail_id',
                templateUrl: 'partials/detail/detail.html',
                authenticate: true
            })

            .state('main.mplans', {
                url: '/mplans',
                templateUrl: 'partials/mplan/mplans.html',
                authenticate: true
            })
            .state('main.mplans_date', {
                url: '/mplans/date',
                templateUrl: 'partials/mplan/mplan_date.html',
                authenticate: true
            })
            .state('main.colors', {
                url: '/colors',
                templateUrl: 'partials/color/colors.html',
                authenticate: true
            })
    }]);

rekazaApp.run(['$rootScope', '$state',  'Authenticate',
    function ($rootScope, $state, Authenticate) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                $rootScope.toState = toState;
                $rootScope.toStateParams = toParams;
                console.log('$stateChangeStart go? ', Authenticate.initDone())
                if (Authenticate.initDone()) {
                    Authenticate.checkAccess();
                }

            }
        );
    }]);