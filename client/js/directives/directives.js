'use strict';
var rDirectives = angular.module('rDirectives', []);

angular.module('rDirectives').controller('PlanCellContr', function ($scope, $element, $attrs,
                                                                    Alert, ModalApi, MplanApi) {
        var vm = this;

        vm.click = click;
        vm.currentPeriod = null;
        vm.newPeriod = null;
        vm.removeItem = removeItem;
        vm.onLoading = $scope.onLoading ? $scope.onLoading :
            function () {
                console.log('NO ON_LOADING METHOD!')
            }

        function click() {
            if (!$scope.period) {
                console.log('NO PERIOD - NO MODAL')
                return
            }
            vm.currentPeriod = $scope.period
            vm.newPeriod = JSON.parse(JSON.stringify($scope.period))

            var haveChanges = function () {
                return JSON.stringify(params.newPeriod) != JSON.stringify(params.currentPeriod)
                    || params.operation.OPERATION_ID != params.currentPeriod.OPERATION_ID
                    || params.machine.MACHINE_ID != params.currentPeriod.MACHINE_ID
            }
            var save = function () {
                vm.onLoading({isLoading: true})
                return MplanApi.edit(params.newPeriod.MPLAN_ID, params.newPeriod.PARENT_MPLAN_ID, params.machine.MACHINE_ID,
                    null, params.newPeriod.MPLAN_FROM, params.newPeriod.MPLAN_DURATION,
                    params.operation.OPERATION_ID, params.newPeriod.MPLAN_IMPORTANT)
                    .then(function (res) {
                        console.log('period edited ', res.data);
                        return $scope.load();
                    })
                    .catch(function (err) {
                        console.log('edit period return ERROR!', err.data);
                        Alert.showError( "Ошибка при сохранении: " + err.data);
                    })
                    .finally(function () {
                        vm.onLoading({isLoading: false})
                    })


            }

            var params = {
                newPeriod: vm.newPeriod,
                currentPeriod: vm.currentPeriod,
                operations: $scope.operations,
                machines: $scope.machines,
                operation: {
                    OPERATION_ID: vm.newPeriod.OPERATION_ID,
                    OPERATION_NAME: vm.newPeriod.OPERATION_NAME,
                    OPERATION_TYPE: vm.newPeriod.OPERATION_TYPE
                },
                machine: {
                    MACHINE_ID: vm.newPeriod.MACHINE_ID,
                    MACHINE_NAME: vm.newPeriod.MACHINE_NAME,
                },
                updateDateTo: function () {
                    if (params.newPeriod.MPLAN_DURATION >= 1) {
                        var newTo = new Date(params.newPeriod.MPLAN_FROM)
                        newTo.setTime(newTo.getTime() + params.newPeriod.MPLAN_DURATION * 60 * 60 * 1000)
                        params.newPeriod.MPLAN_TO = newTo;
                    }
                },
                updateDuration: function () {
                    var to = new Date(params.newPeriod.MPLAN_TO)
                    var from = new Date(params.newPeriod.MPLAN_FROM)
                    params.newPeriod.MPLAN_DURATION = (to - from) / 36e5
                },
                changeImportant: function () {
                    params.newPeriod.MPLAN_IMPORTANT = params.newPeriod.MPLAN_IMPORTANT ? 0 : 1;
                },
                haveChanges: haveChanges,
                save: save

            }

            console.log('CLICK', $scope.period, params)
            return ModalApi.show('partials/directives/plan_cell_modal.html', params, false)
                .then(function (confirm) {
                    console.log('confirm', confirm)
                })
                .catch(function (err) {
                    console.log('err', err)
                })
        }

        function removeItem(period) {
            vm.onLoading({isLoading: true})
            return MplanApi.remove(period.MPLAN_ID)
                .then(function (res) {
                    console.log('period removed ', res.data);
                    return $scope.load();
                })
                .catch(function (err) {
                    console.log('remove period return ERROR!', err.data);
                    Alert.showError("Ошибка при удалении: " + err.data);
                })
                .finally(function () {
                    vm.onLoading({isLoading: false})
                })
        }
    }
)

angular.module('rDirectives').directive('planCell', function () {
    return {
        scope: {
            period: '=',
            machines: '=',
            operations: '=',
            onLoading: '&',
            load: '&'
        },
        restrict: 'A',
        replace: true,
        templateUrl: 'partials/directives/plan_cell.html',
    };
});

angular.module('rDirectives').directive('orderCable', function () {
    return {
        scope: {
            cable: '=',
            cableTypes: '=',
            cableStructures: '=',
            access: '&',
            create: '='
        },
        restrict: 'A',
        templateUrl: 'partials/directives/order_cable.html',
    };
});


angular.module('rDirectives').controller('EditGridContr',
    function ($scope, $element, $attrs, Alert, $q, uiGridConstants) {
        var vm = this;

        vm.dataIsLoaded = false;
        vm.data = null;
        vm.dopData = [];
        vm.isCreating = false;
        vm.newObject = null;
        vm.loading = false;

        vm.createClick = createClick;
        vm.cancelCreate = cancelCreate;
        vm.create = create;
        vm.gridOption = null;
        vm.gridApi = null;

        $scope.dopHash = [];
        $scope.removeRow = removeRow;

        load();

        function createClick() {
            vm.isCreating = true
        }

        function cancelCreate() {
            vm.newObject = null;
            vm.isCreating = false
        }

        function load() {
            if (!$scope.columns) {
                throw 'No columns'
            }
            vm.columns = $scope.columns
            console.log('columns', vm.columns)

            if (!$scope.load) {
                throw 'No method load'
            }
            vm.loading = true;
            $scope.load()
                .then(function (res) {
                    console.log('objects load ', res.data);
                    vm.data = res.data;
                })
                .catch(function (err) {
                    console.log('objects load return ERROR!', err);
                    Alert.showError("Ошибка при получении данных: "
                        + err && err.data ? err.data : err);

                })
                .then(function () {
                    return $q.all(
                        vm.columns.map(function (element) {
                            if (element.type == 'list') {
                                return element.load()
                                    .then(function (res) {
                                        if (element.nullable) {
                                            var row = {}
                                            row[element.attr] = null;
                                            row[element.value_attr] = 'Пусто';
                                            vm.dopData[element.attr] = [row];
                                        } else {
                                            vm.dopData[element.attr] = [];
                                        }
                                        vm.dopData[element.attr] = vm.dopData[element.attr].concat(res.data)
                                    })
                            }
                        })
                    )
                })
                .finally(function () {
                    console.log('vm.dopData', vm.dopData)
                    vm.loading = false;
                    initGridOptions();
                    vm.dataIsLoaded = true;
                })
        }

        function initGridOptions() {
            var columns = vm.columns.map(function (col) {
                var i = {
                    field: col.attr,
                    displayName: col.name,
                    enableHiding: false
                }
                if (col.type == 'list') {
                    var hash = {}
                    var list = vm.dopData[col.attr].map(function (el) {
                        hash[el[col.attr]] = el[col.value_attr]
                        return {id: el[col.attr], value: el[col.value_attr]}
                    })
                    $scope.dopHash[col.attr] = hash
                    i['editableCellTemplate'] = "<div class=\"grid_cell-select\">" +
                        "<form name=\"inputForm\"><select ng-class=\"'colt' + col.uid\" " +
                        "ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] " +
                        "CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></form></div>"

                    i['cellFilter'] = 'foreign_type: grid.appScope.dopHash["' + col.attr + '"]';
                    i['editDropdownOptionsArray'] = list
                    i['filter']= {
                        condition: function(searchTerm, cellValue) {
                            //console.log(searchTerm, cellValue)
                            //var cell = cellValue
                            //if (!cellValue){
                            //    cell = "_"
                            //}
                            return hash[cellValue].toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0;
                        }
                    }
                }
                if (col.disableEdit) {
                    i.enableCellEdit = false;
                } else {
                    i.cellEditableCondition = $scope.access
                }
                return i;
            })
            if ($scope.access && $scope.access()){
                columns.push(
                    {
                        name: ' ',
                        enableColumnMenu: false,
                        enableFiltering: false,
                        cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                        '<a  ng-click="grid.appScope.removeRow(row.entity)"' +
                        'ng-show="!vm.loading">' +
                        '<span class="glyphicon glyphicon-remove" style="font-size: 18px;"></span>' +
                        '</a></div>',
                        width: "30",
                        enableCellEdit: false
                    }
                )
            }

            vm.gridOptions = {
                enableFiltering: true,
                rowEditWaitInterval: 100,
                columnDefs: columns
            };

            vm.gridOptions.onRegisterApi = function (gridApi) {
                //set gridApi on scope
                vm.gridApi = gridApi
                gridApi.rowEdit.on.saveRow($scope, save);
            };

            vm.gridOptions.data = vm.data;

            console.log('INIIIIT', vm.gridOptions)
        }

        function create() {
            if (!$scope.create) {
                throw 'No method create'
            }
            vm.loading = true;
            return $scope.create({newObject: vm.newObject})
                .then(function (res) {
                    console.log('object is added ', res.data);
                    vm.data.push(res.data);
                    vm.newObject = null;
                    vm.isCreating = false
                })
                .catch(function (err) {
                    console.log('object add return ERROR!', err);
                    Alert.showError("Ошибка при добавлении: "
                        + err && err.data ? err.data : err);

                })
                .finally(function () {
                    vm.loading = false;
                })
        }

        function save(gridRow) {
            if (!$scope.save) {
                throw 'no save method'
            }
            var deferred = $q.defer();
            vm.gridApi.rowEdit.setSavePromise(gridRow, deferred.promise);

            vm.loading = true;
            return $scope.save({gridRow: gridRow})
                .then(function (res) {
                    console.log('object is saved ', res.data);
                    deferred.resolve();
                })
                .catch(function (err) {
                    console.log('object save return ERROR!', err);
                    deferred.reject();
                    Alert.showError( "Ошибка при сохранении: "
                        + err && err.data ? err.data : err);
                })
                .finally(function () {
                    vm.loading = false;
                })

        }

        function removeRow(gridRow) {
            if (!$scope.remove) {
                throw 'no remove method'
            }
            vm.loading = true;
            return $scope.remove({gridRow: gridRow})
                .then(function (res) {
                    console.log('object is removed ', res.data);
                    var index = vm.data.indexOf(gridRow);
                    console.log('index', index)
                    if (index >= 0) {
                        vm.data.splice(index, 1);
                    }
                })
                .catch(function (err) {
                    console.log('object remove return ERROR!', err);
                    Alert.showError("Ошибка при удалении: "
                        + err && err.data ? err.data : err);

                })
                .finally(function () {
                    vm.loading = false;
                })
        }
    }
)

angular.module('rDirectives').directive('editGrid', function () {
    return {
        scope: {
            title: '=',
            load: '&',
            create: '&',
            save: '&',
            remove: '&',
            access: '&',
            columns: '='
        },
        restrict: 'AE',
        templateUrl: 'partials/directives/edit_grid.html'
    };
});

angular.module('rDirectives').directive('upload', function (Alert, Upload) {
    return {
        scope: {
            apiName: '=',
            accept: '=',
            title: '=',
            callback: '&'
        },
        restrict: 'AE',
        templateUrl: 'partials/directives/upload.html',
        link: function ($scope, element, attrs) {
            $scope.loading = false
            $scope.uploadFiles = function (file) {
                console.log('UPLOAD', file, $scope.apiName)
                if (!file || !$scope.apiName) {
                    return
                }
                if (file.$error) {
                    var error = ""
                    switch (file.$error) {
                        case "pattern":
                            error = "Выбран неправильный формат. Допустимые: " + $scope.accept
                            break
                        case "maxSize":
                            error = "Слишком большой файл. Ограничение:  " + file.$errorParam
                            break
                        default:
                            error = "Выберите другой файл"
                    }
                    Alert.showError(error);
                    return
                }
                $scope.loading = true;
                file.upload = Upload.upload({
                    url: $scope.apiName,
                    file: file
                });

                file.upload.then(function (response) {
                    console.log('RESPONSE', response)
                    $scope.loading = false;
                    if ($scope.callback){
                        $scope.callback();
                    }
                    $Alert.showSuccess("Файл успешно импортирован. " + response.data);
                }, function (response) {
                    console.log('RESPONSE ERROR', response)
                    $scope.loading = false;
                    Alert.showError("Ошибка при обработке файла: " + response.data);
                });

                file.upload.progress(function (evt) {
                    console.log('PROGRESS', 100.0 * evt.loaded / evt.total)
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });

            }
        }
    };
});