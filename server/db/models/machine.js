var DB = require('./../db');

var Machine = DB.Model.extend({
    tableName: 'machine',
    idAttribute: 'MACHINE_ID',

});

module.exports = Machine;