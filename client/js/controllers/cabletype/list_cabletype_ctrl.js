'use strict';

function CabletypesCtrl($scope, $state, $q, Alert, Authenticate,CabletypeApi, TechseqApi) {
    var vm = this;

    vm.api = CabletypeApi
    vm.gridAdd = gridAdd
    vm.gridRemove = gridRemove
    vm.gridSave = gridSave
    vm.gridLoad = vm.api.all
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    vm.columns = [
        {
            name: 'Название',
            attr: 'CABLETYPE_NAME',
            type: 'text'
        },
        {
            name: 'Технологическая цепочка',
            attr: 'TECHSEQ_ID',
            type: 'list',
            load: TechseqApi.all,
            value_attr: 'TECHSEQ_NAME'
        }
    ]

    function gridAdd(newObject) {
        console.log('gridAdd',newObject )
        if (!newObject || !newObject.CABLETYPE_NAME || !newObject.TECHSEQ_ID) {
            return $q.reject("Не все поля заполнены.");
        }
        return vm.api.add(newObject.CABLETYPE_NAME, newObject.TECHSEQ_ID.TECHSEQ_ID)
    }

    function gridSave(gridRow){
        console.log('gridSave',gridRow )
        return vm.api.edit(gridRow.CABLETYPE_ID, gridRow.CABLETYPE_NAME, gridRow.TECHSEQ_ID)
    }

    function gridRemove(gridRow){
        console.log('gridRemove',gridRow )
        return vm.api.remove(gridRow.CABLETYPE_ID)
    }

}

angular.module('rControllers').controller('CabletypesCtrl', CabletypesCtrl);