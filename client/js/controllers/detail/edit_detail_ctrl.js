'use strict';

function DetailCtrl($scope, $state, $q, $stateParams, Alert, Authenticate, DetailApi, MachineApi, ModalApi, MplanApi) {
    var vm = this;
    vm.loading = false;
    vm.detail = null;
    vm.operations = [];
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.mplanOperations = []
    vm.copyMplanOperationsIds = ''
    vm.saveMplan = saveMplan;
    vm.mplanIsChanged = mplanIsChanged;
    vm.removeMplanOperation = removeMplanOperation;
    vm.editMplanOperation = editMplanOperation;
    $scope.addOperationToPlan = addOperationToPlan
    vm.access = function () {
        return Authenticate.checkRole([1, 2, 4])
    }
    vm.gridOptions1 = {}

    vm.load();

    function load() {
        if (!$stateParams.detail_id) {
            Alert.showError("Не найден id наряда.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return DetailApi.get($stateParams.detail_id)
            .then(function (res) {
                console.log('detail is reseived ', res.data);
                if (!res.data || res.data.length == 0){
                    throw {data:"Не получены данные наряда"}
                }
                vm.detail = res.data[0]
                vm.mplanOperations = res.data.filter(function(element){
                    return element.MPLANSTACK_ID
                });
                vm.copyMplanOperationsIds = vm.mplanOperations.map(function (el) {
                    return el.MPLANSTACK_ID
                }).join(',')
            })
            .then(function (res) {
                return MachineApi.mplan(vm.detail.MACHINE_ID)
                    .then(function (res) {
                        console.log('stack is received ', res.data);
                        vm.operations = res.data;
                        initGridOprions( vm.operations)
                        vm.gridOptions1.data = res.data;
                    })
                    .catch(function (err) {
                        console.log('MachineApi.mplan return ERROR!', err.data);
                        vm.operations = [];
                        Alert.showError("Ошибка при получении данных: " + err.data);
                    })
            })
            .catch(function (err) {
                console.log('get detail or operations return ERROR!', err.data);
                vm.detail = {};
                vm.mplanOperations = [];
                Alert.showError("Ошибка при получении данных: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
                vm.dataIsLoaded = true
            })

    }

    function mplanIsChanged() {
        var ids = vm.mplanOperations.map(function (el) {
            return el.MPLANSTACK_ID
        }).join(',')
        return vm.copyMplanOperationsIds != ids
    }

    function removeMplanOperation(operation) {
        console.log('moperation is removing ',confirm, operation);
        var index = vm.mplanOperations.indexOf(operation);
        if (index >= 0) {
            vm.mplanOperations.splice(index, 1);
            vm.operations.push(operation)
        } else {
            Alert.showError("Не найдена удаляемая операция")
        }
    }

    function editMplanOperation(operation) {
        var haveChanges = function (newOperation, oldOperation) {
            return  JSON.stringify(newOperation) != JSON.stringify(oldOperation)
        }
        var save = function (operation) {
            console.log('CLICK SAVE', operation)
            vm.loading = true;
            return MplanApi.edit(operation.MPLAN_ID, operation.MPLAN_DONE, operation.MPLAN_DURATION, operation.MPLAN_VOL)
                .then(function (res) {
                    console.log('MplanApi.edit return ', res.data);
                    vm.loading = false;
                    Alert.showSuccess("Операция успешно сохранена");
                })
                .catch(function (err) {
                    console.log('MplanApi.edit return ERROR!', err.data);
                    vm.loading = true;
                    Alert.showError("Ошибка при сохранении: " + err.data);
                })
        }
        var params = {
            operation:operation,
            MPLAN_FROM: new Date(operation.MPLAN_FROM),
            MPLAN_TO: new Date(operation.MPLAN_TO),
            copyOperation: JSON.parse(JSON.stringify(operation)),
            haveChanges: haveChanges,
            save:save,
            changeImportant: function () {
                params.operation.MPLAN_DONE = params.operation.MPLAN_DONE ? 0 : 1;
            },
            setTo: function () {
                if (!params.MPLAN_FROM || !params.operation || !params.operation.MPLAN_DURATION)
                    return
                params.MPLAN_TO = new Date(params.MPLAN_FROM)
                params.MPLAN_TO.setHours(params.MPLAN_TO.getHours()+params.operation.MPLAN_DURATION);
            }
        }
        console.log('CLICK', params)
        return ModalApi.show('partials/directives/plan_operation_modal.html', params, false)
            .then(function (confirm) {
                console.log('confirm', confirm)
            })
            .catch(function (err) {
                console.log('err', err)
            })
    }

    function addOperationToPlan(gridRow) {
        if (!gridRow) {
            return;
        }
        vm.mplanOperations.push(gridRow);
        var index = vm.operations.indexOf(gridRow);
        console.log('index', index)
        if (index >= 0) {
            vm.operations.splice(index, 1);
        }
    }

    function saveMplan() {
        vm.loading = true;

        var ids = vm.mplanOperations.map(function (el) {
            return el.MPLANSTACK_ID
        }).join(',')

        return DetailApi.mplan_set(vm.detail.DETAIL_ID, ids)
            .then(function (res) {
                console.log('detail is SAVED ', res.data);
                if (!res.data || res.data.length == 0){
                    throw {data:"Не получены данные наряда"}
                }
                vm.detail = res.data[0]
                vm.mplanOperations = res.data.filter(function(element){
                    return element.MPLANSTACK_ID
                });
                vm.copyMplanOperationsIds = vm.mplanOperations.map(function (el) {
                    return el.MPLANSTACK_ID
                }).join(',')
                Alert.showSuccess("План усппешно сохранен.");
            })
            .catch(function (err) {
                console.log('save mplan return ERROR!', err.data);
                Alert.showError("Ошибка при сохранении плана: " + err.data);
            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function initGridOprions(data) {
        vm.gridOptions1 = {
            data: data,
            enableFiltering: true,
            columnDefs: [
                {
                    name: " ",
                    enableColumnMenu: false,
                    enableFiltering: false,
                    cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                    '<a  ng-click="grid.appScope.addOperationToPlan(row.entity)"' +
                    'ng-show="!vm.loading">' +
                    '<span class="glyphicon glyphicon-plus" style="font-size: 18px;"></span>' +
                    '</a></div>',
                    width: "30",
                    enableCellEdit: false
                },
                { field: 'OPERATION_NAME', displayName: "Операция",
                    enableHiding: false},
                { field: 'OBJ_NAME', displayName: "Название",
                    enableHiding: false},
                { field: 'MPLANSTACK_VOL', displayName: "Количество",
                    enableHiding: false},
                { field: 'MPLANSTACK_DURATION', displayName: "Длительность, ч",
                    enableHiding: false}
            ]
        }
    }
}

angular.module('rControllers').controller('DetailCtrl', DetailCtrl);