var DB = require('./../db');

var TechSeq = DB.Model.extend({
    tableName: 'techseq',
    idAttribute: 'TECHSEQ_ID',

});

module.exports = TechSeq;