//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000

describe('DB TEST', function(){
  it('api_get_detail', function () {

    function api_get_detail(mass) {
      var result = [];
      //--------------------------------
      for1 : for (var i = 0; i < mass.length; i++){
        var row = mass[i];
        var res_row_id = -1;
        var res_mplan_id = -1;
        var res_order_id = -1;
        var res_material_id = -1;
        // проверяем есть ли запись с такой же операцией и станком в результатах
        for2 : for (var j = 0; j < result.length; j++){
          var res_row = result[j]
          if (row['OPERATION_ID'] == res_row.operation_id && row['MACHINE_ID'] == res_row.machine_id){
            res_row_id = j;
            for3 : for (var n = 0; n < res_row.mplans.length; n++){
              res_mplan = res_row.mplans[n];
              if (row['MPLAN_ID'] == res_mplan.mplan_id){
                res_mplan_id = n;
                break for2;
              }
            }
            break for2;
          }
        }
        // если нет в результатах станков и операций
        if (res_row_id == -1){
          result.push({
            operation_id : row['OPERATION_ID'],
            operation_name : row['OPERATION_NAME'],
            machine_id : row['MACHINE_ID'],
            machine_name : row['MACHINE_NAME'],
            mplans : []
          })
          res_row_id = result.length - 1;
        }
        // если нет в результатах mplan-ов
        if (res_mplan_id == -1){
          result[res_row_id].mplans.push({
            mplan_id : row['MPLAN_ID'],
            from : row['MPLAN_FROM'],
            to : row['MPLAN_TO'],
            orders : [],
            materials : []
          })
          res_mplan_id = result[res_row_id].mplans.length - 1;
        }
        //есть заказ?
        for2 : for (var j = 0; j < result[res_row_id].mplans[res_mplan_id].orders.length; j++){
          order = result[res_row_id].mplans[res_mplan_id].orders[j]
          if (row['ORDER_NAME'] == order.order){
            res_order_id = j;
            break for2;
          }
        }
        //если еще нет, то добавляем
        if (res_order_id == -1){
          result[res_row_id].mplans[res_mplan_id].orders.push({
            order : row['ORDER_NAME'],
            cable : row['CABLE_NAME'],
            cable_vol : row['CABLE_VOL'],
          });
          res_order_id = result[res_row_id].mplans[res_mplan_id].orders.length - 1;
        }
        //есть материал?
        for2 : for (var j = 0; j < result[res_row_id].mplans[res_mplan_id].materials.length; j++){
          material = result[res_row_id].mplans[res_mplan_id].materials[j]
          if (row['MATERIAL_NAME'] == material.material){
            res_material_id = j;
            break for2;
          }
        }
        //если еще нет, то добавляем
        if (res_material_id == -1){
          result[res_row_id].mplans[res_mplan_id].materials.push({
            material : row['MATERIAL_NAME'],
            material_vol : 0
          });
          res_material_id = result[res_row_id].mplans[res_mplan_id].materials.length - 1;
        }
        result[res_row_id].mplans[res_mplan_id].materials[res_material_id].material_vol += row['MATERIAL_VOL'];
      }
      //--------------------------------
      return result
    }
    var mass = [];
    mass.push({
      OPERATION_ID : 1,
      OPERATION_NAME : 'Операция 1',
      MACHINE_ID : 1,
      MACHINE_NAME : 'Станок 1',
      MPLAN_ID : 1,
      MPLAN_FROM : '2015-06-01',
      MPLAN_TO : '2015-06-02',
      ORDER_NAME : 'Заказ 1',
      CABLE_NAME : 'Кабель 1',
      CABLE_VOL : 17,
      MATERIAL_NAME : 'Медь',
      MATERIAL_VOL : 1.3
    })
    mass.push({
      OPERATION_ID : 1,
      OPERATION_NAME : 'Операция 1',
      MACHINE_ID : 1,
      MACHINE_NAME : 'Станок 1',
      MPLAN_ID : 1,
      MPLAN_FROM : '2015-06-01',
      MPLAN_TO : '2015-06-02',
      ORDER_NAME : 'Заказ 1',
      CABLE_NAME : 'Кабель 1',
      CABLE_VOL : 17,
      MATERIAL_NAME : 'Медь',
      MATERIAL_VOL : 1.9
    })
    mass.push({
      OPERATION_ID : 1,
      OPERATION_NAME : 'Операция 1',
      MACHINE_ID : 1,
      MACHINE_NAME : 'Станок 1',
      MPLAN_ID : 1,
      MPLAN_FROM : '2015-06-01',
      MPLAN_TO : '2015-06-02',
      ORDER_NAME : 'Заказ 2',
      CABLE_NAME : 'Кабель 1',
      CABLE_VOL : 17,
      MATERIAL_NAME : 'Алюминий',
      MATERIAL_VOL : 1.1
    })

    console.log(JSON.stringify(api_get_detail(mass), null, 2))

  })

  it('select now()', function () {
    var DB = require("./../server/db/db");
    return DB.knex.raw('select now() as res')
      .then(function (result) {
        console.log(result[0][0]['res'])
        console.log(new Date(result[0][0]['res']))
      })
  })
});


