USE rekaza;

-- СТАНКИ
INSERT IGNORE INTO purchase (PURCHASE_ID, PURCHASE_COMPANY, PURCHASE_CONTACT, PURCHASE_REGDATE) VALUES
(1, 'Компания 1','Тел', NOW()),
(2, 'Компания 2','Тел', NOW()),
(3, 'Компания 3','Тел', NOW()),
(4, 'Компания 4','Тел', NOW());

INSERT IGNORE INTO machine (MACHINE_ID, MACHINE_NAME) VALUES
(1,'Станок 1'),
(2,'Станок 2'),
(3,'Станок 3'),
(4,'Станок 4');

INSERT IGNORE INTO techseq (TECHSEQ_ID, TECHSEQ_NAME) VALUES
(1, 'Технологическая цепочка 1'),
(2, 'Технологическая цепочка 2');

INSERT IGNORE INTO cabletype (CABLETYPE_ID, CABLETYPE_NAME, TECHSEQ_ID) VALUES
(1,'Тип кабеля 1', 1),
(2,'Тип кабеля 2', 2);

INSERT IGNORE INTO operation (OPERATION_ID, OPERATION_NAME, OPERATION_TYPE) VALUES
(1, 'Операция 1', 2),
(2, 'Операция 2', 1),
(3, 'Операция 3', 1),
(4, 'Операция 4', 1);

INSERT IGNORE INTO techunit (TECHSEQ_ID, TECHUNIT_N, OPERATION_ID) VALUES
(1, 0, 1),
(1, 1, 3),
(1, 2, 2),

(2, 0, 2),
(2, 1, 3),
(2, 2, 4);

INSERT IGNORE INTO cablestructure (CABLESTRUCTURE_ID, CABLESTRUCTURE_NAME) VALUES
(1, 'М: 5*0.5; А: 7*1.0'),
(2, 'М: 10*0.25'),
(3, 'А: 0.25');


INSERT IGNORE INTO conductor (CONDUCTOR_ID, CONDUCTOR_SQUERE, MATERIAL_ID) VALUES
(1,  0.5,  2),
(2,  1.0,  1),
(3,  0.25, 2),
(4,  0.25,  1);

INSERT IGNORE INTO cablespart (CABLESPART_ID, CABLESPART_COUNT, CONDUCTOR_ID, CABLESTRUCTURE_ID) VALUES
(1,   5,   1,  1),
(2,   7,   2,  1),
(3,  10,   3,  2),
(4,   1,   4,  3);

INSERT IGNORE INTO worknorm (CABLESTRUCTURE_ID,CONDUCTOR_ID, MACHINE_ID,OPERATION_ID,CABLETYPE_ID,WORKNORM_VOL,WORKNORM_TIME) VALUES
-- 1
(   1,   NULL,  1,   2,   1,   16,   0.5),
(   1,   NULL,  2,   3,   1,    8,     1),
(   1,   NULL,  3,   4,   1,   12, 0.666),
    
(   1,   NULL,  1,   2,   2,   16,   0.5),
(   1,   NULL,  2,   3,   2,    8,     1),
(   1,   NULL,  3,   4,   2,   12, 0.666),
-- 2    
(   2,   NULL,  3,   2,   1,   16,   0.5),
(   2,   NULL,  4,   3,   1,    8,     1),
(   2,   NULL,  1,   4,   1,   12, 0.666),
    
(   2,   NULL,  3,   2,   2,   16,   0.5),
(   2,   NULL,  4,   3,   2,    8,     1),
(   2,   NULL,  2,   4,   2,   12, 0.666),
-- 3    
(   3,   NULL,  3,   2,   1,   16,   0.5),
(   3,   NULL,  4,   3,   1,    8,     1),
(   3,   NULL,  4,   4,   1,   12, 0.666),
    
(   3,   NULL,  2,   2,   2,   16,   0.5),
(   3,   NULL,  4,   3,   2,    8,     1),
(   3,   NULL,  4,   4,   2,   12, 0.666),
-- NULL
(NULL,      1,  1,   1,   1,  100,  0.08),
(NULL,      1,  1,   1,   2,  160,  0.05),

(NULL,      1,  1,   1,   1,  100,  0.08),
(NULL,      1,  1,   1,   2,  160,  0.05),

(NULL,      2,  1,   1,   1, 1000, 0.008),
(NULL,      2,  1,   1,   2, 1600, 0.005),
            
(NULL,      2,  1,   1,   1, 1000, 0.008),
(NULL,      2,  1,   1,   2, 1600, 0.005);

INSERT IGNORE INTO mnorm (MATERIAL_ID, CABLESTRUCTURE_ID, CONDUCTOR_ID, OPERATION_ID,MNORM_VOL) VALUES
-- 1
(1,   1,   NULL,  2,   1.5),
(1,   1,   NULL,  3,   2.1),
(1,   1,   NULL,  4,   0),

(2,   1,   NULL,  2,   3.5),
(2,   1,   NULL,  3,   0),
(2,   1,   NULL,  4,   0),

(3,   1,   NULL,  2,   0),
(3,   1,   NULL,  3,   0),
(3,   1,   NULL,  4,   4.4),
-- 2
(2,   2,   NULL,  2,   1.6),
(2,   2,   NULL,  3,   2.3),
(2,   2,   NULL,  4,   0),
      
(3,   2,   NULL,  2,   0),
(3,   2,   NULL,  3,   0),
(3,   2,   NULL,  4,   2.3),
-- 3
(1,   3,   NULL,  2,   2.5),
(1,   3,   NULL,  3,   3.1),
(1,   3,   NULL,  4,   0),
      
(3,   3,   NULL,  2,   0),
(3,   3,   NULL,  3,   0),
(3,   3,   NULL,  4,   5.4),

-- NULL
(1,   NULL,   1,  1,   0.7),

(1,   NULL,   2,  1,   0.7),

(1,   NULL,   3,  1,   0.7),

(1,   NULL,   4,  1,   0.7);


CALL CABLE_ADD(1,NULL,1, 1, 15);
CALL CABLE_ADD(2,NULL,1, 2, 27);
CALL CABLE_ADD(2,NULL,1, 3, 36);
CALL CABLE_ADD(1,NULL,2, 1, 1);
CALL CABLE_ADD(2,NULL,2, 2, 7);
CALL CABLE_ADD(2,NULL,3, 3, 6);
