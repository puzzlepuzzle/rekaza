'use strict';

function DetailsCtrl($scope, $state, $q, Alert, Authenticate, DetailApi, MachineApi) {
    var vm = this;

    vm.loading = false;
    vm.dataIsLoaded = false;
    vm.list = [];
    $scope.machines = [];
    $scope.machinesHash = {};
    $scope.detailTypes =  [];
    $scope.detailTypesHash = {}
    vm.checkRole = Authenticate.checkRole;
    vm.createDetail = createDetail;
    vm.isCreating = false;
    vm.newDetail = {};
    vm.createClick = createClick;
    vm.cancelCreate = cancelCreate;
    vm.dateFrom = null;
    vm.dateTo = null;
    vm.load = load;
    vm.gridOptions1 = {}

    startLoad();

    function initGridOprions(data) {
        vm.gridOptions1 = {
            data: data,
            enableFiltering: true,
            rowEditWaitInterval: 100,
            columnDefs: [
                {
                    field: 'DETAIL_ID', displayName: "Номер",
                    enableHiding: false,
                    cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                    '<a ui-sref="main.detail({detail_id:row.entity.DETAIL_ID})">{{COL_FIELD CUSTOM_FILTERS}}</a>' +
                    '</div>', enableCellEdit: false
                },
                {
                    field: 'DETAILTYPE_ID', displayName: "Смена",
                    enableHiding: false,
                    cellEditableCondition: Authenticate.checkRole([1, 2]),
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    cellFilter: 'foreign_type: grid.appScope.detailTypesHash',
                    editDropdownIdLabel: 'DETAILTYPE_ID',
                    editDropdownValueLabel: 'DETAILTYPE_NAME',
                    editDropdownOptionsArray: $scope.detailTypes
                },
                {
                    field: 'DETAIL_DATE',
                    displayName: "Дата",
                    cellFilter: "date:'dd.MM.yyyy'",
                    width: "150",
                    type: 'date',
                    enableHiding: false,
                    cellEditableCondition: Authenticate.checkRole([1, 2])
                },
                {
                    field: 'MACHINE_ID', displayName: "Станок",
                    enableHiding: false,
                    cellEditableCondition: Authenticate.checkRole([1, 2]),
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    cellFilter: 'foreign_type: grid.appScope.machinesHash',
                    editDropdownIdLabel: 'MACHINE_ID',
                    editDropdownValueLabel: 'MACHINE_NAME',
                    editDropdownOptionsArray: $scope.machines
                },
                {
                    field: 'DETAIL_DONE', displayName: "Закрыт",
                    enableHiding: false,
                    cellEditableCondition: Authenticate.checkRole([1, 2]), type: 'boolean',
                    cellFilter: 'yes_no',
                }
            ]
        };
        vm.gridOptions1.onRegisterApi = function (gridApi) {
            vm.gridApi = gridApi
            gridApi.rowEdit.on.saveRow($scope, save);
        };
    }


    function startLoad() {
        vm.loading = true;

        var d = new Date();
        d.setHours(0, 0, 0, 0);
        vm.dateTo = new Date(d);
        d.setDate(d.getDate() - 14)
        vm.dateFrom = new Date(d);

        return MachineApi.all()
            .catch(function (err) {
                console.log('MachineApi.all return ERROR!', err.data);
            })
            .then(function (res) {
                console.log('MachineApi.all ', res.data);
                $scope.machines = res.data;
                $scope.machinesHash = {}
                $scope.machines.map(function (el) {
                    $scope.machinesHash[el['MACHINE_ID']] = el['MACHINE_NAME']
                })
                return DetailApi.types()
            })
            .catch(function (err1) {
                console.log('DetailApi.types return ERROR!', err1.data);
            })
            .then(function (res1) {
                $scope.detailTypes = res1.data;
                $scope.detailTypesHash = {}
                $scope.detailTypes.map(function (el) {
                    $scope.detailTypesHash[el['DETAILTYPE_ID']] = el['DETAILTYPE_NAME']
                })
                console.log('DetailApi.types ', $scope.detailTypes);
                initGridOprions()
            })
            .then(function(){
                return load()
            })
            .finally(function () {
                vm.loading = false;
                vm.dataIsLoaded = true;
            })
    }

    function load() {
        if (!vm.dateFrom || !vm.dateTo) {
            console.log('not all dates');
            return;
        }
        if (vm.dateTo <= vm.dateFrom) {
            console.log('no correct data');
            return
        }

        vm.loading = true;
        return DetailApi.all(vm.dateFrom.toISOString(), vm.dateTo.toISOString())
            .then(function (res) {
                vm.list = res.data.map(function(element){
                    element.DETAIL_DATE = new Date(element.DETAIL_DATE )
                    element.DETAIL_DONE = !!element.DETAIL_DONE
                    return element
                });
                vm.gridOptions1.data = vm.list
                console.log('LOAD ', vm.dateFrom.toISOString(), vm.dateTo.toISOString(), vm.list);
            })
            .catch(function (err) {
                console.log('LOAD return ERROR!', err.data);
                vm.list = [];
                vm.gridOptions1.data = [];
                Alert.showError("Ошибка при получении нарядов: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function createClick() {
        vm.isCreating = true
    }

    function cancelCreate() {
        vm.newCablestructure = {};
        vm.isCreating = false
    }

    function createDetail (){
        vm.loading = true;
        if (!vm.newDetail || !vm.newDetail.date  || !vm.newDetail.DETAILTYPE_ID  || !vm.newDetail.MACHINE_ID){
            Alert.showError("Не все поля заполнениы.");
            return
        }
        return DetailApi.add(vm.newDetail.date.toISOString(), vm.newDetail.DETAILTYPE_ID, vm.newDetail.MACHINE_ID)
            .then(function (res) {
                console.log('object is added ', res.data);
                if (!res.data || res.data.length==0){
                    throw {data:'Не получены данные наряда'}
                }
                vm.newDetail = {};
                vm.isCreating = false;
                vm.loading = false;
                $state.go('main.detail', {detail_id: res.data[0].DETAIL_ID});
            })
            .catch(function (err) {
                console.log('object add return ERROR!', err);
                Alert.showError("Ошибка при добавлении: "
                + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function save(gridRow) {

        var deferred = $q.defer();
        vm.gridApi.rowEdit.setSavePromise(gridRow, deferred.promise);
        var done = gridRow.DETAIL_DONE ? 1 : 0
        vm.loading = true;
        return DetailApi.edit(gridRow.DETAIL_ID, gridRow.DETAIL_DATE, gridRow.DETAILTYPE_ID, gridRow.MACHINE_ID, done)
            .then(function (res) {
                console.log('object is saved ', res.data);
                deferred.resolve();
            })
            .catch(function (err) {
                console.log('object save return ERROR!', err);
                deferred.reject();
                Alert.showError( "Ошибка при сохранении: "
                + err && err.data ? err.data : err);
            })
            .finally(function () {
                vm.loading = false;
            })

    }

}

angular.module('rControllers').controller('DetailsCtrl', DetailsCtrl);