'use strict';

function CspartsCtrl($scope, $state, $q, Alert, Authenticate, CspartApi, ConductorApi) {
    var vm = this;
    vm.loading = false;
    vm.csparts = [];
    vm.conductors = [];
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.createCspart = createCspart;
    vm.isCreating = false;
    vm.newCspart = [];
    vm.createClick = createClick;
    vm.cancelCreate = cancelCreate;
    vm.checkRole = Authenticate.checkRole;
    vm.addToCspart = addToCspart
    vm.removeFromCspart = removeFromCspart
    $scope.removeRow = removeRow;

    vm.gridOptions1 = {
        enableFiltering: true,
        columnDefs: [
            { field: 'CSPART_ID', displayName: "Номер", enableHiding: false},
            { field: 'CSPARTTYPE_NAME', displayName: "Тип",
                enableHiding: false},
            { field: 'CSPART_NAME', displayName: "Жилы",
                enableHiding: false},
            {
                name: " ",
                enableColumnMenu: false,
                enableFiltering: false,
                cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                '<a  ng-click="grid.appScope.removeRow(row.entity)"' +
                'ng-show="!vm.loading">' +
                '<span class="glyphicon glyphicon-remove" style="font-size: 18px;"></span>' +
                '</a></div>',
                width: "30",
                enableCellEdit: false
            }
        ]
    };

    startLoad();

    function startLoad() {
        return load()
            .then(function () {
                return loadConductors()
            })
    }

    function createClick() {
        vm.isCreating = true
    }

    function cancelCreate() {
        vm.newCspart = [];
        vm.isCreating = false
    }

    function addToCspart(newObject) {
        if (newObject){
            vm.newCspart.push(newObject)
        }
    }

    function removeFromCspart(object) {
        if (object){
            var index = vm.newCspart.indexOf(object);
            if (index >= 0) {
                vm.newCspart.splice(index, 1);
            }
        }
    }

    function load() {
        vm.loading = true;
        return CspartApi.all()
            .then(function (res) {
                console.log('csparts are reseived ', res.data);
                vm.csparts = res.data;
                vm.gridOptions1.data = res.data;
            })
            .catch(function (err) {
                console.log('get orders return ERROR!', err.data);
                vm.csparts = [];
                vm.gridOptions1.data = [];
                Alert.showError("Ошибка при получении групп жил: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true;
            })
    }

    function loadConductors() {
        vm.loading = true;
        return ConductorApi.all()
            .then(function (res) {
                console.log('conductors are reseived ', res.data);
                vm.conductors = res.data;
            })
            .catch(function (err) {
                console.log('get conductors return ERROR!', err.data);
                vm.conductors = [];
                Alert.showError("Ошибка при получении списка жил: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })
    }

    function createCspart (){
        vm.loading = true;
        if (!vm.newCspart.length){
            Alert.showError("Не добавлена ни одна жила");
            return
        }
        var ids = vm.newCspart.map(function(el){
            return el.CONDUCTOR_ID
        }).join(',')
        return CspartApi.add(ids)
            .then(function (res) {
                console.log('object is added ', res.data);
                vm.newCspart = [];
                vm.isCreating = false
                return load()
            })
            .catch(function (err) {
                console.log('object add return ERROR!', err);
                Alert.showError("Ошибка при добавлении: "
                + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function removeRow(gridRow) {
        vm.loading = true;
        return CspartApi.remove(gridRow.CSPART_ID)
            .then(function (res) {
                console.log('object is removed ', res.data);
                var index = vm.csparts.indexOf(gridRow);
                console.log('index', index)
                if (index >= 0) {
                    vm.csparts.splice(index, 1);
                }
            })
            .catch(function (err) {
                console.log('object remove return ERROR!', err);
                Alert.showError("Ошибка при удалении: "
                + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }

}

angular.module('rControllers').controller('CspartsCtrl', CspartsCtrl);