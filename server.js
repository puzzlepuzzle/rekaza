var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');


var Config = require('./server/config');
var Api = require("./server/api");
var app = express();


app.use(logger('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser());

var SessionStore = require('express-mysql-session');

var options = {
    host: Config.mysql_host,  // your host
    user: Config.mysql_user, // your database user
    password: Config.mysql_pwd, // your database password
    database: 'rekaza'
};

app.use(session({
    secret: Config.passport_key,
    store: new SessionStore(options),
    resave: true,
    saveUninitialized: true
}));

require('./server/passport')(passport); // pass passport for configuration
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions



app.set('port', process.env.PORT || 7000);
app.disable("x-powered-by");
app.disable('etag');

app.set('view engine', 'jade');

app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'client/files')));
app.use('/bower_components/', express.static(__dirname + '/bower_components/'))

new Api(app, passport); // load our routes and pass in our app and fully configured passport

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

app.locals.sprintf = require('sprintf').sprintf;
app.locals.dateFormat = require('dateformat');

module.exports = app;