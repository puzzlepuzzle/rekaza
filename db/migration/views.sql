
USE rekaza;



-- view для типов кабелей
CREATE OR REPLACE VIEW view_cabletype AS
  SELECT
          CT.CABLETYPE_ID,
          CT.CABLETYPE_NAME AS CABLETYPE_NAME,
          CAST(CONCAT(CT.CABLETYPE_NAME,' (',TS.TECHSEQ_NAME,')') AS CHAR) AS CABLETYPE_NAME2,
          TS.TECHSEQ_ID,
          TS.TECHSEQ_NAME
      FROM
          cabletype CT
          LEFT JOIN techseq TS
              ON CT.TECHSEQ_ID = TS.TECHSEQ_ID;
-- view для операции
CREATE OR REPLACE VIEW view_operation AS
    SELECT 
            O.OPERATION_ID,
            O.OPERATION_NAME,
            OT.OPERATIONTYPE_ID,
            OT.OPERATIONTYPE_NAME
        FROM
            operation O
            JOIN operationtype OT
                ON O.OPERATIONTYPE_ID = OT.OPERATIONTYPE_ID
    ;
-- view для жилы
CREATE OR REPLACE VIEW view_conductor AS
    SELECT 
            CD.CONDUCTOR_ID,
            CD.CONDUCTOR_SQUERE,
            CDM.MATERIAL_ID,
            CDM.MATERIAL_NAME,
            CAST(CONCAT(CDM.MATERIAL_NAME,' ', CD.CONDUCTOR_SQUERE) AS CHAR) AS CONDUCTOR_NAME
        FROM
            conductor CD
            JOIN material CDM 
                ON CD.MATERIAL_ID = CDM.MATERIAL_ID;
-- view для части сечений
CREATE OR REPLACE VIEW view_cspart_all AS
    SELECT 
            CSP.CSPART_ID,
            CSPT.CSPARTTYPE_ID,
            CSPT.CSPARTTYPE_NAME,
            COUNT(CDV.CONDUCTOR_ID) AS CNDCSPART_COUNT,
            CAST(CONCAT(COUNT(CDV.CONDUCTOR_ID),'x',CDV.CONDUCTOR_SQUERE) AS CHAR) AS CNDCSPART_NAME,
            CDV.CONDUCTOR_ID,
            CDV.CONDUCTOR_SQUERE,
            CDV.CONDUCTOR_NAME,
            CDV.MATERIAL_ID,
            CDV.MATERIAL_NAME
        FROM
            cspart CSP
            JOIN cndcspart CCSP
                ON CSP.CSPART_ID = CCSP.CSPART_ID
            JOIN view_conductor CDV
                ON CCSP.CONDUCTOR_ID = CDV.CONDUCTOR_ID
            JOIN csparttype CSPT
                ON CSP.CSPARTTYPE_ID = CSPT.CSPARTTYPE_ID
        GROUP BY
            CSP.CSPART_ID,CDV.CONDUCTOR_ID;
-- view для частей сечения 1 : 1
CREATE OR REPLACE VIEW view_cspart AS
    SELECT 
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CAST(GROUP_CONCAT(CSPV.CNDCSPART_NAME SEPARATOR '+') AS CHAR) AS CSPART_NAME
        FROM
            view_cspart_all CSPV
        GROUP BY 
            CSPV.CSPART_ID;
-- view для реальных жил частей сечений 1:1
CREATE OR REPLACE VIEW view_realcndcspart AS
    SELECT 
            RCCSP.REALCNDCSPART_ID,
            RCCSP.CNDCSPART_ID,
            RCCSP.REALCSPART_ID,
            RCCSP.COLOR_ID,
            CAST(CONCAT(CONDUCTOR_NAME,'(',CL.COLOR_ABBR,')') AS CHAR) AS REALCNDCSPART_NAME,
            CDV.CONDUCTOR_ID,
            CDV.CONDUCTOR_SQUERE,
            CDV.CONDUCTOR_NAME,
            CDV.MATERIAL_ID,
            CDV.MATERIAL_NAME,
            CL.COLOR_ID AS CONDUCTOR_COLOR_ID,
            CL.COLOR_ABBR AS CONDUCTOR_COLOR_ABBR,
            CL.COLOR_NAME AS CONDUCTOR_COLOR_NAME
        FROM
            realcndcspart RCCSP
            JOIN cndcspart CCSP
                ON CCSP.CNDCSPART_ID = RCCSP.CNDCSPART_ID
            JOIN view_conductor CDV
                ON CCSP.CONDUCTOR_ID = CDV.CONDUCTOR_ID
            JOIN color CL
                ON RCCSP.COLOR_ID = CL.COLOR_ID;
-- view для реальных частей сечений 1:жила + продублироано по количеству REALCSPART_COUNT
CREATE OR REPLACE VIEW view_realcspart_all AS
    SELECT 
            RCSP.REALCSPART_ID,
            RCSP.CABLESTRUCTURE_ID,
            RCSP.REALCSPART_COUNT,
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CSPV.CSPART_NAME,
            RCCSPV.REALCNDCSPART_ID,
            RCCSPV.CNDCSPART_ID,
            RCCSPV.REALCNDCSPART_NAME,
            RCCSPV.CONDUCTOR_ID,
            RCCSPV.CONDUCTOR_SQUERE,
            RCCSPV.CONDUCTOR_NAME,
            RCCSPV.MATERIAL_ID,
            RCCSPV.MATERIAL_NAME,
            RCCSPV.CONDUCTOR_COLOR_ID,
            RCCSPV.CONDUCTOR_COLOR_ABBR,
            RCCSPV.CONDUCTOR_COLOR_NAME,
            CL.COLOR_ID AS CSPART_COLOR_ID,
            CL.COLOR_ABBR AS CSPART_COLOR_ABBR,
            CL.COLOR_NAME AS CSPART_COLOR_NAME,
            N.N AS REALCSPART_NO
        FROM
            realcspart RCSP
            JOIN view_realcndcspart RCCSPV
                ON RCSP.REALCSPART_ID = RCCSPV.REALCSPART_ID
            JOIN view_cspart CSPV
                ON RCSP.CSPART_ID = CSPV.CSPART_ID
            JOIN sys_numbers N
                ON N.N <= RCSP.REALCSPART_COUNT
            LEFT JOIN color CL
                ON RCSP.COLOR_ID = CL.COLOR_ID;
-- view для реальных частей сечений 1:жила
CREATE OR REPLACE VIEW view_realcspart_cnd AS
    SELECT 
            RCSP.REALCSPART_ID,
            RCSP.CABLESTRUCTURE_ID,
            RCSP.REALCSPART_COUNT,
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CSPV.CSPART_NAME,
            RCCSPV.REALCNDCSPART_ID,
            RCCSPV.CNDCSPART_ID,
            RCCSPV.REALCNDCSPART_NAME,
            RCCSPV.CONDUCTOR_ID,
            RCCSPV.CONDUCTOR_SQUERE,
            RCCSPV.CONDUCTOR_NAME,
            RCCSPV.MATERIAL_ID,
            RCCSPV.MATERIAL_NAME,
            RCCSPV.CONDUCTOR_COLOR_ID,
            RCCSPV.CONDUCTOR_COLOR_ABBR,
            RCCSPV.CONDUCTOR_COLOR_NAME,
            CL.COLOR_ID AS CSPART_COLOR_ID,
            CL.COLOR_ABBR AS CSPART_COLOR_ABBR,
            CL.COLOR_NAME AS CSPART_COLOR_NAME
        FROM
            realcspart RCSP
            JOIN view_realcndcspart RCCSPV
                ON RCSP.REALCSPART_ID = RCCSPV.REALCSPART_ID
            JOIN view_cspart CSPV
                ON RCSP.CSPART_ID = CSPV.CSPART_ID
            LEFT JOIN color CL
                ON RCSP.COLOR_ID = CL.COLOR_ID;
-- view для частей сечения 1 : 1
CREATE OR REPLACE VIEW view_realcspart AS
    SELECT 
            RCSP.REALCSPART_ID,
            RCSP.CABLESTRUCTURE_ID,
            RCSP.REALCSPART_COUNT,
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CSPV.CSPART_NAME,
            CAST(CONCAT(CSPV.CSPART_NAME,'(',CL.COLOR_ABBR,')') AS CHAR) AS REALCSPART_NAME,
            CL.COLOR_ID AS CSPART_COLOR_ID,
            CL.COLOR_ABBR AS CSPART_COLOR_ABBR,
            CL.COLOR_NAME AS CSPART_COLOR_NAME
        FROM
            realcspart RCSP
            JOIN view_cspart CSPV
                ON RCSP.CSPART_ID = CSPV.CSPART_ID
            LEFT JOIN color CL
                ON RCSP.COLOR_ID = CL.COLOR_ID;
-- view для реальных частей сечений 1:часть сечения + продублироано по количеству REALCSPART_COUNT
CREATE OR REPLACE VIEW view_realcspart_csp_all AS
    SELECT 
            RCSP.REALCSPART_ID,
            N.N AS REALCSPART_NO,
            RCSP.CABLESTRUCTURE_ID,
            -- SUM(RCSP.REALCSPART_COUNT) AS CSPART_COUNT,
            RCSP.REALCSPART_COUNT AS CSPART_COUNT,
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CSPV.CSPART_NAME,
            CONCAT(CSPV.CSPART_NAME,'(',CL.COLOR_ABBR,')') AS REALCSPART_NAME,
            CL.COLOR_ID AS CSPART_COLOR_ID,
            CL.COLOR_ABBR AS CSPART_COLOR_ABBR,
            CL.COLOR_NAME AS CSPART_COLOR_NAME
        FROM
            realcspart RCSP
            JOIN view_cspart CSPV
                ON RCSP.CSPART_ID = CSPV.CSPART_ID
            JOIN sys_numbers N
                ON N.N <= RCSP.REALCSPART_COUNT
            LEFT JOIN color CL
                ON RCSP.COLOR_ID = CL.COLOR_ID
        WHERE
            CSPV.CSPARTTYPE_ID <> 1;
-- view для частей сечения 1 : 1
CREATE OR REPLACE VIEW view_realcspart_csp AS
    SELECT 
            RCSP.REALCSPART_ID,
            RCSP.CABLESTRUCTURE_ID,
            SUM(RCSP.REALCSPART_COUNT) AS CSPART_COUNT,
            CSPV.CSPART_ID,
            CSPV.CSPARTTYPE_ID,
            CSPV.CSPARTTYPE_NAME,
            CSPV.CSPART_NAME
        FROM
            realcspart RCSP
            JOIN view_cspart CSPV
                ON RCSP.CSPART_ID = CSPV.CSPART_ID
        GROUP BY 
            RCSP.CABLESTRUCTURE_ID,RCSP.CSPART_ID;
-- view для сечения 1 : 1
CREATE OR REPLACE VIEW view_cablestructure AS
    SELECT 
            CS.CABLESTRUCTURE_ID,
            CS.CABLESTRUCTURE_NAME,
            GROUP_CONCAT(IF(LOCATE('+', RCSPV.CSPART_NAME),CONCAT('(',RCSPV.CSPART_NAME,')'),RCSPV.CSPART_NAME) SEPARATOR '+') AS CABLESTRUCTURE_STRUCTURE
        FROM
            cablestructure CS
            LEFT JOIN view_realcspart_csp RCSPV
                ON CS.CABLESTRUCTURE_ID = RCSPV.CABLESTRUCTURE_ID
        GROUP BY 
            CS.CABLESTRUCTURE_ID;
-- view для сечения 1 : REALCNDCSPART (одна жила-цвет)
CREATE OR REPLACE VIEW view_cablestructure_all AS
    SELECT 
            CS.CABLESTRUCTURE_NAME,
            RCSPV.REALCSPART_ID,
            RCSPV.CABLESTRUCTURE_ID,
            RCSPV.CSPART_ID,
            RCSPV.CSPARTTYPE_ID,
            RCSPV.CSPARTTYPE_NAME,
            RCSPV.CSPART_NAME,
            RCSPV.REALCNDCSPART_ID,
            RCSPV.CNDCSPART_ID,
            RCSPV.REALCNDCSPART_NAME,
            RCSPV.CONDUCTOR_ID,
            RCSPV.CONDUCTOR_SQUERE,
            RCSPV.CONDUCTOR_NAME,
            RCSPV.MATERIAL_ID,
            RCSPV.MATERIAL_NAME,
            RCSPV.CONDUCTOR_COLOR_ID,
            RCSPV.CONDUCTOR_COLOR_ABBR,
            RCSPV.CONDUCTOR_COLOR_NAME,
            RCSPV.CSPART_COLOR_ID,
            RCSPV.CSPART_COLOR_ABBR,
            RCSPV.CSPART_COLOR_NAME
        FROM
            cablestructure CS
            JOIN view_realcspart_all RCSPV
                ON CS.CABLESTRUCTURE_ID = RCSPV.CABLESTRUCTURE_ID;
-- нормы выработки
CREATE OR REPLACE VIEW view_worknorm AS
    SELECT 
            W.WORKNORM_ID,
            W.CONDUCTOR_ID,
            W.CABLESTRUCTURE_ID,
            OV.OPERATION_ID, 
            OV.OPERATION_NAME, 
            OV.OPERATIONTYPE_ID,
            OV.OPERATIONTYPE_NAME,
            CS.CABLESTRUCTURE_NAME, 
            CDV.CONDUCTOR_NAME,
            M.MACHINE_NAME, 
            M.MACHINE_ID, 
            CT.CABLETYPE_NAME,
            CT.CABLETYPE_ID,
            CAST(W.WORKNORM_VOL AS DECIMAL(14,4)) AS WORKNORM_VOL, 
            W.WORKNORM_TIME,
            CASE OV.OPERATIONTYPE_ID 
                 WHEN 1 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME)
                 WHEN 2 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME,CDV.CONDUCTOR_NAME)
                 WHEN 3 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME)
                 WHEN 4 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME,CSPV.CSPART_NAME)
            END AS OBJ_NAME
        FROM 
            worknorm W
            LEFT JOIN machine M
                ON W.MACHINE_ID = M.MACHINE_ID
            LEFT JOIN view_operation OV
                ON W.OPERATION_ID = OV.OPERATION_ID
            LEFT JOIN cabletype CT
                ON W.CABLETYPE_ID = CT.CABLETYPE_ID
            LEFT JOIN cablestructure CS
                ON W.CABLESTRUCTURE_ID = CS.CABLESTRUCTURE_ID
            LEFT JOIN view_cspart CSPV
                ON W.CSPART_ID = CSPV.CSPART_ID
            LEFT JOIN view_conductor CDV
                ON W.CONDUCTOR_ID = CDV.CONDUCTOR_ID;
-- материалы операции
CREATE OR REPLACE VIEW view_mnorm AS
    SELECT 
            W.MNORM_ID,
            W.CONDUCTOR_ID,
            W.CABLESTRUCTURE_ID,
            CS.CABLESTRUCTURE_NAME, 
            M.MATERIAL_ID,
            M.MATERIAL_NAME,
            OV.OPERATION_ID,
            OV.OPERATION_NAME, 
            OV.OPERATIONTYPE_ID,
            OV.OPERATIONTYPE_NAME,
            CDV.CONDUCTOR_NAME,
            CSPV.CSPART_NAME,
            CAST(W.MNORM_VOL AS DECIMAL(14,4)) AS MNORM_VOL,
              CASE OV.OPERATIONTYPE_ID 
                   WHEN 1 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME)
                   WHEN 2 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME,CDV.CONDUCTOR_NAME)
                   WHEN 3 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME)
                   WHEN 4 THEN CONCAT_WS(' ',CT.CABLETYPE_NAME,CS.CABLESTRUCTURE_NAME,CSPV.CSPART_NAME)
              END AS OBJ_NAME
        FROM 
            mnorm W
            LEFT JOIN view_operation OV
                ON W.OPERATION_ID = OV.OPERATION_ID
            LEFT JOIN cablestructure CS
                ON W.CABLESTRUCTURE_ID = CS.CABLESTRUCTURE_ID
            LEFT JOIN cabletype CT
                ON W.CABLETYPE_ID = CT.CABLETYPE_ID
            LEFT JOIN view_conductor CDV
                ON W.CONDUCTOR_ID = CDV.CONDUCTOR_ID
            LEFT JOIN view_cspart CSPV
                ON W.CSPART_ID = CSPV.CSPART_ID
            LEFT JOIN material M
                ON W.MATERIAL_ID = M.MATERIAL_ID;
-- кабели
CREATE OR REPLACE VIEW view_cable AS
    SELECT 
            C.CABLE_ID,
            C.CABLE_DONE,
            C.CABLE_VOL,
            C.CABLE_DESC,
            CONCAT_WS(' ',CT.CABLETYPE_NAME,CABLESTRUCTURE_NAME) AS CABLE_NAME,
            CS.CABLESTRUCTURE_NAME,
            CS.CABLESTRUCTURE_ID,
            CT.CABLETYPE_NAME,
            CONCAT(CT.CABLETYPE_NAME,' (',CT.TECHSEQ_NAME,')') AS CABLETYPE_NAME2,
            CT.CABLETYPE_ID,
            COALESCE(TS.TECHSEQ_NAME,CT.TECHSEQ_NAME) AS TECHSEQ_NAME,
            COALESCE(TS.TECHSEQ_ID,CT.TECHSEQ_ID) AS TECHSEQ_ID,
            P.PURCHASE_ID,
            P.PURCHASE_COMPANY,
            P.PURCHASE_CONTACT,
            P.PURCHASE_NO,
            P.PURCHASE_REGDATE,
            P.PURCHASE_CONFIRM,
            P.PURCHASE_IMPORTANT
        FROM
            cable C
            JOIN cablestructure CS
                ON C.CABLESTRUCTURE_ID = CS.CABLESTRUCTURE_ID
            JOIN view_cabletype CT
                ON C.CABLETYPE_ID = CT.CABLETYPE_ID
            LEFT JOIN techseq TS
                ON C.TECHSEQ_ID = TS.TECHSEQ_ID
            JOIN purchase P
                ON C.PURCHASE_ID = P.PURCHASE_ID;
-- жилы кабеля
CREATE OR REPLACE VIEW view_cable_cnd AS
    SELECT 
            C.CABLE_ID,
            C.CABLE_DONE,
            C.CABLE_VOL,
            C.CABLE_DESC,
            C.CABLETYPE_ID,
            CONCAT_WS(' ',CT.CABLETYPE_NAME,CABLESTRUCTURE_NAME) AS CABLE_NAME,
            RCSPV.REALCSPART_ID,
            RCSPV.REALCSPART_NO,
            RCSPV.CABLESTRUCTURE_ID,
            RCSPV.CSPART_ID,
            RCSPV.CSPARTTYPE_ID,
            RCSPV.CSPARTTYPE_NAME,
            RCSPV.CSPART_NAME,
            RCSPV.REALCNDCSPART_ID,
            RCSPV.CNDCSPART_ID,
            RCSPV.REALCNDCSPART_NAME,
            RCSPV.CONDUCTOR_ID,
            RCSPV.CONDUCTOR_SQUERE,
            RCSPV.CONDUCTOR_NAME,
            RCSPV.MATERIAL_ID,
            RCSPV.MATERIAL_NAME,
            RCSPV.CONDUCTOR_COLOR_ID,
            RCSPV.CONDUCTOR_COLOR_ABBR,
            RCSPV.CONDUCTOR_COLOR_NAME,
            RCSPV.CSPART_COLOR_ID,
            RCSPV.CSPART_COLOR_ABBR,
            RCSPV.CSPART_COLOR_NAME,
            2 AS OPERATIONTYPE_ID
        FROM
            cable C
            JOIN cablestructure CS
                ON C.CABLESTRUCTURE_ID = CS.CABLESTRUCTURE_ID
            JOIN cabletype CT
                ON C.CABLETYPE_ID = CT.CABLETYPE_ID
            JOIN view_realcspart_all RCSPV
                ON C.CABLESTRUCTURE_ID = RCSPV.CABLESTRUCTURE_ID;
-- части кабеля
CREATE OR REPLACE VIEW view_cable_csp AS
    SELECT 
            C.CABLE_ID,
            C.CABLE_DONE,
            C.CABLE_VOL,
            C.CABLE_DESC,
            C.CABLETYPE_ID,
            CONCAT_WS(' ',CT.CABLETYPE_NAME,CABLESTRUCTURE_NAME) AS CABLE_NAME,
            RCSPV.REALCSPART_ID,
            RCSPV.REALCSPART_NO,
            RCSPV.REALCSPART_NAME,
            RCSPV.CABLESTRUCTURE_ID,
            RCSPV.CSPART_ID,
            RCSPV.CSPARTTYPE_ID,
            RCSPV.CSPARTTYPE_NAME,
            RCSPV.CSPART_NAME,
            RCSPV.CSPART_COLOR_ID,
            RCSPV.CSPART_COLOR_ABBR,
            RCSPV.CSPART_COLOR_NAME,
            4 AS OPERATIONTYPE_ID
        FROM
            cable C
            JOIN cablestructure CS
                ON C.CABLESTRUCTURE_ID = CS.CABLESTRUCTURE_ID
            JOIN cabletype CT
                ON C.CABLETYPE_ID = CT.CABLETYPE_ID
            JOIN view_realcspart_csp_all RCSPV
                ON C.CABLESTRUCTURE_ID = RCSPV.CABLESTRUCTURE_ID;
-- объекты кабеля
CREATE OR REPLACE VIEW view_cable_objects AS
    SELECT 
            OPERATIONTYPE_ID, 
            CABLE_ID, 
            CABLE_DONE,
            CABLE_DESC, 
            CABLE_NAME, 
            CONDUCTOR_COLOR_ID AS COLOR_ID,
            CONDUCTOR_COLOR_ABBR AS COLOR_ABBR,
            CONDUCTOR_COLOR_NAME AS COLOR_NAME,
            CONDUCTOR_ID AS OBJ_ID,
            REALCNDCSPART_NAME AS OBJ_NAME,
            REALCSPART_NO AS REALCSPART_NO,
            CONDUCTOR_ID AS CONDUCTOR_ID,
            CSPART_ID AS CSPART_ID,
            REALCNDCSPART_ID,
            REALCSPART_ID,
            CABLESTRUCTURE_ID AS CABLESTRUCTURE_ID,
            CABLETYPE_ID AS CABLETYPE_ID,
            CABLE_VOL AS VOL
        FROM 
            view_cable_cnd
    UNION ALL
    SELECT 
            1 AS OPERATIONTYPE_ID, 
            CABLE_ID, 
            CABLE_DONE,
            CABLE_DESC, 
            CABLE_NAME, 
            NULL AS COLOR_ID,
            NULL AS COLOR_ABBR,
            NULL AS COLOR_NAME,
            CABLE_ID AS OBJ_ID,
            CABLE_NAME AS OBJ_NAME,
            0 AS REALCSPART_NO,
            NULL AS CONDUCTOR_ID,
            NULL AS CSPART_ID,
            NULL AS REALCNDCSPART_ID,
            NULL AS REALCSPART_ID,
            CABLESTRUCTURE_ID AS CABLESTRUCTURE_ID,
            CABLETYPE_ID AS CABLETYPE_ID,
            CABLE_VOL AS VOL
        FROM 
            view_cable
    UNION ALL
    SELECT 
            OPERATIONTYPE_ID, 
            CABLE_ID, 
            CABLE_DONE,
            CABLE_DESC, 
            CABLE_NAME, 
            CSPART_COLOR_ID AS COLOR_ID,
            CSPART_COLOR_ABBR AS COLOR_ABBR,
            CSPART_COLOR_NAME AS COLOR_NAME,
            CSPART_ID AS OBJ_ID,
            REALCSPART_NAME AS OBJ_NAME,
            REALCSPART_NO AS REALCSPART_NO,
            NULL AS CONDUCTOR_ID,
            CSPART_ID AS CSPART_ID,
            NULL AS REALCNDCSPART_ID,
            REALCSPART_ID,
            CABLESTRUCTURE_ID AS CABLESTRUCTURE_ID,
            CABLETYPE_ID AS CABLETYPE_ID,
            CABLE_VOL AS VOL
        FROM 
            view_cable_csp
    UNION ALL
    SELECT 
            3 AS OPERATIONTYPE_ID, 
            CABLE_ID, 
            CABLE_DONE,
            CABLE_DESC, 
            CABLE_NAME, 
            NULL AS COLOR_ID,
            NULL AS COLOR_ABBR,
            NULL AS COLOR_NAME,
            CABLE_ID AS OBJ_ID,
            CABLE_NAME AS OBJ_NAME,
            0 AS REALCSPART_NO,
            NULL AS CONDUCTOR_ID,
            NULL AS CSPART_ID,
            NULL AS REALCNDCSPART_ID,
            NULL AS REALCSPART_ID,
            CABLESTRUCTURE_ID AS CABLESTRUCTURE_ID,
            CABLETYPE_ID AS CABLETYPE_ID,
            1 AS VOL
        FROM 
            view_cable;
-- смены
CREATE OR REPLACE VIEW view_detail AS
  SELECT
          D.DETAIL_ID,
          D.DETAIL_DATE,
          D.DETAILTYPE_ID,
          D.DETAIL_DONE,
          DT.DETAILTYPE_NAME,
          D.DETAIL_DATE + INTERVAL DT.DETAILTYPE_FROMHOURS HOUR AS DETAIL_FROM,
          D.DETAIL_DATE + INTERVAL DT.DETAILTYPE_TOHOURS HOUR AS DETAIL_TO,
          M.MACHINE_ID,
          M.MACHINE_NAME
      FROM 
          detail D
          JOIN detailtype DT
              ON D.DETAILTYPE_ID = DT.DETAILTYPE_ID
          JOIN machine M
              ON D.MACHINE_ID = M.MACHINE_ID;
-- стэк плана
CREATE OR REPLACE VIEW view_mplanstack AS
    SELECT
            MPS.MPLANSTACK_ID,
            MPS.MPLANSTACK_DONE,
            COALESCE(VRC.REALCNDCSPART_NAME, VRP.REALCSPART_NAME, CV.CABLE_NAME) AS OBJ_NAME,
            MPS.MPLANSTACK_VOL AS MPLANSTACK_VOL,
            W.WORKNORM_TIME * MPS.MPLANSTACK_VOL AS MPLANSTACK_DURATION,
            
            OV.OPERATIONTYPE_ID,
            OV.OPERATIONTYPE_NAME,
            OV.OPERATION_ID,
            OV.OPERATION_NAME,
            OC.OPERCAB_DONE,
            OC.OPERCAB_ID,
            
            CV.CABLE_ID,
            CV.CABLE_DONE,
            CV.CABLE_VOL,
            CV.CABLE_DESC,
            CV.CABLE_NAME,
            CV.CABLESTRUCTURE_NAME,
            CV.CABLESTRUCTURE_ID,
            CV.CABLETYPE_NAME,
            CV.CABLETYPE_NAME2,
            CV.CABLETYPE_ID,
            CV.TECHSEQ_NAME,
            CV.PURCHASE_COMPANY,
            CV.PURCHASE_CONTACT,
            CV.PURCHASE_ID,
            CV.PURCHASE_NO,
            CV.PURCHASE_REGDATE,
            CV.PURCHASE_CONFIRM,
            CV.PURCHASE_IMPORTANT,
            
            MPS.REALCNDCSPART_ID,
            MPS.REALCSPART_NO,
            MPS.REALCSPART_ID,
            VRC.CONDUCTOR_ID,
            VRC.CONDUCTOR_SQUERE,
            VRC.CONDUCTOR_NAME,
            VRP.CSPART_ID,

            TU.TECHSEQ_ID,
            TU.TECHUNIT_N,
            
            W.WORKNORM_TIME,
            W.WORKNORM_VOL,
            
            M.MACHINE_ID,
            M.MACHINE_NAME
        FROM
            mplanstack MPS
            JOIN opercab OC
                ON MPS.OPERCAB_ID = OC.OPERCAB_ID
            JOIN techunit TU
                ON OC.TECHUNIT_ID = TU.TECHUNIT_ID
            JOIN view_cable CV
                ON OC.CABLE_ID = CV.CABLE_ID
            JOIN view_operation OV
                ON OC.OPERATION_ID = OV.OPERATION_ID
            JOIN worknorm W
                ON MPS.WORKNORM_ID = W.WORKNORM_ID
            JOIN machine M
                ON W.MACHINE_ID = M.MACHINE_ID
            LEFT JOIN view_realcndcspart VRC
                ON MPS.REALCNDCSPART_ID = VRC.REALCNDCSPART_ID
            LEFT JOIN view_realcspart VRP
                ON MPS.REALCSPART_ID = VRP.REALCSPART_ID;
        