var DB = require('./../db');

var Role = DB.Model.extend({
    tableName: 'roles',
    idAttribute: 'ROLE_ID'
});

module.exports = Role;