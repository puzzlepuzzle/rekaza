'use strict';

function NewOrderCtrl($scope, $state, Alert, $q,  OrderApi, Authenticate) {
    var vm = this;
    vm.loading = false;
    vm.order = {important: 0};
    vm.createOrder = createOrder;
    vm.checkRole = Authenticate.checkRole;

    function createOrder (){
        if (!vm.order || !vm.order.no || !vm.order.company || !vm.order.contact ){
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return OrderApi.create_order(vm.order.company, vm.order.contact, vm.order.important, vm.order.no)
            .then(function (res) {
                console.log('order is created ', res.data);
                if (!res.data.PURCHASE_ID){
                    Alert.showError("Не получен ид нового заказа.");
                    return;
                }
                $state.go('main.order', {order_id: res.data.PURCHASE_ID});

            })
            .catch(function (err) {
                console.log('create order return ERROR!', err.data);
                vm.order = null;
                Alert.showError("Ошибка при создании заказа: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }


}

angular.module('rControllers').controller('NewOrderCtrl', NewOrderCtrl);