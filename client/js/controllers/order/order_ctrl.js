'use strict';

function OrderCtrl($scope, $state, $q, $stateParams, Alert, OrderApi, CableApi, Authenticate) {
    var vm = this;
    vm.loading = false;
    vm.order = null;
    vm.orderCopy = null;
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.changeOrder = changeOrder;
    vm.orderIsChanged = orderIsChanged;
    vm.cables = [];
    vm.removeCabel = removeCabel;
    vm.addCabel = addCabel;
    vm.cable_types = [];
    vm.cable_structures = [];
    vm.newCabel = {};
    vm.isNewCabel = false;
    vm.cancelAddCabel = cancelAddCabel;
    vm.createCabel = createCabel;
    vm.cableIsChanged = cableIsChanged;
    vm.saveCable = saveCable;
    vm.cablesCopy = null;
    vm.checkRole = Authenticate.checkRole;
    vm.access = function (){
        return Authenticate.checkRole([1,3])
    }

    vm.changeImportant = function (){
        if (vm.access()){
            vm.order.PURCHASE_IMPORTANT = (-1)*vm.order.PURCHASE_IMPORTANT + 1
        }
    };
    vm.changeConfirm = function (){
        if (vm.access()) {
            vm.order.PURCHASE_CONFIRM = (-1) * vm.order.PURCHASE_CONFIRM + 1
        }
    };
    vm.changeCableDone = function (cabel){
        if (vm.access()) {
            cabel.CABLE_DONE = (-1)*cabel.CABLE_DONE + 1
        }
    };

    $scope.$watch('vm.cables.length', function(newValue, oldValue) {
        console.log('CHANGE CABLE', newValue)
        if (vm.cables){
            vm.cablesCopy = JSON.parse(JSON.stringify(vm.cables))
            for (var key in vm.cablesCopy){
                vm.cablesCopy[key].cabletype = {
                    CABLETYPE_ID:vm.cablesCopy[key].CABLETYPE_ID,
                    CABLETYPE_NAME: vm.cablesCopy[key].CABLETYPE_NAME,
                    CABLETYPE_NAME2: vm.cablesCopy[key].CABLETYPE_NAME2
                }
                vm.cablesCopy[key].cablestructure = {
                    CABLESTRUCTURE_ID:vm.cablesCopy[key].CABLESTRUCTURE_ID,
                    CABLESTRUCTURE_NAME: vm.cablesCopy[key].CABLESTRUCTURE_NAME
                }
            }
        }

    });

    vm.load();

    function load() {
        if (!$stateParams.order_id){
            Alert.showError("Не найден id заказа.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return OrderApi.get_order($stateParams.order_id)
            .then(function (res) {
                console.log('order is reseived ', res.data);
                vm.order = res.data;
                vm.orderCopy = JSON.parse(JSON.stringify(res.data));
                return CableApi.cables($stateParams.order_id)
            })
            .then(function (res) {
                console.log('cables is reseived ', res.data);
                vm.cables = res.data;
                return loadDopData();
            })
            .catch(function (err) {
                console.log('get order or cables return ERROR!', err.data);
                vm.order = null;
                vm.orderCopy = null;
                vm.cables = null;
                Alert.showError("Ошибка при получении заказа: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true
            })

    };

    function loadDopData() {

        vm.loading = true;
        return CableApi.cable_types()
            .then(function (res) {
                console.log('cable_types is reseived ', res.data);
                vm.cable_types = res.data;
                return CableApi.cable_structures()
            })
            .then(function (res) {
                console.log('cable_structures is reseived ', res.data);
                vm.cable_structures = res.data;
            })
            .catch(function (err) {
                console.log('get add data return ERROR!', err.data);
                vm.cable_types = null;
                vm.cable_structures = null;
                Alert.showError("Ошибка при получении дополнительных данных: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    };

    function changeOrder (){
        if (!vm.order){
            return $q.when(undefined); ;
        }
        vm.loading = true;
        return OrderApi.change_order(vm.order.PURCHASE_ID, vm.order.PURCHASE_COMPANY,
            vm.order.PURCHASE_CONTACT,vm.order.PURCHASE_IMPORTANT,vm.order.PURCHASE_CONFIRM, vm.order.PURCHASE_NO)
            .then(function (res) {
                console.log('order is changed ', res.data);
                Alert.showSuccess("Заказ успешно обновлен.");
                vm.orderCopy = JSON.parse(JSON.stringify(vm.order));

            })
            .catch(function (err) {
                console.log('create order return ERROR!', err.data);
                //vm.order = null;
                //vm.orderCopy = null;
                Alert.showError( "Ошибка при сохранении заказа: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function orderIsChanged (){
        if (!vm.order || !vm.orderCopy){
            return false;
        }
        //console.log(vm.order,vm.orderCopy)

        if (vm.order.PURCHASE_COMPANY != vm.orderCopy.PURCHASE_COMPANY ||
            vm.order.PURCHASE_CONTACT != vm.orderCopy.PURCHASE_CONTACT ||
            vm.order.PURCHASE_NO != vm.orderCopy.PURCHASE_NO ||
            vm.order.PURCHASE_CONFIRM != vm.orderCopy.PURCHASE_CONFIRM ||
            vm.order.PURCHASE_IMPORTANT != vm.orderCopy.PURCHASE_IMPORTANT){
            return true;
        }
        return false
    }

    function removeCabel (cabel){
        if ( !cabel){
            return $q.when(undefined); ;
        }
        vm.loading = true;
        return CableApi.remove_cable(cabel.CABLE_ID)
            .then(function (res) {
                console.log('cable is deleted ', res.data);
                Alert.showSuccess("Кабель удален.");

                var deletedCable = vm.cables.filter(function ( obj ) {
                    return obj.CABLE_ID === cabel.CABLE_ID;
                })[0];
                if (deletedCable){
                    var index = vm.cables.indexOf(deletedCable);
                    if (index >= 0) {
                        vm.cables.splice(index, 1);
                    }
                } else {
                    Alert.showSuccess("Не найден удаленный кабель.");
                }


            })
            .catch(function (err) {
                console.log('remove cable return ERROR!', err.data);

                Alert.showError("Ошибка при удалении кабеля: ");
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function addCabel (){
        console.log('add')
        vm.isNewCabel = true;
    }

    function cancelAddCabel (){
        vm.isNewCabel = false;
        vm.newCabel = {};
    }

    function createCabel (){
        if (!vm.order || !vm.newCabel || !vm.newCabel.cabletype || !vm.newCabel.cablestructure
        || !vm.newCabel.CABLE_VOL ){
            console.log(vm.newCabel)
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined) ;
        }
        vm.loading = true;
        return CableApi.add_cable(vm.newCabel.cabletype.CABLETYPE_ID, null,
            vm.order.PURCHASE_ID, vm.newCabel.cablestructure.CABLESTRUCTURE_ID,vm.newCabel.CABLE_VOL, vm.newCabel.CABLE_DESC)
            .then(function (res) {
                console.log('cable is created ', res.data);
                vm.cables.push(res.data)
                vm.isNewCabel = false;
                vm.newCabel = {};
            })
            .catch(function (err) {
                console.log('create cable return ERROR!', err.data);
                Alert.showError( "Ошибка при создании кабеля: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function cableIsChanged(cable){
        if (!cable){
            return false
        }
        //console.log(cable)
        var changedCable = vm.cables.filter(function ( obj ) {
            return obj.CABLE_ID === cable.CABLE_ID;
        })[0];
        if (changedCable){
            if (changedCable.CABLETYPE_ID != cable.cabletype.CABLETYPE_ID
            || changedCable.CABLESTRUCTURE_ID != cable.cablestructure.CABLESTRUCTURE_ID
            || changedCable.CABLE_VOL != cable.CABLE_VOL
            || changedCable.CABLE_DONE != cable.CABLE_DONE
            || changedCable.CABLE_DESC != cable.CABLE_DESC){
                return true
            } else {
                return false;
            }
        } else {
            return false
        }
    }

    function saveCable (cable){
        if (!vm.order || !cable){
            console.log(cable)
            Alert.showError("Не все поля заполнены.");
            return $q.when(undefined) ;
        }
        vm.loading = true;
        //id, cabletype_id, techseq_id, cablestructure_id, done, vol
        return CableApi.save_cable(cable.CABLE_ID, cable.CABLE_DONE, cable.CABLE_DESC)
            .then(function (res) {
                console.log('cable is saved ', res.data);
                vm.cables = JSON.parse(JSON.stringify(vm.cablesCopy))

                Alert.showSuccess("Кабель сохранен.");

            })
            .catch(function (err) {
                console.log('edit cable return ERROR!', err.data, cable);
                Alert.showError( "Ошибка при сохранении кабеля: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }
}

angular.module('rControllers').controller('OrderCtrl', OrderCtrl);