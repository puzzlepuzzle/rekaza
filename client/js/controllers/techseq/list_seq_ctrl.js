'use strict';

function SeqsCtrl($scope, $state, $q, Alert,Authenticate, TechseqApi) {
    var vm = this;
    vm.loading = false;
    vm.seqs = [];
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.createClick = createClick;
    vm.cancelCreate = cancelCreate;
    vm.create = create;
    vm.newSeq = null;
    vm.isCreating = false;
    vm.access = function (){
        return Authenticate.checkRole([1,2,4])
    }
    $scope.access = vm.access;
    vm.gridOptions1 = {
        enableFiltering: true,
        columnDefs: [
            { field: 'TECHSEQ_ID', displayName: "Номер", enableHiding: false,
            cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
            '<a ng-show="grid.appScope.access()" ui-sref="main.techseq({techseq_id:COL_FIELD})">{{COL_FIELD CUSTOM_FILTERS}}</a>' +
            '<a ng-hide="grid.appScope.access()" ui-sref="main.show_techseq({techseq_id:COL_FIELD})">{{COL_FIELD CUSTOM_FILTERS}}</a>' +
            '</div>', width: "100",cellClass: 'grid_cell-center'},
            { field: 'TECHSEQ_NAME',  displayName: "Название" ,enableHiding: false }
        ]
    };

    vm.load();

    function load() {
        vm.loading = true;
        return TechseqApi.all()
            .then(function (res) {
                console.log('seqs are reseived ', res.data);
                vm.seqs = res.data;
                vm.gridOptions1.data = res.data;
            })
            .catch(function (err) {
                console.log('get seqs return ERROR!', err.data);
                vm.seqs = [];
                vm.gridOptions1.data = [];
                Alert.showError( "Ошибка при получении технологических цепочек: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true;
            })

    };

    function createClick() {
        vm.isCreating = true
    }

    function cancelCreate() {
        vm.newSeq = null;
        vm.isCreating = false
    }

    function create() {
        if (!vm.newSeq || !vm.newSeq.name ){
            Alert.showError("Не заполнено имя.");
            return $q.when(undefined);
        }
        vm.loading = true;
        return TechseqApi.add(vm.newSeq.name)
            .then(function (res) {
                console.log('seq are created ', res.data);
                $state.go('main.techseq', {techseq_id: res.data.TECHSEQ_ID})
            })
            .catch(function (err) {
                console.log('add seq return ERROR!', err.data);

                Alert.showError("Ошибка при создании технологической цепочки: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true;
            })

    };

}

angular.module('rControllers').controller('SeqsCtrl', SeqsCtrl);