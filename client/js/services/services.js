'use strict';

/* Services */

var rServices = angular.module('rServices', []);

rServices.factory('WorknormApi', ['$http', '$q',
    function ($http, $q) {
        return {
            all: function () {
                return $http.get('/api/worknorms', {});
            },
            get: function (id) {
                return $http.get('/api/worknorm', {params: {id : id}});
            },
            remove: function (id) {
                return $http.post('/api/worknorm/delete', {id : id});
            },
            add: function (cablestructure_id,conductor_id,machine_id,operation_id,cabletype_id,time) {
                return $http.post('/api/worknorm/add',  {cablestructure_id : cablestructure_id,
                                                      conductor_id : conductor_id,
                                                      machine_id : machine_id,
                                                      operation_id : operation_id,
                                                      cabletype_id : cabletype_id,
                                                      time : time});
            },
            edit: function (id,cablestructure_id,conductor_id,machine_id,operation_id,cabletype_id,time) {
                return $http.post('/api/worknorm/edit',  {id : id,
                                                      cablestructure_id : cablestructure_id,
                                                      conductor_id : conductor_id,
                                                      machine_id : machine_id,
                                                      operation_id : operation_id,
                                                      cabletype_id : cabletype_id,
                                                      time : time});
            }
        };
    }]);
rServices.factory('MnormApi', ['$http', '$q',
    function ($http, $q) {
        return {
            all: function () {
                return $http.get('/api/mnorms', {});
            },
            get: function (id) {
                return $http.get('/api/mnorm', {params: {id : id}});
            },
            remove: function (id) {
                return $http.post('/api/mnorm/delete', {id : id});
            },
            add: function (cablestructure_id,conductor_id,operation_id,material_id,vol) {
                return $http.post('/api/mnorm/add',  {cablestructure_id : cablestructure_id,
                                                      conductor_id : conductor_id,
                                                      operation_id : operation_id,
                                                      material_id : material_id,
                                                      vol : vol});
            },
            edit: function (id,cablestructure_id,conductor_id,operation_id,material_id,vol) {
                return $http.post('/api/mnorm/edit',  {id : id,
                                                      cablestructure_id : cablestructure_id,
                                                      conductor_id : conductor_id,
                                                      operation_id : operation_id,
                                                      material_id : material_id,
                                                      vol : vol});
            }
        };
    }]);

rServices.factory('MplanApi', ['$http', '$q',
    function ($http, $q) {
        return {
            get_date_mplan: function (from, to) {
                return $http.get('/api/mplan/date',
                    {params: {from: from, to: to}});
            },
            get_mplans: function () {
                return $http.get('/api/mplans',
                    {params: {}});
            },
            edit: function (id, done, duration, vol) {
                return $http.post('/api/mplan/edit',
                    {
                        id:id,
                        done: done,
                        duration:duration,
                        vol:vol
                    });
            },
            remove: function (id) {
                return $http.post('/api/mplan/delete',
                    {id: id});
            },
            get: function (id) {
                return $http.post('/api/mplan',
                    {params:{id: id}});
            },
        }
    }]);
rServices.factory('DetailApi', ['$http', '$q',
     function ($http, $q) {
         return {
             all: function (from, to) {
                 return $http.get('/api/details',
                     {params: {from : from, to : to}});
             },
             get: function (id) {
                 return $http.get('/api/detail',
                     {params: {id : id}});
             },
             types: function () {
                 return $http.get('/api/detailtypes',
                     {});
             },
             add: function (date, detailtype_id, machine_id) {
                 return $http.post('/api/detail/add',
                     {date: date, detailtype_id: detailtype_id, machine_id: machine_id});
             },
             edit: function (id, date, detailtype_id, machine_id, done) {
                 return $http.post('/api/detail/edit',
                     {id: id, date: date, detailtype_id: detailtype_id, machine_id: machine_id, done: done});
             },
             mplan_set: function (detail_id, ids_string) {
                 return $http.post('/api/detail/mplans/set',
                     {detail_id: detail_id, ids_string:ids_string});
             }
         }
     }]);

rServices.factory('OrderApi', ['$http', '$q',
    function ($http, $q) {
        return {
            orders: function () {
                return $http.get('/api/purchases', {});
            },
            get_order: function (id) {
                return $http.get('/api/purchase',
                    {params: {id: id}});
            },
            remove: function (id) {
                return $http.post('/api/purchase/delete',
                    {id: id});
            },
            create_order: function (company, contact, important, no) {
                return $http.post('/api/purchase/add',
                    {company: company, contact: contact, important: important, no: no});
            },
            change_order: function (id, company, contact, important, confirm, no) {
                return $http.post('/api/purchase/edit',
                    {id: id, company: company, contact: contact, important: important, confirm: confirm, no: no});
            }
        }
    }]);

rServices.factory('CableApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add_cable: function (cabletype_id, techseq_id, purchase_id, cablestructure_id, vol, desc) {
                return $http.post('/api/purchase/cable/add',
                    {
                        now: new Date().toISOString(), cabletype_id: cabletype_id,
                        techseq_id: techseq_id, purchase_id: purchase_id,
                        cablestructure_id: cablestructure_id, vol: vol, desc:desc
                    });
            },
            save_cable: function (id, done, desc) {
                return $http.post('/api/purchase/cable/edit',
                    {
                        now: new Date().toISOString(), id: id, done: done, desc:desc
                    });
            },
            remove_cable: function (id) {
                return $http.post('/api/purchase/cable/delete',
                    {id: id});
            },
            cables: function (order_id) {
                return $http.get('/api/purchase/cables',
                    {params: {id: order_id}});
            },
            cable_types: function () {
                return $http.get('/api/cabletypes', {});
            },
            cable_structures: function () {
                return $http.get('/api/cablestructures', {});
            }
        }
    }]);

rServices.factory('OperationApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name, type) {
                return $http.post('/api/operation/add',
                    {name: name, type: type});
            },
            types: function () {
                return $http.get('/api/operation_types',
                    {});
            },
            edit: function (id, name, type) {
                return $http.post('/api/operation/edit',
                    {id: id, name: name, type: type});
            },
            remove: function (id) {
                return $http.post('/api/operation/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/operations',
                    {});
            },
            get: function (id) {
                return $http.get('/api/operation',
                    {params: {id: id}});
            }
        }
    }]);
    
rServices.factory('CablespartApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (count, cablestructure_id, conductor_id) {
                console.log('ADDDDDDDDDD',{count : count, cablestructure_id: cablestructure_id, conductor_id: conductor_id})
                return $http.post('/api/cablespart/add',
                    {count : count, cablestructure_id: cablestructure_id, conductor_id: conductor_id});
            },
            edit: function (id, count, cablestructure_id, conductor_id) {
                return $http.post('/api/cablespart/edit',
                    {id: id, count : count, cablestructure_id: cablestructure_id, conductor_id: conductor_id});
            },
            remove: function (id) {
                return $http.post('/api/cablespart/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/cablesparts',
                    {});
            },
            get: function (id) {
                return $http.get('/api/cablespart',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('MaterialApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name, vol) {
                return $http.post('/api/material/add',
                    {name: name, vol: vol});
            },
            edit: function (id, name, vol) {
                return $http.post('/api/material/edit',
                    {id: id, name: name, vol: vol});
            },
            remove: function (id) {
                return $http.post('/api/material/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/materials',
                    {});
            },
            get: function (id) {
                return $http.get('/api/material',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('CablestructureApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name) {
                return $http.post('/api/cablestructure/add',
                    {name: name});
            },
            edit: function (id, name) {
                return $http.post('/api/cablestructure/edit',
                    {id: id, name: name});
            },
            remove: function (id) {
                return $http.post('/api/cablestructure/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/cablestructures',
                    {});
            },
            get: function (id) {
                return $http.get('/api/cablestructure',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('MachineApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name) {
                return $http.post('/api/machine/add',
                    {name: name});
            },
            edit: function (id, name) {
                return $http.post('/api/machine/edit',
                    {id: id, name: name});
            },
            remove: function (id) {
                return $http.post('/api/machine/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/machines',
                    {});
            },
            get: function (id) {
                return $http.get('/api/machine',
                    {params: {id: id}});
            },
            mplan: function (id) {
                return $http.get('/api/machine/mplanstack',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('ConductorApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (squere, material_id) {
                return $http.post('/api/conductor/add',
                    {squere: squere, material_id: material_id});
            },
            edit: function (id, squere, material_id) {
                return $http.post('/api/conductor/edit',
                    {id: id, squere: squere, material_id: material_id});
            },
            remove: function (id) {
                return $http.post('/api/conductor/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/conductors',
                    {});
            },
            get: function (id) {
                return $http.get('/api/conductor',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('CspartApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (conductors_ids_string) {
                return $http.post('/api/cspart/add',
                    {conductors_ids_string: conductors_ids_string});
            },
            remove: function (id) {
                return $http.post('/api/cspart/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/csparts',
                    {});
            }
        }
    }]);

rServices.factory('RealcspartApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (cspart_id,cablestructure_id,count,color_id) {
                return $http.post('/api/realcspart/add',
                    {cspart_id: cspart_id,cablestructure_id: cablestructure_id,count: count,color_id: color_id});
            },
            remove: function (id) {
                return $http.post('/api/realcspart/delete',
                    {id: id});
            },
            edit: function (id, color_id) {
                return $http.post('/api/realcspart/edit',
                    {id: id, color_id:color_id});
            },
            get: function (id) {
                return $http.get('/api/realcspart',
                    {params: {id: id}});
            },
            all: function (cablestructure_id) {
                return $http.get('/api/realcsparts',
                    {params:{cablestructure_id:cablestructure_id}});
            },
            all_cnds: function (realcspart_id) {
                return $http.get('/api/realcspart/cnds',
                    {params: {realcspart_id: realcspart_id}});
            },
            edit_cnd: function (id,color_id) {
                return $http.post('/api/realcspart/cnd/edit',
                    {id: id,color_id: color_id});
            }
        }
    }]);

rServices.factory('CabletypeApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name, techseq_id) {
                return $http.post('/api/cabletype/add',
                    {name: name, techseq_id: techseq_id});
            },
            edit: function (id, name, techseq_id) {
                return $http.post('/api/cabletype/edit',
                    {id: id, name: name, techseq_id: techseq_id});
            },
            remove: function (id) {
                return $http.post('/api/cabletype/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/cabletypes',
                    {});
            },
            get: function (id) {
                return $http.get('/api/cabletype',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('TechseqApi', ['$http', '$q',
    function ($http, $q) {
        return {
            add: function (name) {
                return $http.post('/api/techseq/add',
                    {name: name});
            },
            edit: function (id, name) {
                return $http.post('/api/techseq/edit',
                    {id: id, name: name});
            },
            remove: function (id) {
                return $http.post('/api/techseq/delete',
                    {id: id});
            },
            all: function () {
                return $http.get('/api/techseqs',
                    {});
            },
            get: function (id) {
                return $http.get('/api/techseq',
                    {params: {id: id}});
            }
        }
    }]);

rServices.factory('TechUnitApi', ['$http', '$q',
    function ($http, $q) {
        return {
            edit: function (techseq_id, ids_string) {
                return $http.post('/api/techseq/units/edit',
                    {techseq_id: techseq_id, ids_string: ids_string});
            },
            all: function (techseq_id) {
                return $http.get('/api/techseq/units',
                    {params: {id: techseq_id}});
            },
            remove: function (id) {
                return $http.post('/api/techseq/unit/delete',
                    {id: id});
            },
            add: function (techseq_id, operation_id, n) {
                return $http.post('/api/techseq/unit/add',
                    {techseq_id: techseq_id, operation_id: operation_id, n: n});
            }
        }
    }]);

rServices.factory('ColorApi', ['$http', '$q',
    function ($http, $q) {
        return {
            all: function () {
                return $http.get('/api/colors', {});
            },
            get: function (id) {
                return $http.get('/api/color', {
                    params: {id: id}
                });
            },
            edit: function (id, name, abbr) {
                return $http.post('/api/color/edit',
                    {id: id, name: name, abbr: abbr});
            },
            remove: function (id) {
                return $http.post('/api/color/delete',
                    {id: id});
            },
            add: function (name, abbr) {
                return $http.post('/api/color/add',
                    {name: name, abbr: abbr});
            }
        }
    }]);

rServices.factory('UserApi', ['$http', '$q',
    function ($http, $q) {
        return {
            users: function () {
                return $http.get('/api/users', {});
            },
            roles: function () {
                return $http.get('/api/roles', {});
            },
            user: function (user_id) {
                return $http.get('/api/user', {
                    params: {id: user_id}
                })
            },
            remove: function (user_id) {
                return $http.post('/api/user/delete',
                    {id: user_id}
                )
            }
        }
    }]);

rServices.factory('Authenticate',
    ['$rootScope', '$http', '$state', '$q',
        function ($rootScope, $http, $state, $q) {

            var isAuthenticated = undefined;
            var user = {login: null, id: null, role: null};

            function initDone() {
                return angular.isDefined(isAuthenticated);
            }

            function getIsAuthenticated() {
                return isAuthenticated;
            }

            function getUserRole() {
                return user.role;
            }

            function getUserLogin() {
                return user.login;
            }

            function getUserId() {
                return user.id;
            }

            function checkAuth() {
                return $http.get("/api/check_auth", {})
                    .then(function (res) {
                        isAuthenticated = !!res.data.isAuth;
                        user.id = res.data.userId;
                        user.login = res.data.userLogin;
                        user.role = res.data.role;
                        console.log('check_auth DONE', 'isAuthenticated ', isAuthenticated, ' user ', user)
                        return res;

                    }).catch(function (err) {
                        console.log("check_auth service error ", err.data)
                        isAuthenticated = null;
                        user = {login: null, id: null, role: null};
                        throw err;
                    });
            }

            var initAuth = function () {
                var deferred = $q.defer();
                if (angular.isDefined(isAuthenticated)) {
                    console.log('initAuth not need')
                    deferred.resolve(isAuthenticated);
                    return deferred.promise;
                }

                checkAuth()
                    .then(function () {
                        console.log('init auth isAuthenticated ', isAuthenticated)
                        deferred.resolve(isAuthenticated);
                    })
                    .catch(function () {
                        deferred.reject(isAuthenticated);
                    })

                return deferred.promise;
            };

            var checkAccess = function () {
                return initAuth()
                    .then(function () {
                        console.log('checkAccess', $rootScope.toState, 'isAuthenticated ', isAuthenticated)
                        if ($rootScope.toState.authenticate && !isAuthenticated) {
                            $rootScope.returnToState = $rootScope.toState;
                            $rootScope.returnToStateParams = $rootScope.toStateParams;

                            console.log("redirect to login");
                            $state.go('main.login');
                        } else if ($rootScope.toState.roles
                            && $rootScope.toState.roles.indexOf(getUserRole()) == -1) {
                            console.log("cant go to this page", $rootScope.toState.roles, getUserRole());
                            $state.go('main.accessdenied');
                        }
                    })
                    .catch(function () {
                        $state.go('main.error');
                    })
            }

            var login = function (userData) {
                console.log('Authenticate login ')
                return $http.post("/api/login", userData)
                    .then(function () {
                        return checkAuth();
                    }).catch(function (err) {
                        console.log("login service error ", err.data)
                        isAuthenticated = null;
                        user = {login: null, id: null, role: null};
                        throw err;
                    });
            };


            var logout = function () {
                return $http.get("/api/logout")
                    .then(function (res) {
                        isAuthenticated = false;
                        user = {login: null, id: null, role: null};
                        return res
                    }).catch(function (err) {
                        console.log("logout service error ", err.data)

                        throw err;
                    });
            };

            var register = function (userData) {
                console.log('Authenticate register ', userData)
                var promise = $http.post("/api/register", userData);
                return promise;

            }

            var checkRole = function (roles) {
                if (!isAuthenticated
                ){
                    return false
                }
                return roles.indexOf(user.role) !=-1
            }

            return {
                login: login,
                logout: logout,
                register: register,

                isAuthenticated: getIsAuthenticated,
                userLogin: getUserLogin,
                userId: getUserId,
                userRole: getUserRole,
                checkAccess: checkAccess,
                initDone: initDone,
                checkRole: checkRole
            }
        }]);

rServices.factory('ModalApi', ['$modal', '$rootScope', '$q',
    function ($modal, $rootScope, $q) {
        // instantiate our initial object
        var MyModal = function ($modal, $rootScope, $q) {
            this.$modal = $modal;
            this.$rootScope = $rootScope
            this.$q = $q;
            this.currentModal = null;
        };

        MyModal.prototype.show = function (template, params, backdrop) {
            template = template || 'partials/directives/default_modal.html'
            if (this.currentModal) {
                //console.log("modal=", this.currentModal)
                this.currentModal.destroy();
            }
            var scope = this.$rootScope.$new(true);
            scope.params = params;

            scope.deferred = this.$q.defer();

            scope.confirm = function (res) {
                this.deferred.resolve(res);
                this.$parent.$hide();

            }

            this.currentModal = this.$modal({
                template: 'partials/directives/modal_parent.html',
                scope: scope,
                show: true,
                placement: "center",
                content: template,
                container: 'body',
                backdrop: backdrop,
            })

            return scope.deferred.promise;
        }
        return new MyModal($modal, $rootScope, $q);
    }]);

rServices.factory('Alert', ['$alert', '$q',
    function ($alert, $q) {
        // instantiate our initial object
        var MyAlert = function ($alert, $q) {
            this.$alert = $alert;
            this.$q = $q;
            this.currentAlert = null;
        };

        MyAlert.prototype.show = function (msg, type) {
            if (this.currentAlert) {
                this.currentAlert.destroy();
            }

            this.currentAlert = this.$alert({
                title: '',
                content: msg,
                placement: 'top', type: type, show: true,
                duration: '3',
                container: 'body'
            });
        }

        MyAlert.prototype.showError = function (msg) {
            this.show(msg, "danger")
        }

        MyAlert.prototype.showNotice = function (msg) {
            this.show(msg, "warning")
        }

        MyAlert.prototype.showSuccess = function (msg) {
            this.show(msg, "success")
        }

        MyAlert.prototype.clean = function () {
            if (this.currentAlert) {
                this.currentAlert.destroy();
                this.currentAlert = null;
            }
        }
        return new MyAlert($alert, $q);
    }]);
