var DB = require('./../db');

var CableType = DB.Model.extend({
    tableName: 'cabletype',
    idAttribute: 'CABLETYPE_ID',

});

module.exports = CableType;