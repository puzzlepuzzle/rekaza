'use strict';

function MplanCtrl($scope, $state, Alert, uiGridConstants, Authenticate, MplanApi, OperationApi) {
    var vm = this;
    vm.loading = false;
    vm.load = load;
    $scope.operationTypes = [];

    vm.load();

    function load() {
        vm.loading = true;
        var data;
        MplanApi.get_mplans()
            .then(function (res) {
                console.log('mplan_date are received ', res.data);
                data = res.data;
                return OperationApi.types()
            })
            .then(function (res1) {
                console.log('operationTypes', res1.data)
                $scope.operationTypes = {}
                res1.data.map(function (el) {
                    $scope.operationTypes[el['OPERATION_TYPE']] = el['OPERATION_TYPE_NAME']
                })
                initGridOptions(data)
                vm.loading = false;
            })
            .catch(function (err) {
                console.log('get mplan_date return ERROR!', err.data);
                vm.gridOptions1.data = [];

                vm.loading = false;
                Alert.showError("Ошибка при получении Плана: " + err.data);
            })

    }

    function initGridOptions (data){
        vm.gridOptions1 = {
            data: data,
            enableFiltering: true,
            columnDefs: [
                { field: 'MACHINE_NAME', displayName: "Станок" , width: '200'},
                { field: 'OPERATION_NAME', displayName: "Операция", width: '230'},
                { field: 'OPERATION_TYPE', displayName: "Тип операции", width: '120',
                    cellFilter: 'foreign_type: grid.appScope.operationTypes',
                    filter: {
                    condition: function(searchTerm, cellValue) {
                        return $scope.operationTypes[cellValue].toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0;
                    }
                }},
                { field: 'PURCHASE_NO', displayName: "Заказ", width: '70'},
                { field: 'OBJ_NAMES', displayName: "Изделие", width: '200'},
                { field: 'MPLAN_FROM',  displayName: "C", cellFilter: 'date:\'MM.dd hh.mm\'', enableFiltering: false , width: '100'},
                { field: 'MPLAN_DURATION', displayName: "Часы", cellFilter: 'number:2', enableFiltering: false , width: '70' },
                { field: 'MPLAN_TO', displayName: "По", cellFilter: 'date:\'MM.dd hh.mm\'', enableFiltering: false, width: '100' },
            ],
            onRegisterApi: function( gridApi ) {
                $scope.grid1Api = gridApi;
            }
        };
    }

}

angular.module('rControllers').controller('MplanCtrl', MplanCtrl);