'use strict';

function OrdersCtrl($scope, $window, $state, Alert, OrderApi, ModalApi, Authenticate) {
    var vm = this;
    vm.loading = false;
    vm.orders = [];
    vm.dataIsLoaded = false;
    vm.load = load;
    vm.createOrder = createOrder;
    vm.planToMonth = planToMonth;
    vm.checkRole = Authenticate.checkRole;
    $scope.removeRow = removeRow;

    vm.gridOptions1 = {
        enableFiltering: true,
        columnDefs: [
            { field: 'PURCHASE_NO', displayName: "Номер", enableHiding: false,
            cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
            '<a ui-sref="main.order({order_id:row.entity.PURCHASE_ID})">{{COL_FIELD CUSTOM_FILTERS}}</a>' +
            '</div>', width: "100",cellClass: 'grid_cell-center'},
            { field: 'PURCHASE_REGDATE', displayName: "Дата создания",
                enableHiding: false, enableFiltering: false,cellFilter:"date:'dd.MM.yyyy'" , width: "130"},
            { field: 'PURCHASE_IMPORTANT', displayName: "Срочность",
                enableHiding: false, enableFiltering: false, cellFilter: 'yes_no',
                width: "100",cellClass: 'grid_cell-center'},
            { field: 'PURCHASE_CONFIRM', displayName: "Подтвержден",
                enableHiding: false, enableFiltering: false, cellFilter: 'yes_no',
                width: "130",cellClass: 'grid_cell-center'},
            { field: 'PURCHASE_COMPANY',  displayName: "Заказчик" ,enableHiding: false },
            { field: 'CABLE_CNT', displayName: "Кол-во кабелей" ,
                enableHiding: false, enableFiltering: false , 
                width: "140",cellClass: 'grid_cell-center'},
            { field: 'PURCHASE_TO', displayName: "Дата выполнения",
                enableHiding: false, enableFiltering: false,cellFilter:"date:'dd.MM.yyyy'" , width: "150"},
            {
                name: " ",
                enableColumnMenu: false,
                enableFiltering: false,
                cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                '<a  ng-click="grid.appScope.removeRow(row.entity)"' +
                'ng-show="!vm.loading">' +
                '<span class="glyphicon glyphicon-remove" style="font-size: 18px;"></span>' +
                '</a></div>',
                width: "30",
                enableCellEdit: false
            }
        ]
    };

    vm.load();

    function load() {
        vm.loading = true;
        return OrderApi.orders()
            .then(function (res) {
                console.log('orders are reseived ', res.data);
                vm.orders = res.data;
                vm.gridOptions1.data = res.data;
            })
            .catch(function (err) {
                console.log('get orders return ERROR!', err.data);
                vm.orders = [];
                vm.gridOptions1.data = [];
                Alert.showError("Ошибка при получении заказов: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true;
            })

    };

    function createOrder (){
        $state.go('main.new_order')
    }

    function planToMonth (){
        var params = {period: new Date()}
        return ModalApi.show('partials/directives/plan_to_month.html', params, false)
            .then(function (confirm) {
                console.log('confirm', confirm)
                if (confirm) {
                    if (!params.period) {
                        Alert.showError("Не был выбран период");
                    } else {
                        params.period.setDate( params.period.getDate()+1)
                        $window.open('/api/purchases/plan/print?period=' + params.period.toISOString(), '_blank');
                    }
                }
            })
            .catch(function (err) {
                console.log('err', err)
            })
    }

    function removeRow(gridRow) {
        vm.loading = true;
        return OrderApi.remove(gridRow.PURCHASE_ID)
            .then(function (res) {
                console.log('object is removed ', res.data);
                var index = vm.orders.indexOf(gridRow);
                console.log('index', index)
                if (index >= 0) {
                    vm.orders.splice(index, 1);
                }
            })
            .catch(function (err) {
                console.log('object remove return ERROR!', err);
                Alert.showError("Ошибка при удалении: "
                    + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }
}

angular.module('rControllers').controller('OrdersCtrl', OrdersCtrl);