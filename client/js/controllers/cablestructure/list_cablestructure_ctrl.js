'use strict';

function CablestructuresCtrl($scope, $state, $q, Alert, Authenticate,CablestructureApi) {
    var vm = this;

    vm.loading = false;
    vm.dataIsLoaded = false;
    vm.list = [];
    vm.checkRole = Authenticate.checkRole;
    vm.createCablestructure = createCablestructure;
    vm.isCreating = false;
    vm.newCablestructure = {};
    vm.createClick = createClick;
    vm.cancelCreate = cancelCreate;
    $scope.removeRow = removeRow;

    vm.gridOptions1 = {
        enableFiltering: true,
        columnDefs: [
            { field: 'CABLESTRUCTURE_ID', displayName: "Номер",
                enableHiding: false,
                cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                '<a ui-sref="main.cablestructure({cablestructure_id:row.entity.CABLESTRUCTURE_ID})">{{COL_FIELD CUSTOM_FILTERS}}</a>' +
                '</div>'},
            { field: 'CABLESTRUCTURE_NAME', displayName: "Название",
                enableHiding: false},
            { field: 'CABLESTRUCTURE_STRUCTURE', displayName: "Структура",
                enableHiding: false},
            {
                name: " ",
                enableColumnMenu: false,
                enableFiltering: false,
                cellTemplate: '<div class="ui-grid-cell-contents" title="TOOLTIP">' +
                '<a  ng-click="grid.appScope.removeRow(row.entity)"' +
                'ng-show="!vm.loading">' +
                '<span class="glyphicon glyphicon-remove" style="font-size: 18px;"></span>' +
                '</a></div>',
                width: "30",
                enableCellEdit: false
            }
        ]
    };

    load();
    function load() {
        vm.loading = true;
        return CablestructureApi.all()
            .then(function (res) {
                console.log('Cablestructures are reseived ', res.data);
                vm.list = res.data;
                vm.gridOptions1.data = res.data;
            })
            .catch(function (err) {
                console.log('get Cablestructures return ERROR!', err.data);
                vm.list = [];
                vm.gridOptions1.data = [];
                Alert.showError("Ошибка при получении сечений: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
                vm.dataIsLoaded = true;
            })
    }

    function createClick() {
        vm.isCreating = true
    }

    function cancelCreate() {
        vm.newCablestructure = {};
        vm.isCreating = false
    }

    function createCablestructure (){
        vm.loading = true;
        if (!vm.newCablestructure.name){
            Alert.showError("Введите имя");
            return
        }
        return CablestructureApi.add(vm.newCablestructure.name)
            .then(function (res) {
                console.log('object is added ', res.data);
                vm.newCablestructure = {};
                vm.isCreating = false
                $state.go('main.cablestructure', {cablestructure_id: res.data.CABLESTRUCTURE_ID});
            })
            .catch(function (err) {
                console.log('object add return ERROR!', err);
                Alert.showError("Ошибка при добавлении: "
                + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }

    function removeRow(gridRow) {
        vm.loading = true;
        return CablestructureApi.remove(gridRow.CABLESTRUCTURE_ID)
            .then(function (res) {
                console.log('object is removed ', res.data);
                var index = vm.list.indexOf(gridRow);
                console.log('index', index)
                if (index >= 0) {
                    vm.list.splice(index, 1);
                }
            })
            .catch(function (err) {
                console.log('object remove return ERROR!', err);
                Alert.showError("Ошибка при удалении: "
                + err && err.data ? err.data : err);

            })
            .finally(function () {
                vm.loading = false;
            })
    }
}

angular.module('rControllers').controller('CablestructuresCtrl', CablestructuresCtrl);