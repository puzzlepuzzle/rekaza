var DB = require('./../db');

var Cable = DB.Model.extend({
    tableName: 'cable',
    idAttribute: 'CABLE_ID',

});

module.exports = Cable;