//запуск тестов  node ./node_modules/mocha/bin/mocha --timeout 150000
var superagent = require('superagent');
var app = require('../server')
var request = require('supertest')(app);
var agent = superagent.agent();

var testAccount = {
    "login": "admin",
    "password": "1"
};

var login = function (request, done) {
    request
        .post('/api/login')
        .send(testAccount)
        .end(function (err, res) {
            if (err) {
                throw err;
            }
            agent.saveCookies(res);
            done(agent);
        });
};
describe('TEST API DETAIL', function () {

    var loginAgent;
    var test_api = function (mode, api, params) {
        it(api, function (done) {
            var req;
            if (mode == 'post') {
                console.log('post', params)
                req = request.post(api);
                agent.attachCookies(req);
                req.send(params).expect(200)
                    .end(function(err, res){
                        console.log(res.body)
                        console.log(err)
                        done(err)
                    });
            }
            else {
                console.log('get', api)
                req = request.get(api);
                agent.attachCookies(req);
                req.expect(200)
                    .end(function(err, res){
                        done(err)
                    });
            }
        });
    }
    before(function (done) {
        login(request, function (authAgent) {
            loginAgent = authAgent;
            done();
        });
    });
    /*
    //ADD
    test_api('post','/api/detail/add', {date: '2015-09-03', detailtype_id: 1, machine_id: 1});
    test_api('get' ,'/api/details?from=2014-01-01&to=2016-01-01');
    //EDIT
    */
    test_api('post','/api/detail/edit', {id: 5, date: '2015-09-03', detailtype_id: 2, machine_id: 2, done: 0});
    test_api('get' ,'/api/details?from=2014-01-01&to=2016-01-01');
    
    //SET MPLAN
    test_api('post','/api/detail/mplans/set', {detail_id: 5, ids_string: '1,2,6,5,4,3'});
    
});


