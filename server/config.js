
var Config = (function () {
    function Config() {
    }
    Config.mysql_host = process.env.MYSQL_HOST  || 'localhost';
    Config.mysql_user = process.env.MYSQL_USER || 'root';
    Config.mysql_pwd = process.env.MYSQL_PWD || '';
    Config.passport_key = process.env.PASSPORT_KEY || 'qwe';

    console.log(Config)
    return Config;
})();

module.exports = Config;