'use strict';

function UsersCtrl($scope, $state, $q, Alert, Authenticate, UserApi) {
    var vm = this;
    vm.register = register;
    vm.remove = remove;
    vm.oneAtATime = true;
    vm.loading = false;
    vm.load = load;
    vm.users = [];
    vm.roles = [];
    vm.isOpen = true;
    vm.started = false;
    vm.form = null;

    startLoad();

    function register(user) {
        vm.loading = true;
        return Authenticate.register(user)
            .then(function (data) {
                vm.form = null;
                console.log("$scope.register", data)
//                    Authenticate.isAuthenticated = true
//                    $location.path("/sites");

                vm.loading = false;
                Alert.showSuccess("Пользователь успешно зарегистрирован!");
                return vm.load()
            })
            .catch(function (err) {
                console.log("$scope.register error ", err)
                vm.loading = false;
                Alert.showError(err.data);
            })

    }

    function load() {
        vm.loading = true;
        return UserApi.users()
            .then(function (res) {
                console.log('users are reseived ', res.data);
                vm.loading = false;
                vm.users = res.data;
                vm.isOpen = true;
                return loadRoles()
            })
            .catch(function (err) {
                console.log('get users return ERROR!', err.data);
                vm.users = [];
                vm.loading = false;

                Alert.showError("Ошибка при получении списка пользователей: " + err.data);

                throw err;
            })


    };

    function remove (user_id){
        if ( !user_id){
            Alert.showError("Не задан id пользователя");
            return
        }
        vm.loading = true;
        return UserApi.remove(user_id)
            .then(function (res) {
                console.log('user is deleted ', res.data);
                Alert.showSuccess("Пользователь удален.");

                return vm.load()
            })
            .catch(function (err) {
                console.log('remove user return ERROR!', err.data);

                Alert.showError("Ошибка при удалении пользователя: " + err.data);
            })
            .finally(function (){
                vm.loading = false;
            })

    }

    function startLoad() {
        return vm.load()
            .then(function (res) {
                vm.started = true;
            })

    };

    function loadRoles() {
        vm.loading = true;
        return UserApi.roles()
            .then(function (res1) {
                console.log('roles are reseived ', res1.data);
                vm.roles = res1.data;
                vm.loading = false;
            })
            .catch(function (err) {
                console.log('get roles return ERROR!', err.data);
                vm.roles = [];
                vm.users = [];
                vm.loading = false;
                Alert.showError("Ошибка при получении списка ролей: " + err.data);
            });
    }


}

angular.module('rControllers').controller('UsersCtrl', UsersCtrl);