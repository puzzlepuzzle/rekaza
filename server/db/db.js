var Bookshelf = require('bookshelf');
var Config = require('../config');

var config = {
    host: Config.mysql_host,  // your host
    user: Config.mysql_user, // your database user
    password: Config.mysql_pwd, // your database password
    database: 'rekaza',
    charset: 'utf8',
    timezone: '+0:00'
};
var knex = require('knex')({
    client: 'mysql',
    connection: config
});
var DB = require('bookshelf')(knex);

module.exports = DB;